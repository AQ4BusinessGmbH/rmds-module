Attribute VB_Name = "Dom�nenFunktionWrapper"
Option Compare Database
Option Explicit

Private Enum DomainFunctionWrapperEnum
    DLookup_Wrapper
    DCount_Wrapper
    DSum_Wrapper
    DMax_Wrapper
    DMin_Wrapper
    DAvg_Wrapper
End Enum

'***********************************************************************************
'
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert:
'***********************************************************************************
Private Function DomainFunctionWrapper(DomainFunction As DomainFunctionWrapperEnum, _
                                    Expr As String, _
                                    Domain As String, _
                                    Optional Criteria As String) As Variant
    On Error GoTo ErrorHandler
    
    Select Case DomainFunction
    Case DLookup_Wrapper
        DomainFunctionWrapper = DLookup(Expr, Domain, Criteria)
    Case DCount_Wrapper
        DomainFunctionWrapper = DCount(Expr, Domain, Criteria)
    Case DSum_Wrapper
        DomainFunctionWrapper = DSum(Expr, Domain, Criteria)
    Case DMax_Wrapper
        DomainFunctionWrapper = DMax(Expr, Domain, Criteria)
    Case DMin_Wrapper
        DomainFunctionWrapper = DMin(Expr, Domain, Criteria)
    Case DSum_Wrapper
        DomainFunctionWrapper = DSum(Expr, Domain, Criteria)
    Case DAvg_Wrapper
        DomainFunctionWrapper = DAvg(Expr, Domain, Criteria)
    Case Else
        ' Unerwartetes Argument des Typs 'DomainFunction'
        Debug.Assert False
    End Select

Done:
    Exit Function
ErrorHandler:
    Debug.Print Err.Number & " - " & Err.Description & " Domain wurde aufgerufen: " & DomainFunction & Expr & Domain & Criteria
End Function

'***********************************************************************************
'DLookupWrapper is just like DLookup only it will trap errors.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DLookupWrapper(Expr As String, Domain As String, Optional Criteria As String) As Variant
    DLookupWrapper = DomainFunctionWrapper(DLookup_Wrapper, Expr, Domain, Criteria)
End Function

'***********************************************************************************
'DLookupStringWrapper is just like DLookup wrapped in an Nz
'Damit wird stets eine Zeichenfolge zur�ckgegeben.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DLookupStringWrapper(Expr As String, Domain As String, Optional Criteria As String, Optional ValueIfNull As String = "") As String
    DLookupStringWrapper = Nz(DLookupWrapper(Expr, Domain, Criteria), ValueIfNull)
End Function

'***********************************************************************************
'DLookupNumberWrapper' verh�lt sich wie 'DLookup', das in einem Nz umgebrochen wurde, deren Standardwert gegen 0 geht.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DLookupNumberWrapper(Expr As String, Domain As String, Optional Criteria As String, Optional ValueIfNull = 0) As Variant
    DLookupNumberWrapper = Nz(DLookupWrapper(Expr, Domain, Criteria), ValueIfNull)
End Function

'***********************************************************************************
'DCountWrapper verh�lt sich wie DCount, es werden jedoch Fehler erkannt.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DCountWrapper(Expr As String, Domain As String, Optional Criteria As String) As Long
    DCountWrapper = DomainFunctionWrapper(DCount_Wrapper, Expr, Domain, Criteria)
End Function

'***********************************************************************************
'DMaxWrapper' verh�lt sich wie 'DMax', es werden jedoch Fehler erkannt.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DMaxWrapper(Expr As String, Domain As String, Optional Criteria As String) As Long
    DMaxWrapper = DomainFunctionWrapper(DMax_Wrapper, Expr, Domain, Criteria)
End Function

'***********************************************************************************
'DMinWrapper verh�lt sich wie DMin, es werden jedoch Fehler erkannt.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DMinWrapper(Expr As String, Domain As String, Optional Criteria As String) As Long
    DMinWrapper = DomainFunctionWrapper(DMin_Wrapper, Expr, Domain, Criteria)
End Function

'***********************************************************************************
'DSumWrapper verh�lt sich wie DSum, es werden jedoch Fehler erkannt.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DSumWrapper(Expr As String, Domain As String, Optional Criteria As String) As Long
    DSumWrapper = DomainFunctionWrapper(DSum_Wrapper, Expr, Domain, Criteria)
End Function


'***********************************************************************************
'DAvgWrapper verh�lt sich wie DAvg, es werden jedoch Fehler erkannt.
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'***********************************************************************************
Public Function DAvgWrapper(Expr As String, Domain As String, Optional Criteria As String) As Long
    DAvgWrapper = DomainFunctionWrapper(DAvg_Wrapper, Expr, Domain, Criteria)
End Function




