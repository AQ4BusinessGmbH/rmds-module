Attribute VB_Name = "mdlBerechnungen"
Option Compare Database
Option Explicit

'**********************************************************
' Modul enth�lt Hilfsfunktionen f�r Berechnungen in Bezug z.B. auf Proben
' 17.07.2019, KAQ: ASSE-622 M0 Gruppe statt Teilbereich in Probenbezeichnung
'**********************************************************

'***********************************************************************************
'Berechnet den Teufendruck anhand von Luftdruck und Teufe
'erstellt am: 14.10.2016
'erstellt von: Tobias Langer
'R�ckgabewert double: Teufendruck
'***********************************************************************************
Public Function TeufendruckBerechnen(Luftdruck As Double, Teufe As Integer) As Double
    If IsNumeric(Luftdruck) And IsNumeric(Teufe) Then
        TeufendruckBerechnen = Luftdruck * Exp(1.293 * 9.81 * Teufe / 101325)
    Else
        TeufendruckBerechnen = 0
    End If
End Function

'***********************************************************************************
'Berechnet die absolute Feuchte anhand von Trockentemperatur, Feuchttemperatur, Luftdruck und Teufe
'erstellt am: 07.10.2016
'erstellt von: Tobias Langer
'R�ckgabewert double: Absolute Feuchte
'***********************************************************************************
Public Function AbsoluteFeuchteBerechnen(Temperatur_trocken As Double, Temperatur_feucht As Double, Teufendruck As Double) As Double
    If IsNumeric(Temperatur_trocken) And IsNumeric(Temperatur_feucht) And IsNumeric(Teufendruck) Then
        Dim vp As Double
        Dim svp As Double
        Dim rf As Double
        Dim mischungsverhaeltnis As Double
        Dim enthalpie As Double
        Dim feuchteRelativ As Double
        Dim wetterdichte As Double
        
        vp = 6.1078 * Exp(17.08085 * Temperatur_feucht / (234.175 + Temperatur_feucht)) - (0.00066 * (1 + 0.00115 * Temperatur_feucht) * (Temperatur_trocken - Temperatur_feucht) * Teufendruck)
        svp = Exp(17.08085 * Temperatur_trocken / (234.175 + Temperatur_trocken)) * 6.1078
        rf = 287.1 / (1 - 0.378 * vp / Teufendruck)
        mischungsverhaeltnis = 621.98 * vp / (Teufendruck - vp)
        enthalpie = 1004 * Temperatur_trocken + mischungsverhaeltnis * 2500 + mischungsverhaeltnis * 0.001 * 1860 * Temperatur_trocken
        feuchteRelativ = 100 * vp / svp
        wetterdichte = Teufendruck * 100 / (rf * (Temperatur_trocken + 273.15))
        AbsoluteFeuchteBerechnen = mischungsverhaeltnis * wetterdichte * 1000 / (1000 + mischungsverhaeltnis)
    Else
        AbsoluteFeuchteBerechnen = 0
    End If
End Function

'***********************************************************************************
'Berechnet die relative Feuchte anhand von Trockentemperatur, Feuchttemperatur, Luftdruck und Teufe
'erstellt am: 07.10.2016
'erstellt von: Tobias Langer
'R�ckgabewert double: Relative Feuchte
'***********************************************************************************
Public Function RelativeFeuchteBerechnen(Temperatur_trocken As Double, Temperatur_feucht As Double, Teufendruck As Double) As Double
    Dim vp As Double
    Dim svp As Double
    vp = 6.1078 * Exp(17.08085 * Temperatur_feucht / (234.175 + Temperatur_feucht)) - (0.00066 * (1 + 0.00115 * Temperatur_feucht) * (Temperatur_trocken - Temperatur_feucht) * Teufendruck)
    svp = Exp(17.08085 * Temperatur_trocken / (234.175 + Temperatur_trocken)) * 6.1078
    RelativeFeuchteBerechnen = 100 * vp / svp
End Function

'***********************************************************************************
'Generiert Probennummer
'Tabellen tblM0Teilbereich, tblSohle, tblMessstelle, tblM0ProbeArt m�ssen im Zugriff sein.
'erstellt am: 24.05.2018
'erstellt von: Tobias Langer
'R�ckgabewert: void
'ge�ndert am: 25.09.2018
'ge�ndert von: Kai A. Quante
'Sohle immer dreistellig, d.h. auch die 0er Sohle
'ge�ndert: 17.07.2019, KAQ: ASSE-622 M0 Gruppe statt Teilbereich in Probenbezeichnung
'***********************************************************************************
Public Function generiereProbennummer(Teilbereich_ID As Variant, Sohle_ID As Variant, Messstelle_ID As Variant, ProbeArt_ID As Variant, Probenentnahme_Ende As Variant, Probe_ID As Variant) As String
    generiereProbennummer = ""
    Dim TeilbereichText As String
    Dim SohleText As String
    Dim MessstelleText As String
    Dim ProbeArtText As String
    
    If Teilbereich_ID > 0 And Sohle_ID > 0 And Messstelle_ID > 0 And ProbeArt_ID > 0 And Not IsNull(Probenentnahme_Ende) And Probe_ID > 0 Then
        TeilbereichText = DLookupStringWrapper("Kuerzel", "tblM0Teilbereich", "Teilbereich_ID = " & Teilbereich_ID)
        SohleText = Format(DLookupNumberWrapper("Tiefe", "tblSohle", "Sohle_ID = " & Sohle_ID, 0), "000")
        MessstelleText = DLookupStringWrapper("Kuerzel", "tblMessstelle", "Messstelle_ID = " & Messstelle_ID)
        ProbeArtText = DLookupStringWrapper("Kuerzel", "tblM0ProbeArt", "ProbeArt_ID = " & ProbeArt_ID)
        ' ASSE-622 M0 Gruppe statt Teilbereich in Probenbezeichnung, wenn Bereich nicht mit "T-" beginnt
        If Left(TeilbereichText, 2) = "T-" Then
            generiereProbennummer = "AS-"
            generiereProbennummer = generiereProbennummer & Mid(TeilbereichText, 3) & "-"
        Else
            generiereProbennummer = TeilbereichText & "-"
        End If
        generiereProbennummer = generiereProbennummer & "S" & SohleText & "m" & "-"
        generiereProbennummer = generiereProbennummer & MessstelleText & "-"
        generiereProbennummer = generiereProbennummer & ProbeArtText & "-"
        generiereProbennummer = generiereProbennummer & Format(Probenentnahme_Ende, "yyyymmdd") & "-"
        generiereProbennummer = generiereProbennummer & Format(Probe_ID, "00,000,000")
    End If
End Function


