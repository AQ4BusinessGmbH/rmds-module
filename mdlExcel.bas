Attribute VB_Name = "mdlExcel"
Option Compare Database
Option Explicit

'**************************************************************************************
'1.1.0 10.05.19 KAQ: Fortschritt in mdlGlobal verschoben
'1.2.0 27.05.19 KAQ: isExportExcel erg�nzt
'1.2.1 06.08.19 Tobias Langer: erstelleExcelExport um optionale Abfrage-WHERE erg�nzt
'1.2.2 07.08.19 KAQ: Debug.Print in erstelleExcelExport entfernt
'1.2.3 29.08.19 KAQ: ASSE-656 M6 Exportfunktionalit�t in Gamma Men�
'1.3.0 24.09.19 KAQ: ASSE-695 M0 Proben exportieren mit Filter und Jahr ignorieren
'1.3.1 24.09.19 KAQ: ASSE-713 M7 L�nge Tab Name in Excel Export auf 31 begrenzen
'1.4.0 20.07.21 KAQ: ASSE-860 M2 Export f�r GWB
'1.4.1 20.01.22 KAQ: ASSE-880 M0 Excel Export fehlt letzte Spalte
'1.4.2 17.03.22 KAQ: ASSE-888 M0 WHERE Bedingung in Excel Vorlage (CONFIG, TEMPLATE, DATA)
'1.5.0 17.03.23 KAQ: ASSE-1020 Mx mdlExcel.erstelleExcelExport Aufeinander folgende Zellen in Zeile verbinden, wenn Werte gleich oder leer
'1.5.0 17.03.23 KAQ: ASSE-924 Mx mdlExcel.erstelleExcelExport auch leere Spalten bzw. �berspringen
'1.5.1 31.05.23 KAQ: ASSE-924 ... => .Formula statt .Value
'1.5.2 05.10.23 KAQ: ASSE-1092 M0 Abfragen f�r Reports nur lesend, siehe Excel Export
'1.6.0 14.03.24 KAQ: ASSE-1114 Mx Mx Reporting Filter mit freiem Zeitraum: FILTERTAG als YYYYMMDD, YYYYMM oder YYYY
'                    ASSE-1115 Mx Reporting Filter f�r Probeart: PROBEART
'                    ASSE-1137 Mx Reporting Filter Gruppe: GRUPPE
'                    ASSE-1131 Mx Reporting Festen Namen f�r Tabellenblatt in Auswertung definieren: TABELLENBLATT
'1.7.0 23.05.24 KAQ: ASSE-1123 M0 QR-Code in Messprotokoll f�r Labor
'1.7.1 02.07.24 KAQ: ASSE-1172 M0 QR-Code doppelte Option Explicit
'                    ASSE-1179 M0 Updates f�r QR-Codes
'1.7.2 04.11.24 KAQ: ASSE-1219 Mx Excel Export Sortierung
'**************************************************************************************

Public Const Version As String = "1.7.2"

'**************************************************************************************
'Excel exportieren
'erstellt am: 25.08.2016
'erstellt von: Kai A. Quante
'ge�ndert am: 19.01.2017
'ge�ndert von: Kai A. Quante
'Bemerkung: Name f�r Exportdatei hinzugef�gt
'**************************************************************************************
Public Function ExcelExport(sSQL As String, strExportName As String, boolAutostart As Boolean) As Boolean
On Error GoTo Err_cmd_expAuswahl_Click
    Dim qdf As DAO.QueryDef
    Dim db As DAO.Database
       
    Set db = CurrentDb
    If Not QueryExists(strExportName) Then
        Set qdf = db.CreateQueryDef(strExportName, sSQL)
      Else
        db.QueryDefs(strExportName).SQL = sSQL
    End If
    DoCmd.OutputTo acOutputQuery, strExportName, acFormatXLSX, , True
    DoCmd.DeleteObject acQuery, strExportName
    Set db = Nothing
    ExcelExport = True
Exit_cmd_expAuswahl_Click:
    Exit Function
Err_cmd_expAuswahl_Click:
    If DCount("*", "MsysObjects", "Name='" & strExportName & "'") > 0 Then
        DoCmd.DeleteObject acQuery, strExportName
    End If
    ExcelExport = False
    Resume Exit_cmd_expAuswahl_Click
End Function

'**************************************************************************************
'Gibt zur�ck, ob Worksheeds CONFIG, TEMPLATE und DATA existieren
'erstellt am: 27.05.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: boolean
'**************************************************************************************
Public Function isExportExcel(strDateipfad As String) As Boolean
    On Error GoTo KonfigError
    isExportExcel = False
    If Dir(strDateipfad) <> "" Then
        Dim appExcel As Excel.Application
        Dim workbook As Excel.workbook
        Dim worksheetKonfig As Excel.worksheet
        Set appExcel = Excel.Application
        Set workbook = appExcel.Workbooks.Open(strDateipfad)
        Set worksheetKonfig = workbook.Worksheets("CONFIG") 'CONFIG Reiter
        Set worksheetKonfig = workbook.Worksheets("TEMPLATE") 'CONFIG Reiter
        Set worksheetKonfig = workbook.Worksheets("DATA") 'CONFIG Reiter
        workbook.Close
        isExportExcel = True
    End If
    Exit Function
KonfigError:
    On Error Resume Next
    If Not workbook Is Nothing Then
        workbook.Close
    End If
End Function

'**************************************************************************************
'Gibt den Inhaltsbereich (Range) eines Platzhalters in der Konfig zur�ck
'erstellt am: 28.02.2018
'erstellt von: Tobias Langer
'R�ckgabewert: Array Variant
'**************************************************************************************
Public Function getKonfigAusExcel(strDateipfad As String) As Variant
    If Dir(strDateipfad) <> "" Then
        Dim appExcel As Excel.Application
        Dim workbook As Excel.workbook
        Dim worksheetKonfig As Excel.worksheet
        Dim intReihe As Long
        Dim intSpalte As Long
        Dim intSpalteMax As Long
        Dim intReiheMax As Long
        Dim intForSpalte As Long
        Dim intTmpDataReihe As Long
        Dim strIndexKonfig As String
        Dim boolMultiArray As Boolean
        Dim arrData() As Variant
        
        boolMultiArray = False
        intReihe = 1
        intReiheMax = intReihe
        intTmpDataReihe = 0
        intSpalte = 1
        intSpalteMax = intSpalte
        
        Set appExcel = Excel.Application
        Set workbook = appExcel.Workbooks.Open(strDateipfad)
        Set worksheetKonfig = workbook.Worksheets("CONFIG") 'CONFIG Reiter
        
        'Ermittel max Reihe und Max Spalte
        Do
            If boolMultiArray = False And worksheetKonfig.Cells(intReihe, intSpalte).Value = "FELD" Then
                boolMultiArray = True
                
                'Ermittel max Spalten zum einlesen
                Do
                    If worksheetKonfig.Cells(intReihe, intSpalteMax).Value = "" Then
                        Exit Do
                    Else
                        intSpalteMax = intSpalteMax + 1
                    End If
                Loop
            ElseIf boolMultiArray = True And worksheetKonfig.Cells(intReihe, intSpalte).Value <> "" Then
                intReiheMax = intReiheMax + 1
            ElseIf boolMultiArray = True And worksheetKonfig.Cells(intReihe, intSpalte).Value = "" Then
                Exit Do
            End If
        
            intReihe = intReihe + 1
        Loop
        
        'Array mit Daten f�llen
        boolMultiArray = False
        intReihe = 1
        intSpalte = 1
        ReDim arrData(intReiheMax - 2, intSpalteMax)
        
        Do
            If boolMultiArray = False And worksheetKonfig.Cells(intReihe, intSpalte).Value = "FELD" Then
                boolMultiArray = True
            ElseIf boolMultiArray = True And worksheetKonfig.Cells(intReihe, intSpalte).Value <> "" Then
                intForSpalte = 1
                For intForSpalte = 1 To intSpalteMax
                   arrData(intTmpDataReihe, intForSpalte - 1) = worksheetKonfig.Cells(intReihe, intForSpalte).Value
                Next intForSpalte
                
                intTmpDataReihe = intTmpDataReihe + 1
            ElseIf boolMultiArray = True And worksheetKonfig.Cells(intReihe, intSpalte).Value = "" Then
                Exit Do
            End If
        
            intReihe = intReihe + 1
        Loop
        
        workbook.Close
        getKonfigAusExcel = arrData
    End If
End Function

'**************************************************************************************
'Gibt den Inhaltsbereich (Range) eines Platzhalters in der Konfig zur�ck
'erstellt am: 06.03.2018
'erstellt von: Tobias Langer
'R�ckgabewert: String
'**************************************************************************************
Public Function getSqlAnweisungAusExcel(strDateipfad As String, strRange As String, strReiter As String) As String
    If Dir(strDateipfad) <> "" Then
        Dim appExcel As Excel.Application
        Dim workbook As Excel.workbook
        Dim worksheetKonfig As Excel.worksheet
                
        Set appExcel = Excel.Application
        Set workbook = appExcel.Workbooks.Open(strDateipfad)
        Set worksheetKonfig = workbook.Worksheets(strReiter)
        
        getSqlAnweisungAusExcel = worksheetKonfig.Range(strRange).Value
        
        workbook.Close
    End If
End Function

'**************************************************************************************
'Erzeugt einen Excel-Export anhand eines SQL Query, welcher in der Excelvorlage gespeichert ist. Exportjahr, Ablageverzeichnis, Excelvorlage, Modulnamen werden �bergeben
'erstellt am: 28.02.2018
'erstellt von: Tobias Langer
'R�ckgabewert: Boolean
' intJahr = 0 => Jahr wird ignoriert !
'ge�ndert: ASSE-377 25.06.2018 Kai A. Quante: Export mit Zeitstempel yyMMddhhmm
'ge�ndert: ASSE-378 25.06.2018 Kai A. Quante: Zus�tzliche Spalte NULL in CONFIG Tab
'ge�ndert: ASSE-464 07.09.2018 Kai A. Quante: Performance Optimierung bez�glich Formatierung horizontal/vertikal
'ge�ndert: ASSE-643 06.08.2019 Tobias Langer: Optionaler Parameter f�r Abfrage-WHERE
'ge�ndert: ASSE-695 24.09.2018 Kai A. Quante: M0 Proben exportieren mit Filter und Jahr ignorieren
'ge�ndert: 17.03.2022, KAQ: ASSE-888 M0 WHERE Bedingung in Excel Vorlage (CONFIG, TEMPLATE, DATA)
'ge�ndert: 17.03.2023, KAQ: ASSE-1020 Mx mdlExcel.erstelleExcelExport Aufeinander folgende Zellen in Zeile verbinden, wenn Werte gleich oder leer
'ge�ndert: 17.03.2023, KAQ: ASSE-924 Mx mdlExcel.erstelleExcelExport auch leere Spalten bzw. �berspringen
'ge�ndert: 05.10.2023, KAQ: ASSE-1092 M0 Abfragen f�r Reports nur lesend, siehe Excel Export
'ge�ndert: 14.03.2024, KAQ: ASSE-1114 Mx Reporting Filter mit freiem Zeitraum: FILTERTAG als YYYYMMDD, YYYYMM oder YYYY
'ge�ndert: 14.03.2024, KAQ: ASSE-1115 Mx Reporting Filter f�r Probeart: PROBEART
'ge�ndert: 14.03.2024, KAQ: ASSE-1137 Mx Reporting Filter Gruppe: GRUPPE
'ge�ndert: 14.03.2024, KAQ: ASSE-1131 Mx Reporting Festen Namen f�r Tabellenblatt in Auswertung definieren
'ge�ndert: 14.03.2024, KAQ: ASSE-1219 Mx Excel Export Sortierung
'**************************************************************************************
Public Function erstelleExcelExport(intJahr As Integer, strAblage As String, strVorlage As String, strModul As String, Optional boolZentriereFuellzeichen As Boolean = False, Optional strCriteria As String = "", _
             Optional strProbeart As String = "", Optional strVonYYYYMMDD As String = "", Optional strBisYYYYMMDD As String = "", Optional strGruppe As String = "", Optional strSortierung As String = "") As Boolean
    erstelleExcelExport = False
    Dim appExcel As Excel.Application
    Dim workbook As Excel.workbook
    Dim worksheetTab As Excel.worksheet
    Dim cellFind As Range
    Dim arrKonfig() As Variant
    Dim arrDaten() As Variant
    Dim arrMerge() As Variant
    Dim arrOrientation() As Variant
    Dim arrDatenVerbinden() As Variant
    Dim strIndexTabelleDaten As String
    Dim strFuellzeichen As String
    Dim strPlatzhalter1 As String
    Dim strPlatzhalter2 As String
    Dim strPlatzhalter3 As String
    Dim strTabellenblatt As String
    Dim strAbfrage As String
    Dim strAbfrageZusatz As String
    Dim strAbfrageFarbe As String
    Dim strDateiname As String
    Dim rs As DAO.Recordset
    Dim rsFiltered As DAO.Recordset
    Dim rsFiltered2 As DAO.Recordset
    Dim intRsFiltered As Long
    Dim i As Long
    Dim indexReihe As Long
    Dim indexStartReihe As Long
    Dim indexSpalte As Long
    Dim intRSReihe As Long
    Dim intOrientation As Integer
    
    indexReihe = 3
    indexSpalte = 1
    intRSReihe = 1
    
    Call Fortschritt(0, "Initialisierung")
    'Konfig aus Vorlage auslesen und als Array speichern
    arrKonfig = getKonfigAusExcel(strVorlage)
    ReDim arrMerge(UBound(arrKonfig), 2)
    ReDim arrOrientation(UBound(arrKonfig))
    strIndexTabelleDaten = getRangeAusExcel(strVorlage, "TABELLE_DATEN")
    strFuellzeichen = getRangeAusExcel(strVorlage, "F�LLZEICHEN")
    strPlatzhalter1 = getRangeAusExcel(strVorlage, "PLATZHALTER1")
    strPlatzhalter2 = getRangeAusExcel(strVorlage, "PLATZHALTER2")
    strPlatzhalter3 = getRangeAusExcel(strVorlage, "PLATZHALTER3")
    strTabellenblatt = getRangeAusExcel(strVorlage, "TABELLENBLATT")
    strAbfrage = getSqlAnweisungAusExcel(strVorlage, "B1", "DATA")
    strAbfrageZusatz = ""
    indexSpalte = InStr(1, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", Left(strIndexTabelleDaten, 1))
    indexStartReihe = Mid(strIndexTabelleDaten, 2)
    indexReihe = indexStartReihe
    
    'Abfrage-WHERE
    ' ASSE-888 M0 WHERE Bedingung in Excel Vorlage (CONFIG, TEMPLATE, DATA)
    If strCriteria <> "" Then
'        strAbfrage = strAbfrage + " WHERE " + strCriteria
        strAbfrage = strAbfrage + IIf(InStr(strAbfrage, " WHERE ") > 0, " AND ", " WHERE ") + strCriteria
    End If
    
    
    'Abfrage in die richtige Reihenfolge bringen
    For i = LBound(arrKonfig) To UBound(arrKonfig)
        If arrKonfig(i, 0) <> "" Then
            If arrKonfig(i, 0) = "PLATZHALTER1" Then
                strAbfrageZusatz = strAbfrageZusatz & ",'" & strPlatzhalter1 & "'"
            ElseIf arrKonfig(i, 0) = "PLATZHALTER2" Then
                strAbfrageZusatz = strAbfrageZusatz & ",'" & strPlatzhalter2 & "'"
            ElseIf arrKonfig(i, 0) = "PLATZHALTER3" Then
                strAbfrageZusatz = strAbfrageZusatz & ",'" & strPlatzhalter3 & "'"
            Else
                strAbfrageZusatz = strAbfrageZusatz & ",[" & arrKonfig(i, 0) & "]"
            End If
        End If
    Next i
    'Farbe erg�nzen wenn noch nicht vorhanden
    strAbfrageFarbe = ""
    i = 0
    For i = LBound(arrKonfig) To UBound(arrKonfig)
        ' Farben merken
        If arrKonfig(i, 2) <> "" Then
            If InStr(strAbfrageFarbe, arrKonfig(i, 2)) = 0 And InStr(strAbfrageZusatz, arrKonfig(i, 2)) = 0 Then
                strAbfrageFarbe = strAbfrageFarbe & "," & arrKonfig(i, 2)
            End If
        End If
    Next i
    
    'ASSE-1114 Mx Reporting Filter mit freiem Zeitraum: FILTERTAG als YYYYMMDD, YYYYMM oder YYYY
    'ASSE-1115 Mx Reporting Filter f�r Probeart: PROBEART
    'ASSE-1137 Mx Reporting Filter Gruppe: GRUPPE
    'ASSE-1131 Mx Reporting Festen Namen f�r Tabellenblatt in Auswertung definieren
    If strAbfrageZusatz <> "" Then
        strAbfrage = "SELECT " & Mid(strAbfrageZusatz, 2) & strAbfrageFarbe _
                & IIf(InStr(strAbfrageZusatz, "[FILTERJAHR]") = 0 And intJahr <> 0, ",FILTERJAHR", "") _
                & IIf(InStr(strAbfrageZusatz, "[FILTERTAG]") = 0 And (strBisYYYYMMDD <> "" Or strVonYYYYMMDD <> ""), ",FILTERTAG", "") _
                & IIf(InStr(strAbfrageZusatz, "[PROBEART]") = 0 And (strProbeart <> ""), ",PROBEART", "") _
                & IIf(InStr(strAbfrageZusatz, "[GRUPPE]") = 0 And (strGruppe <> ""), ",GRUPPE", "") _
                & " FROM (" & strAbfrage & ")"
    End If
    
    Call Fortschritt(1, "Excel starten")
    'Daten in Excel schreiben
    Set appExcel = Excel.Application
    Set workbook = appExcel.Workbooks.Open(strVorlage)
    appExcel.DisplayAlerts = False
    appExcel.Calculation = xlCalculationManual
    
    workbook.Sheets("TEMPLATE").Copy After:=workbook.Sheets(workbook.Sheets.count)
    ' Jahr ignorieren wenn = 0
    ' ASSE-1131 Mx Reporting Festen Namen f�r Tabellenblatt in Auswertung definieren: TABELLENBLATT
    workbook.Sheets(workbook.Sheets.count).Name = IIf(strTabellenblatt <> "", strTabellenblatt, Left(strModul, 27) & IIf(intJahr <> 0, " " & intJahr, ""))
    Set worksheetTab = workbook.Worksheets(workbook.Sheets.count)
    
    ' Orientierung holen
    For i = LBound(arrKonfig) To UBound(arrKonfig)
        arrOrientation(i) = worksheetTab.Cells(indexReihe, i + 1).HorizontalAlignment
    Next i
    
    
    'Reset i f�r weitere Verwendung
    i = 0

    Call Fortschritt(3, "Daten abrufen")
    'Daten verwenden mit �bergebener Abfrage und optionalen Filter
    On Error GoTo RecordsetError
    'ASSE-1219 Mx Excel Export Sortierung
    If strSortierung > "" Then
        strAbfrage = strAbfrage & " ORDER BY " & strSortierung
    End If
    'ASSE-1092 M0 Abfragen f�r Reports nur lesend, siehe Excel Export
    Set rs = CurrentDb.OpenRecordset(strAbfrage, dbOpenSnapshot)
'    Set rs = CurrentDb.OpenRecordset(strAbfrage, dbOpenDynaset, dbSeeChanges)
    On Error GoTo 0
    ' Jahr ignorieren wenn = 0
    If intJahr <> 0 Then
        rs.Filter = "FILTERJAHR = " & intJahr
    End If
    'ASSE-1114 Mx Mx Reporting Filter mit freiem Zeitraum: FILTERTAG als YYYYMMDD, YYYYMM oder YYYY
    If strVonYYYYMMDD <> "" Then
        rs.Filter = IIf(Nz(rs.Filter, "") <> "", rs.Filter & " AND ", "") & "FILTERTAG >= '" & strVonYYYYMMDD & String(8 - Len(strVonYYYYMMDD), "0") & "'"
    End If
    If strBisYYYYMMDD <> "" Then
        rs.Filter = IIf(Nz(rs.Filter, "") <> "", rs.Filter & " AND ", "") & "FILTERTAG <= '" & strBisYYYYMMDD & String(8 - Len(strBisYYYYMMDD), "9") & "'"
    End If
    'ASSE-1115 Mx Reporting Filter f�r Probeart: PROBEART
    If strProbeart <> "" Then
        rs.Filter = IIf(Nz(rs.Filter, "") <> "", rs.Filter & " AND ", "") & "PROBEART = '" & strProbeart & "'"
    End If
    'ASSE-1137 Mx Reporting Filter Gruppe: GRUPPE
    If strGruppe <> "" Then
        rs.Filter = IIf(Nz(rs.Filter, "") <> "", rs.Filter & " AND ", "") & "GRUPPE like '" & strGruppe & "*'"
    End If
    Set rsFiltered = rs.OpenRecordset
    Set rsFiltered2 = rs.OpenRecordset
    
    If Not (rsFiltered.EOF And rsFiltered.BOF) Then
        rsFiltered.MoveLast
        intRsFiltered = rsFiltered.RecordCount
        rsFiltered.MoveFirst
        For i = LBound(arrKonfig) To UBound(arrKonfig)
            arrMerge(i, 0) = -1
        Next i
        ReDim arrDaten(UBound(arrKonfig))
        Do Until rsFiltered.EOF = True
            If intRSReihe = 2 Then
                rsFiltered2.MoveFirst
            End If

            For i = LBound(arrKonfig) To UBound(arrKonfig)
                If arrKonfig(i, 0) = "PLATZHALTER1" Then
                    arrDaten(i) = strPlatzhalter1
                ElseIf arrKonfig(i, 0) = "PLATZHALTER2" Then
                    arrDaten(i) = strPlatzhalter2
                ElseIf arrKonfig(i, 0) = "PLATZHALTER3" Then
                    arrDaten(i) = strPlatzhalter3
                Else
                    If arrKonfig(i, 3) = "x" And rsFiltered.Fields(arrKonfig(i, 0)) = 0 Then
                        arrDaten(i) = strFuellzeichen
                    Else
                        arrDaten(i) = IIf(rsFiltered.Fields(arrKonfig(i, 0)) <> "", rsFiltered.Fields(arrKonfig(i, 0)), strFuellzeichen)
                    End If
                End If
                    
                'Farbe
                'TODO in Vorlage einbauen
                If arrKonfig(i, 2) <> "" Then
                    worksheetTab.Cells(indexReihe, indexSpalte).Interior.Color = getRGB(IIf(IsNull(rsFiltered.Fields(arrKonfig(i, 2))), "255.255.255", rsFiltered.Fields(arrKonfig(i, 2))))
                End If

                'Reihen verbinden
                If intRSReihe >= 2 Then
                    If arrKonfig(i, 1) = "JA" Then
                        If rsFiltered.Fields(arrKonfig(i, 0)) = rsFiltered2.Fields(arrKonfig(i, 0)) Then
                            arrMerge(i, 0) = IIf(arrMerge(i, 0) = -1, indexReihe - 1, arrMerge(i, 0))
                            arrMerge(i, 1) = arrDaten(i)
                        ElseIf arrMerge(i, 0) >= 0 Then
                            ' vorhergehend gesetzte verbinden
                            With worksheetTab.Range(worksheetTab.Cells(arrMerge(i, 0), indexSpalte), worksheetTab.Cells(indexReihe - 1, indexSpalte))
                                .MergeCells = True
                            End With
                            arrMerge(i, 0) = -1
                        End If
                    ElseIf arrKonfig(i, 1) <> "NEIN" And arrKonfig(i, 1) <> "" And arrKonfig(i, 1) <> "JA" Then 'Reihen verbinden in Abh�ngigkeit mit Spalte
                        If rsFiltered.Fields(arrKonfig(i, 1)) = rsFiltered2.Fields(arrKonfig(i, 1)) Then
                            'worksheetTab.Range(worksheetTab.Cells(indexReihe - 1, indexSpalte), worksheetTab.Cells(indexReihe, indexSpalte)).MergeCells = True
                            arrMerge(i, 0) = IIf(arrMerge(i, 0) = -1, indexReihe - 1, arrMerge(i, 0))
                            arrMerge(i, 1) = arrDaten(i)
                        ElseIf arrMerge(i, 0) >= 0 Then
                            ' vorhergehend gesetzte verbinden
                            With worksheetTab.Range(worksheetTab.Cells(arrMerge(i, 0), indexSpalte), worksheetTab.Cells(indexReihe - 1, indexSpalte))
                                .MergeCells = True
                            End With
                            arrMerge(i, 0) = -1
                        End If
                    End If
                End If

                indexSpalte = indexSpalte + 1
            Next i
            
            For i = LBound(arrDaten) To UBound(arrDaten)
                'ASSE-924 Mx mdlExcel.erstelleExcelExport auch leere Spalten bzw. �berspringen
                If arrDaten(i) = "SKIP_FIELD" Then
                    'ge�ndert 31.05.2023, KAQ: .Formula statt .Value
                    arrDaten(i) = worksheetTab.Cells(indexReihe, i + 1).Formula
                End If
            Next i
            'Daten Zeilenweise schreiben
            'KAQ, 21.01.2022: ASSE-880 M0 Excel Export fehlt letzte Spalte
'            worksheetTab.Range(worksheetTab.Cells(indexReihe, 1), worksheetTab.Cells(indexReihe, UBound(arrKonfig))).Value = arrDaten
            worksheetTab.Range(worksheetTab.Cells(indexReihe, 1), worksheetTab.Cells(indexReihe, UBound(arrKonfig) + 1)).Value = arrDaten
            
            For i = LBound(arrDaten) To UBound(arrDaten)
                'ASSE-1020 Mx mdlExcel.erstelleExcelExport Aufeinander folgende Zellen in Zeile verbinden, wenn Werte gleich oder leer
                If arrKonfig(i, 4) = "JA" And UBound(arrDaten) > i Then
                    If arrDaten(i) = arrDaten(i + 1) Then
                        worksheetTab.Range(worksheetTab.Cells(indexReihe, i + 1), worksheetTab.Cells(indexReihe, i + 2)).Merge
'                        arrMerge(i, 0) = IIf(arrMerge(i, 0) = -1, indexReihe - 1, arrMerge(i, 0))
'                        arrMerge(i, 1) = arrDaten(i)
                    End If
                End If
            Next i
            
            ' Fortschritt anzeigen mit Daten aus erster Spalte
            Call Fortschritt(intRSReihe / intRsFiltered * 100, "Exportiert wird " & intRSReihe & " von " & intRsFiltered)
            indexReihe = indexReihe + 1
            indexSpalte = 1
            rsFiltered.MoveNext
            If intRSReihe > 1 Then
                rsFiltered2.MoveNext
            End If
            intRSReihe = intRSReihe + 1
        Loop
    End If
    
    'Datenbankobjekte bereinigen
    rsFiltered.Close
    rsFiltered2.Close
    rs.Close
    
    ' Orientierung wieder setzen
    Dim firstAddress As Variant
    For i = LBound(arrKonfig) To UBound(arrKonfig)
         worksheetTab.Range(Cells(indexStartReihe, i + 1), Cells(indexReihe, i + 1)).HorizontalAlignment = arrOrientation(i)
    Next i
    ' F�llzeichen zentrieren
    If boolZentriereFuellzeichen Then
        With worksheetTab.Range(Cells(indexStartReihe, 1), Cells(indexReihe, UBound(arrKonfig)))
            Set cellFind = .Find(strFuellzeichen)
            If Not cellFind Is Nothing Then
                firstAddress = cellFind.Address
                Do
                ' Fortschritt anzeigen mit Daten aus erster Spalte
                    If arrOrientation(cellFind.Column - 1) <> xlCenter Then
                        Call Fortschritt(cellFind.Row / indexReihe * 100, "F�llzeichen zentriert: " & cellFind.Row & " von " & indexReihe)
                        cellFind.HorizontalAlignment = xlCenter
                    End If
                    Set cellFind = .FindNext(cellFind)
                Loop While Not cellFind Is Nothing And cellFind.Address <> firstAddress
            End If
        End With
    End If
    
    'Konfig und Template Reiter l�schen
    workbook.Sheets("CONFIG").Delete
    workbook.Sheets("TEMPLATE").Delete
    workbook.Sheets("DATA").Delete
    appExcel.DisplayAlerts = True
    appExcel.Calculation = xlCalculationAutomatic
    
    'Speichern und schlie�en
    ' Jahr ignorieren wenn = 0
    strDateiname = strModul & IIf(intJahr <> 0, "_" & intJahr, "") & "_" & Format(DateTime.Now, "yyMMddhhmm") & ".xlsx"
    
    If Dir(strAblage, vbDirectory) = "" Then
        MkDir (strAblage)
        MsgBox "Verzeichnis '" & strAblage & "' wurde angelegt!"
    End If
    On Error Resume Next
    workbook.SaveAs (strAblage & strDateiname)
    If Err.Number <> 0 Then
        erstelleExcelExport = False
        On Error GoTo 0
        workbook.Close SaveChanges:=False
    Else
        erstelleExcelExport = True
        On Error GoTo 0
        workbook.Close
    End If
    Call Fortschritt(101, "Meldung")
    Exit Function
RecordsetError:
    Call Fortschritt(101, "Meldung")
    erstelleExcelExport = False
    MsgBox "Die Excel Vorlage wurde so definiert, dass folgende DB-Abfrage nicht ausgef�hrt werden konnte:" & vbCrLf & strAbfrage, vbCritical
    workbook.Close SaveChanges:=False
End Function

'**************************************************************************************
'Gibt den Inhaltsbereich (Range) eines Platzhalters in der Konfig zur�ck
'erstellt am: 11.05.2017
'erstellt von: Tobias Langer
'R�ckgabewert: String
'**************************************************************************************
Public Function getRangeAusExcel(strDateipfad As String, strPlatzhalter As String) As String
    If Dir(strDateipfad) <> "" Then
        Dim appExcel As Excel.Application
        Dim workbook As Excel.workbook
        Dim worksheetKonfig As Excel.worksheet
        Dim intIndexKonfig As Integer
        Dim strIndexKonfig As String
        
        intIndexKonfig = 1
        
        Set appExcel = Excel.Application
        appExcel.DisplayAlerts = False
        Set workbook = appExcel.Workbooks.Open(strDateipfad)
        Set worksheetKonfig = workbook.Worksheets("CONFIG") 'CONFIG Reiter
        
        'Suche nach Platzhalter, stelle Koordinaten fest und lie� Inhalte aus
        Do
            strIndexKonfig = "A" & intIndexKonfig
            If worksheetKonfig.Range(strIndexKonfig).Value = strPlatzhalter Then
                getRangeAusExcel = worksheetKonfig.Range("B" & intIndexKonfig).Value
                Exit Do
            ElseIf worksheetKonfig.Range(strIndexKonfig).Value = "" Then
                getRangeAusExcel = ""
                Exit Do
            End If
            intIndexKonfig = intIndexKonfig + 1
        Loop
        workbook.Close
    End If
End Function


'**************************************************************************************
'Erzeugt einen GWB Excel Bericht anhand eines Jahres, Ablageverzeichnis, Excelvorlage, Modulnamen und einer Abfrage
'Die Abfrage kann variabel aufgebaut sein, wenn die ben�tigten Felder f�r den Excelbericht in den letzten 4 Spalten, entsprechend sortiert aufgef�hrt sind
'erstellt am: 11.05.2017
'erstellt von: Tobias Langer
'R�ckgabewert: Boolean
'ge�ndert: ASSE-194, 01.06.2017, TL: Erg�nzt um Ablageverzeichnis f�r Speicherung
'ge�ndert: ASSE-191, 01.06.2017, TL: Erweiterung um Monat im Zeitraum
'ge�ndert: ASSE-860, 29.06.2021, KAQ: M2 Export f�r GWB:
'          Fortschritt, optionale Spaltenanzahl, tblGWBBemerkung pr�fen, Alarmwert=120 nur Radon, Alarmwert auch direkt in Template, intIndexReihe falls keine Daten
'ge�ndert: ASSE-863, 19.07.2021, KAQ: M1 Alarmwert direkt in Template �bernehmen
'ge�ndert: ASSE-866, 19.07.2021, KAQ: M2 Alarmwert direkt in Template �bernehmen
'ge�ndert: 05.10.23 KAQ: ASSE-1092 M0 Abfragen f�r Reports nur lesend, siehe Excel Export
'**************************************************************************************
Public Function erstelleGWBAlsExcel(intJahr As Integer, strMonat As String, strAblage As String, strVorlage As String, strModul As String, strAbfrage As String, Optional intSpalten = 4) As Boolean
    Dim strIndexTitel As String
    Dim strIndexBeschreibung As String
    Dim strIndexBemerkung As String
    Dim strIndexMessintervall As String
    Dim strIndexTabelleTitel As String
    Dim strIndexTabelleDaten As String
    Dim strMessstelleKuerzel As String
    Dim strIndexAlarmwert As String
    Dim strDateiname As String
    Dim strIndexDaten As String
    Dim strBemerkung As String
    
    Dim i As Long
    Dim intIndexSpalte As Integer
    Dim intIndexReihe As Long
    Dim intSpaltenAuswertung As Integer
    Dim intAlarmwert As Integer
    Dim intFortschrittStep, intFortschritt As Integer
    
    Dim appExcel As Excel.Application
    Dim workbook As Excel.workbook
    Dim worksheetMessstelle As Excel.worksheet
    
    Dim rs As DAO.Recordset
    Dim rsFiltered As DAO.Recordset
    
    erstelleGWBAlsExcel = False
    If intJahr > 0 And strVorlage <> "" And strModul <> "" And strAbfrage <> "" Then
        Fortschritt 10, "GWB erstellen"
        ' Alarmwert nur Radon auf 120
        intAlarmwert = IIf(strModul = "Radon", 120, 0)
        
        'PlatzhalterIndex ermitteln
        strIndexTitel = getRangeAusExcel(strVorlage, "TITEL")
        strIndexBeschreibung = getRangeAusExcel(strVorlage, "BESCHREIBUNG")
        strIndexTabelleTitel = getRangeAusExcel(strVorlage, "TABELLE_TITEL")
        strIndexTabelleDaten = getRangeAusExcel(strVorlage, "TABELLE_DATEN")
        strIndexBemerkung = getRangeAusExcel(strVorlage, "BEMERKUNG")
        strIndexMessintervall = getRangeAusExcel(strVorlage, "MESSINTERVALL")
        strIndexAlarmwert = getRangeAusExcel(strVorlage, "ALARMWERT")
        
        'Vorlage �ffnen
        Set appExcel = Excel.Application
        Set workbook = appExcel.Workbooks.Open(strVorlage)
        
        'Daten verwenden mit �bergebener Abfrage und optionalen Filter
        'ASSE-1092 M0 Abfragen f�r Reports nur lesend, siehe Excel Export
        Set rs = CurrentDb.OpenRecordset(strAbfrage, dbOpenSnapshot)
    '    Set rs = CurrentDb.OpenRecordset(strAbfrage, dbOpenDynaset, dbSeeChanges)
        
        
        If strMonat <> "" Then
            rs.Filter = "Monat BETWEEN '" & intJahr & "01' AND '" & intJahr & strMonat & "'"
        Else
            rs.Filter = "Jahr = " & intJahr
        End If
        
        Set rsFiltered = rs.OpenRecordset
    
        Fortschritt 10, "Daten schreiben"
        
        If Not (rsFiltered.EOF And rsFiltered.BOF) Then
            rsFiltered.MoveLast
            intFortschrittStep = 80 / rsFiltered.RecordCount * IIf(strMonat <> "", Val(strMonat), 12)
            intFortschritt = 10
            rsFiltered.MoveFirst
            intSpaltenAuswertung = intSpalten
            Do Until rsFiltered.EOF = True
                
                'Vorbereitung neue Messstelle in Dokument
                If strMessstelleKuerzel <> rsFiltered.Fields(0) Then
                    strMessstelleKuerzel = rsFiltered.Fields(0)
                    
                    'Fortschritt anzeigen
                    Fortschritt intFortschritt, strMessstelleKuerzel & " erstellen"
                    intFortschritt = IIf(intFortschritt + intFortschrittStep <= 90, intFortschritt + intFortschrittStep, 90)
                    
                    'Index z.B. B5 aufsplitten und aufbereiten zum sp�teren hochz�hlen
                    intIndexSpalte = Asc(Left(strIndexTabelleDaten, 1))
                    intIndexReihe = Right(strIndexTabelleDaten, 1)
                    
                    'Neuen Reiter erstellung und benennen
                    workbook.Sheets("TEMPLATE").Copy After:=workbook.Sheets(workbook.Sheets.count)
                    workbook.Sheets(workbook.Sheets.count).Name = Replace(rsFiltered.Fields(0), "/", "_")
                    Set worksheetMessstelle = workbook.Worksheets(workbook.Sheets.count)
                    
                    'Standardplatzhalter ersetzen
                    If strIndexTitel <> "" And rsFiltered.Fields(1) > 0 Then
                        worksheetMessstelle.Range(strIndexTitel).Value = DLookupStringWrapper("Bezeichnung", "tblMessstelle", "Messstelle_ID = " & rsFiltered.Fields(1)) & " (" & rsFiltered.Fields(0) & ")"
                    Else
                        worksheetMessstelle.Range(strIndexTitel).Value = ""
                    End If
                    
                    If strIndexTabelleTitel <> "" And rsFiltered.Fields(1) > 0 Then
                        worksheetMessstelle.Range(strIndexTabelleTitel).Value = DLookupStringWrapper("Bezeichnung", "tblMessstelle", "Messstelle_ID = " & rsFiltered.Fields(1))
                    Else
                        worksheetMessstelle.Range(strIndexTabelleTitel).Value = ""
                    End If
                    
                    If strIndexBeschreibung <> "" And rsFiltered.Fields(1) > 0 Then
                        worksheetMessstelle.Range(strIndexBeschreibung).Value = DLookupStringWrapper("Bemerkung", "tblMessstelle", "Messstelle_ID = " & rsFiltered.Fields(1))
                    Else
                        worksheetMessstelle.Range(strIndexBeschreibung).Value = ""
                    End If
                    ' Bemerkung setzen, wenn Tabelle vorhanden und Wert gesetzt
                    If strIndexBemerkung <> "" And rsFiltered.Fields(1) > 0 And DLookupStringWrapper("Name", "MSysObjects", "Name = 'tblGWBBemerkung'", "") > "" Then
                        worksheetMessstelle.Range(strIndexBemerkung).Value = DLookupStringWrapper("Bemerkung", "tblGWBBemerkung", "Messstelle_ID_f = " & rsFiltered.Fields(1) & " AND Jahr =" & intJahr, "")
                    End If
                    
                    If strIndexMessintervall <> "" And rsFiltered.Fields(1) > 0 Then
                        worksheetMessstelle.Range(strIndexMessintervall).Value = DLookupStringWrapper("Messintervall", "tblMessstelle", "Messstelle_ID = " & rsFiltered.Fields(1))
                    Else
                        worksheetMessstelle.Range(strIndexMessintervall).Value = ""
                    End If
                    ' Alarmwert nur wenn Referenz auf ein Feld und keine Zahl
                    ' ASSE-863 M1 Alarmwert direkt in Template �bernehmen
                    If strIndexAlarmwert <> "" And rsFiltered.Fields(1) > 0 Then
                        If UCase(Left(strIndexAlarmwert, 1)) >= "A" And UCase(Left(strIndexAlarmwert, 1)) <= "Z" Then
                            worksheetMessstelle.Range(strIndexAlarmwert).Value = intAlarmwert
                        End If
                    End If
                End If
                
                'Tabelle mit Daten f�llen
                If strMessstelleKuerzel = rsFiltered.Fields(0) And workbook.Sheets(workbook.Sheets.count).Name <> "" Then
                    Dim intIndexFor As Long
                    intIndexFor = 0
                    For i = (rsFiltered.Fields.count - intSpaltenAuswertung) To (rsFiltered.Fields.count - 1)
                        '30.06.21, KAQ: Position abh�ngig vom Monat
                        strIndexDaten = Chr(intIndexSpalte + intIndexFor) & (intIndexReihe + Val(Right(rsFiltered.Fields(4), 2)) - 1)
                        worksheetMessstelle.Range(strIndexDaten).Value = rsFiltered.Fields(i)
                        intIndexFor = intIndexFor + 1
                    Next i
                End If
                
'                intIndexReihe = intIndexReihe + 1
                rsFiltered.MoveNext
            Loop
        End If
        Set rs = Nothing
        
        Fortschritt 90, "Datei bereinigen"
        
        'Konfig und Template Reiter l�schen
        appExcel.DisplayAlerts = False
        workbook.Sheets("CONFIG").Delete
        workbook.Sheets("TEMPLATE").Delete
        appExcel.DisplayAlerts = True
        
        'Speichern und schlie�en
        strDateiname = "GWB_" & intJahr & "_" & strModul & ".xlsx"
        
        If Dir(strAblage, vbDirectory) = "" Then
            MkDir (strAblage)
'            MsgBox "Verzeichnis '" & strAblage & "' wurde angelegt!"
        End If
        
        Fortschritt 95, "Datei speichern"
        
        workbook.SaveAs (strAblage & strDateiname)
        workbook.Close
        Fortschritt 100, "GWB erstellt"
        Fortschritt 101, "GWB abgeschlossen"
        
        erstelleGWBAlsExcel = True
    End If
End Function

'**************************************************************************************
'F�gt einer Excel Datei die Module f�r das Erstellen von Barcodes (Aztec, QRCode, Code128, DataMatrix) hinzu
'erstellt am: 04.04.2024
'erstellt von: Kai A. Quante
'Parameter:
' - strVBAPath : Dateiname, wird bei Bedarf in .xlsm gespeichert
'Parameter (optional):
' - boolQRcode : Draw QR code, Default = true
' - boolAztec  : Draw Aztec barcode, Default = false
' - boolCode128 : Draw Code 128 barcode, Default = false
' - boolDataMatrix : Draw DataMatrix barcode, Default = false
'R�ckgabewert: Dateiname mit Macro oder "" f�r Fehler
'Funktionsparameter:
'Application.MacroOptions macro:="QRCode", Description:="Draw QR code", Category:="Barcode", ArgumentDescriptions:=arg
'Application.MacroOptions macro:="Aztec", Description:="Draw Aztec barcode", Category:="Barcode", ArgumentDescriptions:=arg
'arg(1) = "security level ""LMQH""" + vbCrLf + "low, medium, quartile, high" + vbCrLf + "letter, optional, default L"
'arg(2) = "minimum version size(-3..40)" + vbCrLf + "number, optional, default 1" + vbCrLf + "MircoQR M1:-3, M2:-2, M3:-1, M4:0"
'Application.MacroOptions macro:="Code128", Description:="Draw Code 128 barcode", Category:="Barcode", ArgumentDescriptions:=arg
'Application.MacroOptions macro:="DataMatrix", Description:="Draw DataMatrix barcode", Category:="Barcode", ArgumentDescriptions:=arg
'arg(1) = "percentage of checkwords (1..90)" + vbCrLf + "number, optional, default 23%"
'arg(2) = "minimum number of layers (0-32)" + vbCrLf + "number, optional, default 1" + vbCrLf + "set to 0 for Aztec rune"
'ge�ndert 23.05.2024, KAQ: ASSE-1123 M0 QR-Code in Messprotokoll f�r Labor
'**************************************************************************************
Public Function AddBarcodeToExcel(strVBAPath As String, Optional boolQRcode As Boolean = True, Optional boolAztec As Boolean = False, _
                              Optional boolCode128 As Boolean = False, Optional boolDataMatrix As Boolean = False) As String
    Dim xlApp As Object
    Dim xlWorkbook As Object
    Dim xlModule As Object
    Dim vbComponent As Object
    Dim i As Integer
    
    Dim strVBAContent As String
    Dim strModulName As String
    Dim strCopyright As String
    
    ' Excel-Anwendung �ffnen
    Set xlApp = CreateObject("Excel.Application")
    ' Excel-Datei �ffnen
    Set xlWorkbook = xlApp.Workbooks.Open(strVBAPath)
    
    
    'Fehlermeldung: "Der programmatische Zugriff auf das Visual Basic-Projekt ist nicht sicher."
On Error GoTo Err

    ' Barcode Basismodul
    AddBarcodeToExcelModul "Barcode", xlWorkbook
    'QR-Code
    If boolQRcode Then
        AddBarcodeToExcelModul "QRcode", xlWorkbook
    End If
    'Aztec
    If boolAztec Then
        AddBarcodeToExcelModul "Aztec", xlWorkbook
    End If
    'Code128
    If boolCode128 Then
        AddBarcodeToExcelModul "Code128", xlWorkbook
    End If
    'DataMatrix
    If boolDataMatrix Then
        AddBarcodeToExcelModul "DataMatrix", xlWorkbook
    End If
    On Error GoTo 0
    
    ' Excel-Datei als ".xlsm" speichern
    If Right(strVBAPath, 4) <> "xlsm" Then
        strVBAPath = Left(strVBAPath, InStrRev(strVBAPath, ".")) & "xlsm"
    End If
    xlWorkbook.SaveAs strVBAPath, 52 ' 52 entspricht xlOpenXMLWorkbookMacroEnabled (xlsm-Format)

    ' Excel-Datei schlie�en
    xlWorkbook.Close False

    ' Excel-Anwendung beenden
    xlApp.Quit

    ' Objekte freigeben
    Set xlModule = Nothing
    Set xlWorkbook = Nothing
    Set xlApp = Nothing
    
    AddBarcodeToExcel = strVBAPath
    Exit Function
Err:
    ' TrustCenter
    If Err.Number = 1004 Then
        MsgBox "Sie m�ssen unter Datei - Optionen - Trust Center - Einstellungen f�r das Trust Center - Makroeinstellungen die Option 'Zugriff auf das VBA-Projektobjektmodell vertrauen' ausw�hlen.", vbOKOnly + vbExclamation, "Trust Center"
    Else
        MsgBox "Es ist ein Fehler " & Err.Number & " aufgetreten:" & vbCrLf & Err.Description, vbOKOnly + vbExclamation, "Error"
    End If
    On Error Resume Next
    ' Excel-Datei schlie�en
    xlWorkbook.Close False

    ' Excel-Anwendung beenden
    xlApp.Quit

    ' Objekte freigeben
    Set xlModule = Nothing
    Set xlWorkbook = Nothing
    Set xlApp = Nothing
    AddBarcodeToExcel = ""
End Function

'**************************************************************************************
'F�gt einer Excel Datei das Module f�r das Erstellen von Barcodes (Aztec, QRCode, Code128, DataMatrix) hinzu
'erstellt am: 02.07.2024
'erstellt von: Kai A. Quante
'Parameter:
' - strModulName : Modulname
' - xlWorkbook : ge�ffnete Excel-Datei
'R�ckgabewert: void
'ASSE-1172 M0 QR-Code doppelte Option Explicit
'ASSE-1179 M0 Updates f�r QR-Codes
'**************************************************************************************
Private Sub AddBarcodeToExcelModul(strModulName As String, ByVal xlWorkbook As Object)
    Dim xlModule As Object
    Dim strVBAContent As String
    Dim strCopyright As String
    Dim vbComponent As Object
    
    strCopyright = DLookupStringWrapper("Sourcecode", "tblBarcode", "Modul = 'Copyright'", "")
    
    ' Falls das Modul bereits existiert, l�schen
    For Each vbComponent In xlWorkbook.VBProject.VBComponents
        If vbComponent.Name = "Modul" & strModulName Then
            xlWorkbook.VBProject.VBComponents.Remove vbComponent
            Exit For
        End If
    Next vbComponent
    ' Neues Modul in Excel-Datei einf�gen
    Set xlModule = xlWorkbook.VBProject.VBComponents.Add(1)
    ' VBA-Modul name vergeben
    xlModule.Name = "Modul" & strModulName
    'Source Code generieren
    strVBAContent = strCopyright & vbCrLf & DLookupStringWrapper("Sourcecode", "tblBarcode", "Modul = '" & strModulName & "'", "")
    'Option Explicit erg�nzen, wenn nicht vorhanden
    If Trim(Nz(xlModule.codemodule.Lines(1, 1), "")) <> "Option Explicit" Then
        strVBAContent = "Option Explicit" & vbCrLf & strVBAContent
    End If
    ' VBA-Code in das Modul einf�gen
    xlModule.codemodule.AddFromString strVBAContent
End Sub



