Attribute VB_Name = "mdlImport"
Option Compare Database
Option Explicit

'**********************************************************
' Modul enth�lt Hilfsfunktionen f�r Import der Textdateien
' 22.05.2019, KAQ: ASSE -592 M2 Messunsicherheit in Prozent und absolut
' 23.05.2019, KAQ: ASSE-599 M2 Bisher nicht ben�tigten Felder aus CSV importieren
' 02.08.2019, KAQ: ASSE-620 M6 Gamma-Spe einlesen
' 05.08.2019, KAQ: ASSE-616 M2 Spektrum / Aktivit�t zur Probe �bernehmen
' 27.08.2019, KAQ: ASSE-679 M6 Gamma-Messdaten k�nnen nicht eingelesen werden
' 27.08.2019, KAQ: ASSE-676 M6 Startmonat f�r Import
' 27.08.2019, KAQ: ASSE-680 M6 Probenahmeunsicherheiten importieren
' 03.09.2019, KAQ: ASSE-694 M2 Rollback vs. BeginTrans in performImportCSV
' 08.08.2022, KAQ: ASSE-908 M6 Probe ID nicht am Anfang vom Kommentar
' 11.08.2022, KAQ: ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6. V2.4.0
' 28.09.2022, KAQ: ASSE-929 M6 Mehrere Pfade f�r einen Parameter in ini-Datei
' 28.09.2022, KAQ: ASSE-940 M2 Mehrere Pfade f�r einen Parameter in ini-Datei
'2.5.0 29.09.2022, KAQ: ASSE-933 M6 Neueste Spektrum f�r Probe verwenden
'2.5.0 29.09.2022, KAQ: ASSE-934 M2 Neueste Spektrum verwenden
'2.5.1 29.09.2022, KAQ: ASSE-937 M2 Performance Import
'2.5.2 30.09.2022, KAQ: Tests, FileCount, Debug-Anpassungen
'2.5.3 05.10.2022, KAQ: Counts, Pfad-Array f�r Import
'2.5.4 05.10.2022, KAQ: Version
'2.5.5 10.10.2022, KAQ: ASSE-950 M6 Fehler bei langen Dateinamen: passen nicht zu PATTERN und werden eingelesen => fitPattern
'2.5.6 13.10.2022, KAQ: ASSE-946 M6 Wasserprobe: Probenmenge in kg/l vs. Bezugsmenge m� im Labor (Formel fix)
'2.6.0 15.11.2022, KAQ: M6 v 1.3.0
'2.7.1 23.11.2022, KAQ: M2 v 1.4.0
'2.7.2 08.12.2022, KAQ: getKonfiguration("IMPORT_GAMMA_LAUGE") = "ONLY"
'2.7.3 19.12.2022, KAQ: M6 v 1.3.1
'                       ASSE-984 M6 AE Mengen Ganzzahlung in Messung entspricht Rundung aus Probe
'                       ASSE-985 M6 Differenzanzeige mit Bezugsmenge bei Wasserprobe
'                       ASSE-965 M6 Warnmeldungen
'2.8.0 23.01.2023, KAQ: M0 v 1.3.0
'2.9.0 23.01.2023, KAQ: Methoden/Funktionen an mdlGlobal abgegeben (!!!)
'3.0.0 11.08.2023, KAQ: ASSE-436 M5 _Low Level Messung
'3.1.0 04.10.2023, KAQ: M2 v 1.5.1, M6 v 1.5.1, M5 v 1.0.0
'                       ASSE-1083 M2 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'                       ASSE-1084 M6 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'3.1.1 10.10.2023, KAQ: ASSE-1095 M6 Fehler Import vom 09.10.2023
'3.1.2 10.10.2023, KAQ: ASSE-1098 M2 Import: Probe ID nur aus erster Spalte und nicht aus Kommentar
'3.1.3 14.02.2024, KAQ: ASSE-1108 M6 Anzeige Bezugsmenge nur Wasserproben
'3.1.4 13.03.2024, KAQ: M6 1.5.5
'                       ASSE-1101 M6 Reload setzt file:/// als Dokument-Link
'                       ASSE-1126 M6 Spektrum neu einlesen
'                       ASSE-1134 M6 Differenz Probe vs. Spektrum bei AE St�rfallproben
'                       ASSE-1136 M6 TXT Dateien nicht in .csv kopieren
'                       ASSE-986 M6 Verschobener Dateipfad f�r Vergleich oder Neueinlesen
'                       ASSE-1129 M6 Aktualisierung Differenz Probe vs. Spektrum per Button
'                       ASSE-1132 M6 Export aktuelle Differenz Daten aus Probe und Spektrum
'**********************************************************

Private Const Version As String = "3.1.4"

Private Const cTabelle As Integer = 1
Private Const cDatei As Integer = 2
Private Const TRITIUM_VERSION As Integer = cDatei

Public Type TGamma_Unsicherheit
    Bezeichnung As String
    Wert As String
End Type

Public Type TGamma_Kopf
    Proben_ID As Long
    Spektrum As String
'    Detektor As String
'    Messzeit As String
'    Probenalter As String
    Masse_Volumen As String
    Kommentar As String
    EinheitMesserg As String
    BeginnPN As Date
    EndePN As Date
'   Probenahmeunsicherheit As String
    Bezugsdatum As Date
'    Messdatum As Date
'    Auswertedatum As Date
    AbweichungProbe As Boolean
    Unsicherheit() As TGamma_Unsicherheit
    Messstelle_Lauge As String
End Type

Public Type TGamma_Tabelle
    ZeilenNr As Long
    Isotop As String
    BSchaetzer As Double
    FehlerProz As Double
    FehlerAbs As Double
    UVG As Double
    OVG As Double
    EKG As Double
    NWG As Double
End Type

Private Const GAMMA_TRENN_MINUS As String = " - "
Private Const GAMMA_TRENN_SPALTE As String = "|"
Private Const GAMMA_TRENN_PLUS_MINUS As String = " � "
Private Const GAMMA_KENN_EINHEIT As String = "|   |"

Private Type TKeyValue
    Key As String
    Value As String
End Type

Private strTableColumn() As TKeyValue

'**************************************************************************************
'Funktion zum Auslesen der Version.
'erstellt am: 05.10.2022
'erstellt von: Kai A. Quante
'R�ckgabewert: Version
'**************************************************************************************
Public Function getImportVersion() As String
  getImportVersion = Version
End Function


'**************************************************************************************
'Liefert einen Wert aus der 2. Spalte einer Textdatei zur�ck,
'wenn die 1. Spalte gleich dem Schl�ssel (strSchluessel) ist.
'Optionale Parameter:
'strSplit = Spaltentrenner beim Einlesen
'strBorderLeft = Text wird rechts vom Parameter aus Spalte 2 genommen.
'strBorderRight = Text wird links vom Parameter aus Spalte 2 genommen.
'erstellt von: Kai A. Quante
'erstellt am: 25.01.2019
'return: String , gefundener Wert sonst leere Zeichenkette
'ge�ndert: 09.07.2019, KAQ: Trenner, Mehrzeilige Daten
'ge�ndert: 29.08.2019, KAQ:_ASSE-683 M6 Sonderzeichen in Keyw�rtern ersetzen f�r Feldnamen
'**************************************************************************************
Public Function getKeyValueFromImportFile(strFile As String, strSchluessel As String, Optional strSplit As String = ";", _
                Optional strBorderLeft As String = "", Optional strBorderRight As String = "", Optional boolMehrzeilig As Boolean = False, Optional boolEntferneLeerzeichen As Boolean = True) As String
    Dim i As Integer
    Dim j As Integer
    j = 0
    i = 1
    Dim LineFromFile As String
    Dim LineItems() As String
    Dim intPos As Integer
    ReDim LineItems(2)
    getKeyValueFromImportFile = ""
    If Dir(strFile) <> "" Then
        'Datei zeilenweise einlesen
        Open strFile For Input As #1
            Do Until EOF(1)
                Line Input #1, LineFromFile
                intPos = InStr(LineFromFile, strSplit)
                If intPos > 0 Then
                    LineItems(0) = Trim(Left(LineFromFile, intPos - 1))
                    LineItems(0) = IIf(boolEntferneLeerzeichen, entferneSonderzeichen(LineItems(0)), LineItems(0)) ' Sonderzeichen entfernen
                    LineItems(1) = Trim(Mid(LineFromFile, intPos + 1))
                    If LineItems(0) = strSchluessel Then
                        getKeyValueFromImportFile = getStringData(LineItems(1), strBorderLeft, strBorderRight)
                        If boolMehrzeilig Then
                            j = i
                        End If
                    End If
                ElseIf i > 1 And j = i - 1 And boolMehrzeilig Then
                    If Trim(Trim(LineFromFile)) <> "" Then
                        getKeyValueFromImportFile = getKeyValueFromImportFile & " " & Trim(LineFromFile)
                    Else
                        boolMehrzeilig = False
                    End If
                    j = i
                End If
                i = i + 1
            Loop
        Close #1
    End If
End Function

'**************************************************************************************
'Liefert einen Wert aus der 2. Spalte eines Text-Array zur�ck,
'wenn die 1. Spalte gleich dem Schl�ssel (strSchluessel) ist.
'Kann genutzt werden, wenn eine Datei zeilenweise eingelesen wurde und
'nicht st�ndig neu ge�ffnet werden soll.
'Mehrzeilig ist m�glich.
'Optionale Parameter:
'strSplit = Spaltentrenner beim Einlesen
'strBorderLeft = Text wird rechts vom Parameter aus Spalte 2 genommen.
'strBorderRight = Text wird links vom Parameter aus Spalte 2 genommen.
'erstellt von: Kai A. Quante
'erstellt am: 09.07.2019
'return: String , gefundener Wert sonst leere Zeichenkette
'ge�ndert: 29.08.2019, KAQ:_ASSE-683 M6 Sonderzeichen in Keyw�rtern ersetzen f�r Feldnamen
'**************************************************************************************
Public Function getKeyValueFromStringArray(strArray() As String, strSchluessel As String, Optional strSplit As String = ";", _
                Optional strBorderLeft As String = "", Optional strBorderRight As String = "", Optional boolMehrzeilig As Boolean = False, Optional boolEntferneLeerzeichen As Boolean = True) As String
    Dim i As Integer
    Dim j As Integer
    j = -2
    Dim LineItems() As String
    Dim intPos As Integer
    ReDim LineItems(2)
    For i = LBound(strArray) To UBound(strArray)
        intPos = IIf(strSplit > "", InStr(strArray(i), strSplit), 0)
        If strSplit = "" And boolMehrzeilig And Trim(strArray(i)) = strSchluessel Then
            j = i
        ElseIf intPos > 0 Then
            LineItems(0) = Trim(Left(strArray(i), intPos - 1))
            LineItems(0) = IIf(boolEntferneLeerzeichen, entferneSonderzeichen(LineItems(0)), LineItems(0)) ' Sonderzeichen entfernen
            LineItems(1) = Trim(Mid(strArray(i), intPos + 1))
            If LineItems(0) = strSchluessel Then
                getKeyValueFromStringArray = getStringData(LineItems(1), strBorderLeft, strBorderRight)
                If boolMehrzeilig Then
                    j = i
                End If
            End If
        ElseIf i > LBound(strArray) And j = i - 1 And boolMehrzeilig Then
            If Trim(strArray(i)) <> "" Then
                getKeyValueFromStringArray = getKeyValueFromStringArray & " " & Trim(strArray(i))
            Else
                boolMehrzeilig = False
            End If
            j = i
        End If
    Next i
End Function

'**************************************************************************************
'Pr�ft ob ein Dateinamen zu einem Pattern passt.
'Problem ist, dass Dir Funktion bei Windows nur bis 8.3 Format geht und z.B. T???????.TXT
'auch l�ngere Dateinamen anerkennen w�rde, die vorne passen.
'erstellt von: Kai A. Quante
'erstellt am: 10.10.2022
'return: Boolean, ob Pattern zum Dateinamen passt.
'ASSE-950 M6 Fehler bei langen Dateinamen: passen nicht zu PATTERN und werden eingelesen => fitPattern
'**************************************************************************************
Public Function fitPattern(strPattern As String, strFileName As String) As Boolean
    fitPattern = False
    Dim regex As New RegExp, Fundstellen As MatchCollection, Fund As Match, i As Integer, strZeichen As String
'    regex.Pattern = "\d\d[.]\d\d\d[.]\d\d\d[^T]?"
    regex.Pattern = ""
    For i = 1 To Len(strPattern)
        strZeichen = Mid(strPattern, i, 1)
        If strZeichen = "?" Then
            regex.Pattern = regex.Pattern & "."
        ElseIf strZeichen = "*" Then
            regex.Pattern = regex.Pattern & ".*"
        ElseIf strZeichen = "." Then
            regex.Pattern = regex.Pattern & "[.]"
        Else
            regex.Pattern = regex.Pattern & "[" & strZeichen & "]"
        End If
    Next i
    regex.MultiLine = False
    regex.Global = False
    Set Fundstellen = regex.Execute(strFileName)
    For Each Fund In Fundstellen
        fitPattern = True
        Exit For
    Next
End Function

'**************************************************************************************
'Z�hlt alle Verzeichnisse in einem Verzeichnis.
'erstellt von: Kai A. Quante
'erstellt am: 03.05.2019
'return: Long , Anzahl Dateien im Verzeichnis
'**************************************************************************************
Public Function countPathsInPath(strPfad As String) As Long
    Dim strDatei As String
    countPathsInPath = 0
    Dim strFileFound As String
    strFileFound = ""
    strDatei = Dir(strPfad, vbDirectory) ' Retrieve the first entry.
    ' Ignore the current directory and the encompassing directory.
    Do While strDatei <> "" And Len(strPfad & strDatei) < 256 ' Start the loop.
        If (GetAttr(strPfad & strDatei) And vbDirectory) = vbDirectory And strDatei <> "." And strDatei <> ".." Then  ' only count if it's directory
            countPathsInPath = countPathsInPath + 1
        End If
        strDatei = Dir    ' Get next entry.
    Loop
End Function

'**************************************************************************************
'Z�hlt alle Dateien in einem Verzeichnis NICHT rekursiv.
'Optionale Parameter:
'strPattern = Datei kann Pattern entsprechen, z.B. "H????????.dat"
'strStart   = Startmonat bezogen auf Pattern, z.B. "1905" hei�t Stellen 2 - 5 m�ssen >= "1905" sein, z.B. H19060013.dat w�rde passen.
'strEnde    = Endemonat bezogen auf Pattern, z.B. "1905" hei�t Stellen 2 - 5 m�ssen >= "1905" sein, z.B. H19060013.dat w�rde passen.
'14.11.2022, KAQ: strStart und strEnde k�nnen auch l�nger sein, dann werden jeweils die entsprechenden Stellen ber�cksichtigt!
'erstellt von: Kai A. Quante
'erstellt am: 25.01.2019
'return: Long , Anzahl Dateien im Verzeichnis
'ge�ndert: 03.05.2019, KAQ Pattern
'27.08.2019, KAQ: ASSE-676 M6 Startmonat f�r Import
'10.10.2022, KAQ: ASSE-950 M6 Fehler bei langen Dateinamen: passen nicht zu PATTERN und werden eingelesen => fitPattern
'08.11.2022, KAQ: ASSE-960 M6 Zeitraum zum Messdaten einlesen eingeben
'14.11.2022, KAQ: ASSE-971 M6 Einlesen nach Bereich von Spektrennummern
'**************************************************************************************
Public Function countFilesInPath(strPfad As String, Optional strPattern As String = "", Optional strStart As String = "", Optional strEnde As String = "") As Long
    Dim strDatei As String
    countFilesInPath = 0
    Dim strFileFound As String
    strFileFound = ""
    strDatei = Dir(IIf(strPattern <> "", strPfad & strPattern, strPfad))  ' Retrieve the first entry.
    ' Ignore the current directory and the encompassing directory.
    Do While strDatei <> "" And strDatei <> "." And strDatei <> ".." ' Start the loop.
        If (GetAttr(strPfad & strDatei) And vbDirectory) <> vbDirectory And IIf(strStart <> "", Val(Mid(strDatei, 2, Len(strStart))) >= Val(strStart), True) And IIf(strEnde <> "", Val(Mid(strDatei, 2, Len(strEnde))) <= Val(strEnde), True) And IIf(strPattern <> "", fitPattern(strPattern, strDatei), True) Then ' only count if it's no directory
            countFilesInPath = countFilesInPath + 1
        End If
        strDatei = Dir    ' Get next entry.
    Loop
End Function

'**************************************************************************************
'Z�hlt alle Dateien in einer Verzeichnisliste NICHT rekursiv.
'Optionale Parameter:
'strPattern = Datei kann Pattern entsprechen, z.B. "H????????.dat"
'strStart   = Startmonat bezogen auf Pattern, z.B. "1905" hei�t Stellen 2 - 5 m�ssen >= "1905" sein, z.B. H19060013.dat w�rde passen.
'erstellt von: Kai A. Quante
'erstellt am: 05.10.2022
'return: Long , Anzahl Dateien in den Verzeichnissen
'08.11.2022, KAQ: ASSE-960 M6 Zeitraum zum Messdaten einlesen eingeben
'**************************************************************************************
Public Function countFilesInPaths(strPfad() As String, Optional strPattern As String = "", Optional strStart As String = "", Optional strEnde As String = "") As Long
    Dim i As Integer
    Dim count As Long
    Dim tmpCount As Long
    count = 0
    For i = LBound(strPfad) To UBound(strPfad)
        tmpCount = countFilesInPath(strPfad(i), strPattern, strStart, strEnde)
        count = count + tmpCount
    Next i
    countFilesInPaths = count
End Function

'**********************************************************
'Z�hlt die Dateien in Pfaden mit Unterverzeichnissen mit einem Pattern und Optional Start.
'Das Pfad Array darf NICHT �berschneidend sein!
'Start 1905 w�re ab 1905, was im Dateinamen festgelegt ist.
'erstellt von: Kai A. Quante
'erstellt am: 30.09.2022
'08.11.2022, KAQ: ASSE-960 M6 Zeitraum zum Messdaten einlesen eingeben
'**********************************************************
Public Function countFilesRecursive(strPaths() As String, strPattern As String, Optional strStart As String = "", Optional strEnde As String = "") As Long
    Dim j, intMaxFiles As Long
    Dim strPfade() As String
    intMaxFiles = 0
    For j = LBound(strPaths) To UBound(strPaths)    ' Start the loop.
        intMaxFiles = intMaxFiles + countFilesInPathRecursive(strPaths(j), strPattern, strStart, strEnde)
    Next j
    countFilesRecursive = intMaxFiles
End Function

'**********************************************************
'Z�hlt die Dateien in einem Pfad mit Unterverzeichnissen mit einem Pattern und Optional Start.
'Das Pfad Array darf NICHT �berschneidend sein!
'Start 1905 w�re ab 1905, was im Dateinamen festgelegt ist.
'erstellt von: Kai A. Quante
'erstellt am: 30.09.2022
'08.11.2022, KAQ: ASSE-960 M6 Zeitraum zum Messdaten einlesen eingeben
'**********************************************************
Public Function countFilesInPathRecursive(strPaths As String, strPattern As String, Optional strStart As String = "", Optional strEnde As String = "") As Long
    Dim i, intMaxFiles As Long
    Dim strPfade() As String
    intMaxFiles = 0
    If strPaths <> "" Then
        strPfade = getPathsFromPath(strPaths)
        For i = LBound(strPfade) To UBound(strPfade)    ' Start the loop.
            intMaxFiles = intMaxFiles + countFilesInPath(strPfade(i), strPattern, strStart, strEnde)
        Next i
    End If
    countFilesInPathRecursive = intMaxFiles
End Function

'**************************************************************************************
'Holt alle Dateien aus einem Verzeichnis.
'Optionale Parameter:
'strPattern = Parameter kann ein Pattern sein, z.B. H???????.csv
'strStart   = Startmonat bezogen auf Pattern, z.B. "1905" hei�t Stellen 2 - 5 m�ssen >= "1905" sein, z.B. H19060013.dat w�rde passen.
'erstellt von: Kai A. Quante
'erstellt am: 28.01.2019
'return: String (), Alle Dateien aus einem Verzeichnis
'ge�ndert: 03.05.2019, KAQ Pattern
'27.08.2019, KAQ: ASSE-676 M6 Startmonat f�r Import
'10.10.2022, KAQ: ASSE-950 M6 Fehler bei langen Dateinamen: passen nicht zu PATTERN und werden eingelesen => fitPattern
'14.11.2022, KAQ: ASSE-971 M6 Einlesen nach Bereich von Spektrennummern
'ge�ndert: KAQ, 05.09.2023
'ASSE-1083 M2 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1085 M5 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1084 M6 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'**************************************************************************************
Public Function getFilesFromPath(strPfad As String, Optional strPattern As String = "", Optional strStart As String = "", Optional strEnde As String = "") As String()
    Dim strDatei() As String
    Dim strFile As String
    Dim intCount As Integer
    intCount = countFilesInPath(strPfad, strPattern, strStart, strEnde)
    ReDim strDatei(intCount)
    If intCount = 0 Then
        getFilesFromPath = strDatei
    Else
        ReDim strDatei(intCount - 1)
        intCount = 0
        strFile = Dir(IIf(strPattern <> "", strPfad & strPattern, strPfad))  ' Retrieve the first entry.
        ' Retrieve the first entry.
        Do While strFile <> "" ' Start the loop.
            ' Ignore the current directory and the encompassing directory.
            If (GetAttr(strPfad & strFile) And vbDirectory) <> vbDirectory And strFile <> "." And strFile <> ".." And IIf(strStart <> "", Val(Mid(strFile, 2, Len(strStart))) >= Val(strStart), True) And IIf(strEnde <> "", Val(Mid(strFile, 2, Len(strEnde))) <= Val(strEnde), True) And IIf(strPattern <> "", fitPattern(strPattern, strFile), True) Then    ' only use if it's no directory
                strDatei(intCount) = strFile
                intCount = intCount + 1
            End If
            strFile = Dir    ' Get next entry.
        Loop
        getFilesFromPath = sortFilesArray(strDatei)
    End If
End Function

'**************************************************************************************
'Sortiert ein Array aus Dateinamen, auf oder absteigend.
'Parameter:
'arr : Array von files
'Optionale Parameter:
'ascending : ob aufsteigende Sortierung
'erstellt von: Kai A. Quante
'erstellt am: 05.09.2023
'ASSE-1083 M2 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1085 M5 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1084 M6 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'**************************************************************************************
Public Function sortFilesArray(strFiles() As String, Optional ascending As Boolean = True) As String()
    Dim i As Integer, j As Integer
    Dim temp As String
    For i = LBound(strFiles) To UBound(strFiles) - 1
        For j = i + LBound(strFiles) To UBound(strFiles)
            If (ascending And StrComp(strFiles(i), strFiles(j), vbTextCompare) > 0) Or (Not ascending And StrComp(strFiles(i), strFiles(j), vbTextCompare) < 0) Then
                ' Tauschen
                temp = strFiles(j)
                strFiles(j) = strFiles(i)
                strFiles(i) = temp
            End If
        Next j
    Next i
    
    ' Das sortierte Array zur�ckgeben
    sortFilesArray = strFiles
End Function

'**************************************************************************************
'Holt alle Verzeichnisse aus einem Verzeichnis inkl. Unterverzeichnisse.
'erstellt von: Kai A. Quante
'erstellt am: 03.05.2019
'return: String (), Alle Verzeichnisse aus einem Verzeichnis
'**************************************************************************************
Public Function getPathsFromPath(strPfad As String) As String()
    Dim strDatei() As String
    Dim strSubDir() As String
    Dim strFile As String
    Dim intCount, i, j, intStart As Long
    intCount = countPathsInPath(strPfad)
    ReDim strDatei(0 To intCount)
    strDatei(0) = strPfad
    If intCount > 0 Then
        intCount = 1
        strFile = Dir(strPfad, vbDirectory) ' Retrieve the first entry.
        ' Retrieve the first entry.
        Do While strFile <> "" ' Start the loop.
            ' Ignore the current directory and the encompassing directory.
            If (GetAttr(strPfad & strFile) And vbDirectory) = vbDirectory And strFile <> "." And strFile <> ".." Then  ' only use if it's no directory
                strDatei(intCount) = strPfad & strFile & "\"
                intCount = intCount + 1
            End If
            strFile = Dir    ' Get next entry.
        Loop
        'Unterverzeichnisse anh�ngen
        For i = 1 To intCount - 1  ' Start the loop.
            If strDatei(i) <> "" And countPathsInPath(strDatei(i)) > 0 Then
                strSubDir = getPathsFromPath(strDatei(i))
                intStart = UBound(strDatei)
                ReDim Preserve strDatei(intStart + UBound(strSubDir))
                For j = 1 To UBound(strSubDir) ' Start the loop.
                    strDatei(intStart + j) = strSubDir(j)
                Next j
            End If
        Next i
    End If
    getPathsFromPath = strDatei
End Function

'**************************************************************************************
'Holt alle Verzeichnisse und Unterverzeichnisse aus einem Verzeichnisarray.
'erstellt von: Kai A. Quante
'erstellt am: 05.10.2022
'return: String (), Alle Verzeichnisse aus nicht �berschneidenden Verzeichnissen
'**************************************************************************************
Public Function getPathsFromPaths(strPfad() As String) As String()
    Dim strPaths() As String
    Dim i As Integer
    ' erste als Ausgangspunkt
    strPaths = getPathsFromPath(strPfad(LBound(strPfad)))
    For i = LBound(strPfad) + 1 To UBound(strPfad)
        If strPfad(i) > "" Then
            strPaths = connectPaths(strPaths, getPathsFromPath(strPfad(i)))
        End If
    Next i
    getPathsFromPaths = strPaths
End Function

'**************************************************************************************
'Holt alle Verzeichnisse und Unterverzeichnisse aus einem Verzeichnisarray.
'erstellt von: Kai A. Quante
'erstellt am: 05.10.2022
'return: String (), Alle Verzeichnisse aus nicht �berschneidenden Verzeichnissen
'**************************************************************************************
Private Function connectPaths(strPfad() As String, strPfadAdd() As String) As String()
    Dim strPaths() As String
    Dim i, j As Integer
    Dim intStart As Integer
    Dim intAnzahl As Integer
    intStart = UBound(strPfad) + 1
    intAnzahl = UBound(strPfadAdd) - LBound(strPfadAdd) + 1
    
    strPaths = strPfad
    ReDim Preserve strPaths(LBound(strPaths) To UBound(strPaths) + intAnzahl)
    j = LBound(strPfadAdd)
    For i = intStart To intStart + intAnzahl - 1
       strPaths(i) = strPfadAdd(j)
       j = j + 1
    Next i
    
    connectPaths = strPaths
End Function

'**************************************************************************************
'Benennt Felder des Imports entsprechend der Konfigdatei strImportTable & "ImportConfig" um.
'Dabei werden Leerzeichen gel�scht.
'strImportTable = Zieltabelle
'strTempImportTable = Quelltabelle
'Optionale Parameter:
'intFieldNameRow = Zeile mit den Feldnamen, 0 = erste Zeile
'erstellt von: Valeria Schumacher
'erstellt am: 25.01.2019
'ge�ndert von: Kai A. Quante
'ge�ndert am: 28.01.2019
'ge�ndert: 13.05.2019, KAQ: return false, wenn alle Felder "Feld*" und keins ge�ndert
'return: boolean
'**************************************************************************************
Public Function renameFieldNames(strImportTable As String, strTempImportTable As String, Optional intFieldNameRow As Integer = 0) As Boolean
    Dim rs As DAO.Recordset
    Dim dbs As DAO.Database
    Dim i As Integer
    Dim fld As DAO.Field
    Dim td As DAO.TableDef
    Dim FldName  As String
    Dim strWhere As String
    Dim strValue As String
    Dim dictFldNames As Dictionary
    Dim boolChanged As Boolean
    boolChanged = False
    
    Set dbs = CurrentDb
    Set dictFldNames = New Dictionary

    Set rs = dbs.OpenRecordset("Select * FROM " & strTempImportTable, dbReadOnly)
    If Not rs.EOF And Not rs.BOF Then
        'intFieldNameRow Datensatz in der Tabelle enth�lt Spaltennamen
        rs.Move intFieldNameRow
        For i = 0 To rs.Fields.count - 1
            Set fld = rs.Fields(i)
            'Alle Leerzeichen entfernen aus dem Datensatz enfernen
            If IsNull(fld.Value) Then
                strValue = ""
            Else
                strValue = Replace(fld.Value, " ", "")
            End If
            strWhere = "Replace(Spaltenname,' ','') = '" & strValue & "'"
            FldName = ""
            'Feldnamen aus Konfig Tabelle auslesen
            FldName = DLookupStringWrapper("Tabellenspalte", strImportTable & "ImportConfig", strWhere)
            If FldName <> "" Then
                'aktuellen Feldnamen und Feldnamen aus Konfig Tabelle in die Dictionary schreiben
                dictFldNames.Add fld.Name, FldName
            ElseIf strValue <> "" Then
                dictFldNames.Add fld.Name, fld.Name
                Debug.Print "Das Feld: " & strValue & " existiert nicht in der Tabelle: " & strImportTable & "ImportConfig"
            End If
        Next
        Set fld = Nothing
    End If
    ' Recordset schliessen, erst dann kann man Tabellen Spalten umbenennen
    rs.Close
    Set rs = Nothing
    
    'FeldNamen in der Tabelle umbennenen
    Set td = dbs.TableDefs(strTempImportTable)
    For Each fld In td.Fields
        If Not IsEmpty(dictFldNames.Item(fld.Name)) Then
            If fld.Name <> dictFldNames.Item(fld.Name) Then
                'doppelte Spalte ignorieren, falls schon vorhanden
                On Error Resume Next
                dbs.TableDefs(strTempImportTable).Fields(fld.Name).Name = dictFldNames.Item(fld.Name)
                boolChanged = True
                On Error GoTo 0
            End If
        End If
    Next
    'Feld ID hinzuf�gen
    Set fld = td.CreateField("ID", dbLong)
    fld.Attributes = dbAutoIncrField
    td.Fields.Append fld
    'Feld Dateiname hinzuf�gen
    Set fld = td.CreateField("Dateiname", dbText, 255)
    td.Fields.Append fld
    'Feld Zeit hinzuf�gen
    Set fld = td.CreateField("Zeit", dbText, 255)
    td.Fields.Append fld
    Set fld = Nothing
    Set td = Nothing
    'Aufr�umen
    Set dbs = Nothing
    renameFieldNames = boolChanged
End Function

'**************************************************************************************
'Importiert alle Dateien eines Verzeichnisses mit der Function performImportCSV.
'strType = Typ des Imports , z.B. TRITIUM
'strPfad = Pfad, von dem alle Dateien importiert werden sollen.
'strPattern = Pattern f�r die Dateisuche, z.B. "H???????.csv"
'strImportTable = Zieltabelle
'Optionale Parameter:
'strFileEnd = Datei muss auf Parameter enden, z.B. ".dat"
'strTable = In angegebener Tabelle wird gesucht, ob Datei bereits importiert wurde (nur zusammen mit strCol).
'strCol   = In angegebener Spalte wird gesucht, ob Datei bereits importiert wurde (nur zusammen mit strTable).
'intFieldNameRow =
'strSpecificationName = Import Spezifikation, die genommen werden soll.
'strImportTable = Zieltabelle in die Importiert wird
'erstellt von: Kai A. Quante
'erstellt am: 25.01.2019
'return: Long , Anzahl importierter Dateien
'ge�ndert: 03.05.2019, KAQ ASSE-589 M2 Einlesen Messdaten (9.4.2019)
'ge�ndert: 08.05.2019, KAQ ASSE-589 M2 Einlesen Messdaten (9.4.2019)
'ge�ndert: 27.08.2019, KAQ ASSE-676 M6 Startmonat f�r Import
'ge�ndert: 28.08.2019, KAQ ASSE-681 M6 Import Dateien ohne Probe ID merken und zuk�nftig ignorieren
'ge�ndert: 28.09.2022, KAQ ASSE-929 M6 Mehrere Pfade f�r einen Parameter in ini-Datei
'ge�ndert: 28.09.2022, KAQ ASSE-940 M2 Mehrere Pfade f�r einen Parameter in ini-Datei
'ge�ndert: 05.10.2022, KAQ �bergabe mehrerer Pfade als Array. Auslesen der Dateien NICHT rekursiv.
'**************************************************************************************
Public Function performImportCSVPath(strType As String, strPfad() As String, strPattern As String, strImportTable As String, Optional strTempImportTable As String = "tmpImportTable", _
                Optional strCol As String = "", Optional intFieldNameRow As Integer = 0, Optional strSpecificationName As String = "CSVImport", Optional boolWithHeader As Boolean = False, _
                Optional strStart As String = "", Optional strEnde As String = "", Optional intMaxFiles As Long) As Long
    Dim strFileFound As String
    Dim strFileIgnore As String
    Dim strFileEnd As String
    Dim strDatei() As String
    Dim strPaths() As String
    Dim strFile As String
    Dim strFileName As String
    Dim strImportTableHeader As String
    Dim strSQL As String
    Dim intCount As Long
    Dim intCounter As Long
    Dim boolError As Boolean
    boolError = False
    Dim strSpektrum As String
    
    ' ASSE-973 M6 Spektrum in Datenbank speichern
    addFieldSpektrumDatei strType, strImportTable
    
    'Datenbank offen halten
    Dim dbs As DAO.Database
    Dim rsSQL As DAO.Recordset
    Set dbs = CurrentDb
    Set rsSQL = dbs.OpenRecordset(strImportTable, dbOpenSnapshot)
    
'    Dim intMaxFiles As Long
    Dim i, j As Integer
    performImportCSVPath = 0
    intCounter = 0
    intCount = 0

    'extra Tabelle f�r Header Daten, die auf Vorhandensein gepr�ft wird
    strImportTableHeader = strImportTable & IIf(boolWithHeader And strImportTable <> "", "Header", "")

    If strPattern > "" And InStr(strPattern, ".") > 0 Then
        strFileEnd = Mid(strPattern, InStr(strPattern, "."))
    Else
        strFileEnd = ".csv"
    End If
    
    strPaths = strPfad
    ' Dateien z�hlen, die kommen
    intMaxFiles = IIf(intMaxFiles = 0, countFilesInPaths(strPaths, strPattern, strStart, strEnde), intMaxFiles)
    

    'Spaltenmapper initialisieren
    If strType = "TRITIUM" Then
        ReDim strTableColumn(0)
    End If

    For j = LBound(strPaths) To UBound(strPaths)    ' Start the loop.
        If strPaths(j) <> "" Then
            strDatei = getFilesFromPath(strPaths(j), strPattern, strStart, strEnde)
            
            For i = LBound(strDatei) To UBound(strDatei)    ' Start the loop.
                ' Ignore the current directory and the encompassing directory.
                If strDatei(i) <> "" Then
                    intCounter = intCounter + 1
                    strFile = strPaths(j) & strDatei(i)
                    ' Fortschritt mit Pfaden anzeigen
                    Fortschritt intCounter / intMaxFiles * 100, strPaths(j) & vbCrLf & intCounter & "/" & intMaxFiles & ": " & strDatei(i) & IIf(strFileFound = "", " einlesen.", " vorhanden.")
                    strFileName = Left(strDatei(i), Len(strDatei(i)) - Len(strFileEnd))
                    strFileIgnore = DLookupStringWrapper("Datei", "tblM0MessdatenImportIgnorieren", "Datei = '" & strFileName & "'", "")
                    If strFileIgnore = "" Then ' nur mit Dateien, die nicht ignoriert werden
                        If strImportTable <> "" And strCol <> "" Then
                            strFileFound = DLookupStringWrapper(strCol, strImportTableHeader, strCol & " = '" & strFileName & "'", "")
                        End If
                        If strFileFound = "" Then
                            ' Datei importieren
                            strSpektrum = performImportCSV(strType, strFile, strImportTable, strTempImportTable, intFieldNameRow, strSpecificationName)
                            'Eingelesen
                            If strSpektrum <> "" And strSpektrum <> "ERROR" Then
                                intCount = intCount + 1
                            'Fehler bei ERROR
                            ElseIf strSpektrum = "ERROR" Then
                                boolError = True
                            'nicht eingelesen
                            Else
                            End If
                        ElseIf FileDateTime(strFile) <> DLookupWrapper("Dokument_Timestamp", strImportTableHeader, strCol & " = '" & strFileName & "'") Then
                            ' Error Handling: bereits importiert, aber anderes Dateidatum
                            If DLookupStringWrapper("MD5Hash", strImportTableHeader, strCol & " = '" & strFileName & "'", "") = calculateMD5file(strFile) Then
                            ' Dateidatum �ndern
                                strSQL = "UPDATE " & strImportTableHeader & " SET Dokument_Timestamp = CDate('" & FileDateTime(strFile) & "') WHERE " & strCol & " = '" & strFileName & "'"
                                CurrentDb.Execute strSQL, dbSeeChanges
                            ElseIf DLookupWrapper("Auswertung_geprueft", strImportTableHeader, strCol & " = '" & strFileName & "'") = False Then
                            ' neu einlesen, falls noch nicht gepr�ft
                            ' alte Daten l�schen (gibt es eine Header Tabelle wird der strCol der Datentabelle ein _f als Referenz angeh�ngt)
                                strSQL = "DELETE FROM " & strImportTable & " WHERE " & strCol & IIf(strImportTableHeader <> strImportTable, "_f", "") & " = '" & strFileName & "'"
                                CurrentDb.Execute strSQL, dbSeeChanges
                                If strImportTableHeader <> strImportTable Then
                                    strSQL = "DELETE FROM " & strImportTableHeader & " WHERE " & strCol & " = '" & strFileName & "'"
                                    CurrentDb.Execute strSQL, dbSeeChanges
                                End If
                                strSpektrum = performImportCSV(strType, strFile, strImportTable, strTempImportTable, intFieldNameRow, strSpecificationName)
                                'Eingelesen
                                If strSpektrum <> "" And strSpektrum <> "ERROR" Then
                                    intCount = intCount + 1
                                'Fehler bei ERROR
                                ElseIf strSpektrum = "ERROR" Then
                                    boolError = True
                                'nicht eingelesen
                                Else
                                End If
                            Else
                                'Error Handling: TRITIUM = M2, GAMMA = M6
                                ' umstellen auf Header , falls vorhanden
'                                strImportTable = IIf(boolWithHeader And Right(strImportTable, 6) <> "Header", strImportTable & "Header", strImportTable)
                                reportError "Error", getModulname, strFile & " konnte nicht importiert werden, da die Datei ge�ndert wurde und die Messdaten bereits auf gepr�ft gesetzt sind." & _
                                    vbCrLf & "Heben Sie die Pr�fung auf, um die Messdaten zu �berschreiben oder tauschen Sie die Datei gegen das gepr�fte Original vom " & DLookupWrapper("Dokument_Timestamp", IIf(boolWithHeader, strImportTableHeader, strImportTable), strCol & " = '" & strFileName & "'") & " aus.", _
                                    Format(DLookupNumberWrapper("Proben_ID_f", IIf(boolWithHeader, strImportTableHeader, strImportTable), strCol & " = '" & strFileName & "'", 0), "00.000.000"), strFile, False
                                boolError = True
                            End If
                        End If
                    End If
                End If
            Next i
        End If
    Next j
    performImportCSVPath = intCount
    Fortschritt 101, "Ende"
    If boolError Then
        MsgBox "Es sind Fehler aufgetreten. Bitte pr�fen Sie das Fehlerprotokoll.", vbOKOnly + vbCritical
    End If
    'Recordset schlie�en
    On Error Resume Next
    rsSQL.Close
    Set rsSQL = Nothing
    dbs.Close
    Set dbs = Nothing
End Function

'**************************************************************************************
'Importiert eine Datei.
'strType = Typ des Imports , z.B. TRITIUM
'strFileOrigin = Datei incl. Pfad
'strImportTable = Zieltabelle
'Optionale Parameter:
'strTempImportTable = Tempor�re Tabelle f�r den Import, default tmpImportTable
'intFieldNameRow = Zeile mit den Importfeldnamen
'erstellt von: Valeria Schumacher
'erstellt am: 25.01.2019
'ge�ndert von: Kai A. Quante
'ge�ndert am: 25.01.2019
'return: Long, ID des Header bzw. Eintrags
' 05.07.2019, KAQ: ASSE-620 M6 Gamma-Spe einlesen
' 23.07.2019, KAQ: ASSE-602 M6 Einlesen Messdaten (9.4.2019) => *.txt
' 03.09.2019, KAQ: ASSE-694 M2 Rollback vs. BeginTrans in performImportCSV
' 10.10.2023, KAQ: ASSE-1095 M6 Fehler Import vom 09.10.2023
'**************************************************************************************
Public Function performImportCSV(strType As String, strFileOrigin As String, strImportTable As String, Optional strTempImportTable As String = "tmpImportTable", _
                Optional intFieldNameRow As Integer = 0, Optional strSpecificationName As String = "CSVImport") As String
    Dim strFileRenamed As String
    Dim strFile As String
    Dim wrk As DAO.Workspace
    Dim dbC As DAO.Database
    Dim intRenameCounter As Integer
    Dim lngProbenId As Long
    Dim strImportTableHeader As String
    
    Dim boolError As Boolean
    boolError = False
    performImportCSV = ""
    
    Set wrk = DBEngine(0)
    Set dbC = CurrentDb
    
    'Dateien vom Typ <> .CSV werden kopiert mit Endung .CSV
    If isFileOpen(strFileOrigin) Then
        performImportCSV = "ERROR"
        reportError "Error", getModulname, strFileOrigin & " ist ge�ffnet und konnte daher nicht eingelesen werden.", , strFile, False
        boolError = True
    Else
        ' .csv muss nur kopiert werden, falls ein Import �ber die Import-Funktion von Access mit Spezifikation erfolgt
        If Right(strFileOrigin, 4) <> ".csv" And strSpecificationName <> "" Then
            strFileRenamed = Left(strFileOrigin, Len(strFileOrigin) - 4) & ".csv"
            FileCopy strFileOrigin, strFileRenamed
            strFile = strFileRenamed
        Else
            strFile = strFileOrigin
        End If
        'Datei in die Tabelle kopieren KAQSpezi
        '10 x probieren, ob das einlesen in richtiger Reihenfolge klappt
        Select Case strType
        Case "TRITIUM":
            If TRITIUM_VERSION = cTabelle Then
                'Tabelle l�schen, falls sie schon existiert
                On Error Resume Next
                CurrentDb.TableDefs.Delete strTempImportTable
                On Error GoTo trans_Err
                wrk.BeginTrans
                For intRenameCounter = 0 To 9
                    'Begin der transaction
                    DoCmd.TransferText TransferType:=acImportDelim, SpecificationName:=strSpecificationName, _
                                 tableName:=strTempImportTable, hasfieldnames:=False, FileName:=strFile
                    'Felder gem�ss KonfigTabelle umbennenen
                    'falls das Umbenennen nicht funktioniert, wurde nicht in passender Reihenfolge eingelesen, d.h. 10 x probieren
                    If renameFieldNames(strImportTable, strTempImportTable, intFieldNameRow) = False Then
                        'Roll back transaction
                        wrk.Rollback
                        CurrentDb.TableDefs.Delete strTempImportTable
                        wrk.BeginTrans
                    Else
                        'Daten aus tempTabelle in die Datenbank Tabelle importieren
                        performImportCSV = insertData(strType, strImportTable, strTempImportTable, strFile)
                        'Commit transaction
                        CurrentDb.TableDefs.Delete strTempImportTable
                        wrk.CommitTrans dbForceOSFlush
                        Exit For
                    End If
                Next
            Else
                On Error GoTo trans_Err
                wrk.BeginTrans
                ' ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
                performImportCSV = insertData(strType, strImportTable, strTempImportTable, strFile)
                'Commit transaction
                wrk.CommitTrans dbForceOSFlush
                wrk.Close
                On Error GoTo after_trans_err
                If performImportCSV > "" Then
                    updateLfdNr strImportTable, 0, performImportCSV
                    'zu verwenden nur setzen, wenn noch nicht gesetzt
                    updateZuVerwenden strImportTable, 0, strType, performImportCSV
                    'ASSE-973 M6 Spektrum in Datenbank speichern
                    'ToDo KAQ 18.09.2023
                    addSpektrumDatei strImportTable, strType, performImportCSV, getStringFromStringArray(getFileAsStringArray(strFile))
                End If
            End If
        Case "GAMMA":
            On Error GoTo trans_Err
            wrk.BeginTrans
            performImportCSV = insertData(strType, strImportTable, strTempImportTable, strFile)
            wrk.CommitTrans dbForceOSFlush
            wrk.Close
            On Error GoTo after_trans_err
            ' zu verwenden und laufende Nummer setzen
            strImportTableHeader = strImportTable & "Header"
            lngProbenId = DLookupNumberWrapper("Proben_ID_f", strImportTableHeader, "Spektrum = '" & performImportCSV & "'", 0)
            
            If performImportCSV > "" And lngProbenId > 0 Then
                updateLfdNr strImportTableHeader, lngProbenId
                'zu verwenden nur setzen, wenn noch nicht gesetzt
                updateZuVerwenden strImportTableHeader, lngProbenId, strType
                'ASSE-973 M6 Spektrum in Datenbank speichern
                addSpektrumDatei strImportTableHeader, strType, performImportCSV, getStringFromStringArray(getFileAsStringArray(strFile))
            End If

        Case Else
            Debug.Print "F�r " & strType & " ist kein Import implementiert." & vbCrLf & strFileOrigin & " kann nicht importiert werden.", vbExclamation
        End Select
    End If
    
trans_Exit:
    On Error Resume Next
    'umbennante Datei wieder l�schen
    If strFileRenamed <> "" Then
       Kill strFileRenamed
    End If
    'aufr�umen
    'wrk.Close
    Set dbC = Nothing
    Set wrk = Nothing
    On Error GoTo 0
    Exit Function
trans_Err:
    On Error Resume Next
    'Roll back transaction
    'Debug.Print "Rollback"
    wrk.Rollback
after_trans_err:
    performImportCSV = "ERROR"
    boolError = True
    reportError "Error", getModulname, strFileOrigin & " konnte nicht eingelesen werden.", , strFile, False
    Resume trans_Exit
End Function

'**************************************************************************************
'�bertr�gt Daten von tempor�rer in Zieltabelle und holt dazu noch Informationen aus Quelldatei.
'Die Methode ist spezifisch f�r die Art des Imports, d.h. wird abh�ngig vom Type individualisiert.
'strType = Typ des Imports , z.B. TRITIUM
'strImportTable = Zieltabelle
'strTempImportTable = Quelltabelle
'Optionale Parameter:
'strFile = Quelldatei
'erstellt von: Valeria Schumacher
'erstellt am: 25.01.2019
'ge�ndert von: Kai A. Quante
'ge�ndert am: 28.01.2019
'return: String, Spektrum vom neuen Eintrag; leer wenn kein Eintrag, ERROR bei Fehler
'22.05.2019, KAQ: ASSE-592 M2 Messunsicherheit in Prozent und absolut
'23.05.2019, KAQ: ASSE-599 M2 Bisher nicht ben�tigten Felder aus CSV importieren
'05.08.2019, KAQ: ASSE-616 M2 Spektrum / Aktivit�t zur Probe �bernehmen
'11.08.2022, KAQ: ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'09.11.2022, KAQ: ASSE-968 M6 IMPORT_GAMMA_LAUGE=JA um Lauge-Messstellen ohne Probe ID einzulesen
'10.10.2023, KAQ: ASSE-1095 M6 Fehler Import vom 09.10.2023
'10.10.2023, KAQ: ASSE-1098 M2 Import: Probe ID nur aus erster Spalte und nicht aus Kommentar
'**************************************************************************************
Public Function insertData(strType As String, ByVal strImportTable As String, strTempImportTable As String, strFile As String) As String
    Dim dbs As DAO.Database
    Dim rs As DAO.Recordset
    Dim fld As DAO.Field
    Dim td As DAO.TableDef
    Dim i, j As Integer
    Dim strMessung As String
    Dim strDatei As String
    Dim strFileDateTime As String
    Dim strGeraet As String
    Dim strGeraetSerial As String
    Dim strDokument As String
    Dim strInsert As String
    Dim strInsertTo As String
    Dim strInsertFrom As String
    Dim strInsertNormal As String
    Dim strInsertWhere As String
    Dim strDelete As String
    Dim strMD5Hash As String
    Dim strLfdNr As String
    Dim strNow As String
    Dim strSpektrum As String
    Dim strSQL() As String
    Set dbs = CurrentDb
    insertData = ""
    Dim strArray() As String
    
    strDelete = ""
    strInsert = ""
    strInsertWhere = ""
    strInsertNormal = ""
    
    ' f�r Tritium
    Dim boolAktivitaet_m3 As Boolean
    Dim boolAbsoluteFeuchte As Boolean
    Dim strTritiumArray() As String
    Dim strTritiumFields() As String
    Dim strTritiumValues() As String
    Dim lngProbenId As Long
    Dim strColName As String
    Dim intSpalteMessunsicherheit, intSpalteAktivitaet As Integer
    intSpalteMessunsicherheit = 0
    intSpalteAktivitaet = 0
    
    ' f�r Gamma GAMMA
    Dim strGammaArray() As String
    Dim gammaKopfdaten As TGamma_Kopf
    Dim gammaMessungen() As TGamma_Tabelle
    
    ' Dokument = strFile
    strFileDateTime = FileDateTime(strFile)
    ' Hash Code vom File
    strMD5Hash = calculateMD5file(strFile)
    'Now
    strNow = Now()
    
    'String Array aus Datei
    strArray = getFileAsStringArray(strFile)
    
    ' Tritium Import
    If strType = "TRITIUM" Then
        strTritiumArray = strArray
        ' Import_Datei minus Endung
        strDatei = getKeyValueFromStringArray(strTritiumArray, "Dateien", ";", "Datendatei : ", " - ")
        If Len(strDatei) > 4 Then
            strDatei = Left(strDatei, Len(strDatei) - 4)
            strSpektrum = strDatei
        End If
        'Messung
        strMessung = getKeyValueFromStringArray(strTritiumArray, "Messung", ";", "Zeit: ", ", Status")
        strMessung = getStringData(strMessung, "", " / ") & " " & getStringData(strMessung, " / ")
        ' Geraet
        strGeraet = getKeyValueFromStringArray(strTritiumArray, "Reader", ";")
        ' Geraet_Device_Serial
        strGeraetSerial = getKeyValueFromStringArray(strTritiumArray, "Reader", ";", "Device Serial: ", ", Firmware")
        If TRITIUM_VERSION = cTabelle Then
            'XOR (Aktivitaet_m3,Aktivitaet_Liter)
            boolAktivitaet_m3 = existsField(strTempImportTable, "Aktivitaet_m3")
            'Absolute Feuchte
            boolAbsoluteFeuchte = existsField(strTempImportTable, "Feuchte_absolut")
            'Laufende Nummer
            strLfdNr = "SELECT Count(tblM2Messdaten.Proben_ID_f)+1 FROM tblM2Messdaten LEFT JOIN tmpImportTable ON Proben_ID_f = Val(Replace(Left(tmpImportTable.Probenbezeichnung,10),'.','')) GROUP BY tblM2Messdaten.Proben_ID_f"
            ' Auswertung_erstellt_von = fctGetUser()
            ' Auswertung_erstellt_am = Now()
    
            'insert generieren
            strInsertTo = "INSERT INTO " & strImportTable & " (Proben_ID_f,Einheit,Aktivitaet_Messdaten," & IIf(boolAktivitaet_m3, "Aktivitaet_m3", "Aktivitaet_Liter") & IIf(boolAbsoluteFeuchte, ",Feuchte_absolut", "") & ",Messunsicherheit_Prozent,Import_Datei,Datum_Zeit," & _
                        "Geraet_Messdaten,Geraet_Device_Serial,Dokument,Dokument_Timestamp,MD5Hash,Auswertung_erstellt_von,Auswertung_erstellt_am"
            strInsertFrom = "SELECT Val(Replace(Left(Probenbezeichnung_Messdaten,10),'.',''))," & IIf(boolAktivitaet_m3, "'Bq/m�', Aktivitaet_m3, Aktivitaet_m3", "'Bq/l', Aktivitaet_Liter, Aktivitaet_Liter") & IIf(boolAbsoluteFeuchte, ", Feuchte_absolut", "") & ", Messunsicherheit/" & IIf(boolAktivitaet_m3, "Aktivitaet_m3", "Aktivitaet_Liter") & "*100, '" & strDatei & "', '" & strMessung & "', " & _
                        "'" & strGeraet & "', '" & strGeraetSerial & "', '" & strFile & "', '" & strFileDateTime & "', '" & strMD5Hash & "', '" & fctGetUser() & "', CDate('" & strNow & "')"
            strInsertWhere = "WHERE Val(Replace(Left(Probenbezeichnung_Messdaten,10),'.','')) > 0"
            '�berfl�ssige: ersten 3 Zeilen Zeile und alle ohne Daten und nur die, die eine Probennummer > 0 haben nehmen
            strDelete = "DELETE FROM " & strTempImportTable & " WHERE ID <=3 OR ID >=(SELECT MIN([tmpImportTable1].[ID]) FROM tmpImportTable as tmpImportTable1 WHERE tmpImportTable1.Probenbezeichnung_Messdaten Is Null)"
            ' alle Felder aus Import einlesen, falls im Import und Zieltabelle, die noch nicht anderweitig importiert werden.
            ' WICHTIG: inStrInsertTo m�ssen vorne und hinten Felder stehen, die gesichert nicht eingelesen werden, z.B. AutoWert ID oder Timestamp
            Set td = dbs.TableDefs(strImportTable)
            For Each fld In td.Fields
                If existsField(strTempImportTable, fld.Name) And InStr(strInsertTo, "," & fld.Name & ",") = 0 Then
                    strInsertNormal = strInsertNormal & "," & fld.Name
                End If
            Next
            strInsert = strInsertTo & strInsertNormal & ") " & strInsertFrom & strInsertNormal & " FROM " & strTempImportTable & IIf(strInsertWhere > "", " " & strInsertWhere, "")
        Else
            'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
            strTritiumFields = getTritiumFields(strTritiumArray)
            For i = LBound(strTritiumArray) To UBound(strTritiumArray)
                'ASSE-1098 M2 Import: Probe ID nur aus erster Spalte und nicht aus Kommentar
'                lngProbenId = getProbenId(strTritiumArray(i))
                If InStr(strTritiumArray(i), ";") > 0 Then
                    lngProbenId = getProbenId(Left(strTritiumArray(i), InStr(strTritiumArray(i), ";") - 1))
                Else
                    lngProbenId = 0
                End If
                If lngProbenId > 0 Then
                    ' Strings f�r Insert
                    strInsertTo = "INSERT INTO " & strImportTable & " (Proben_ID_f," & "Import_Datei,Datum_Zeit," & "Geraet_Messdaten,Geraet_Device_Serial,Dokument,Dokument_Timestamp,MD5Hash,Auswertung_erstellt_von,Auswertung_erstellt_am"
                    strInsertFrom = "VALUES (" & lngProbenId & ", '" & strDatei & "', '" & strMessung & "', " & "'" & strGeraet & "', '" & strGeraetSerial & "', '" & strFile & "', '" & strFileDateTime & "', '" & strMD5Hash & "', '" & fctGetUser() & "', CDate('" & strNow & "')"
                    strTritiumValues = getTritiumValues(strTritiumArray(i), countArray(strTritiumFields))
                    For j = LBound(strTritiumFields) To UBound(strTritiumFields)
                        strColName = getTableColumn(strTritiumFields(j))
                        If strColName > "" Then
                            strInsertTo = strInsertTo & ", " & strColName
                            strInsertFrom = strInsertFrom & ", '" & strTritiumValues(j) & "'"
                            ' Aktivit�t auch in Messdaten und Einheit
                            If strColName = "Aktivitaet_m3" Or strColName = "Aktivitaet_Liter" Then
                                strInsertTo = strInsertTo & ", Aktivitaet_Messdaten, Einheit"
                                strInsertFrom = strInsertFrom & ", '" & strTritiumValues(j) & "'"
                                strInsertFrom = strInsertFrom & ", " & IIf(strColName = "Aktivitaet_m3", "'Bq/m�'", "'Bq/l'")
                                intSpalteAktivitaet = j
                            End If
                            ' Aktivit�t auch in Messdaten und Einheit
                            If strColName = "Messunsicherheit" Then
                                intSpalteMessunsicherheit = j
                            End If
                        End If
                    Next j
                    ' Messunsicherheit in Prozent
                    If intSpalteAktivitaet * intSpalteMessunsicherheit > 0 Then
                        strInsertTo = strInsertTo & ", Messunsicherheit_Prozent"
                        strInsertFrom = strInsertFrom & ", '" & (strTritiumValues(intSpalteMessunsicherheit) / strTritiumValues(intSpalteAktivitaet) * 100) & "'"
                    End If
                    strInsert = IIf(strInsert <> "", strInsert & "|", "") & strInsertTo & ") " & strInsertFrom & ")"
                End If
            Next i
        End If
        lngProbenId = 0
    ' Gamma Import
    ElseIf strType = "GAMMA" Then
        strGammaArray = strArray
        gammaKopfdaten = getGammaHeader(strGammaArray)
        lngProbenId = gammaKopfdaten.Proben_ID
        On Error GoTo GammaFormatError
        gammaMessungen = getGammaData(strGammaArray)
        On Error GoTo InsertError
        ' nur Datei importieren, wenn eine Probe ID gesetzt ist.
        'ToDo:
        ' ASSE-955 REP Salzl�sungs - Berichts - Modul
        ' ASSE-967 M6 Lauge ohne Probe ID mit einlesen und nicht anzeigen
        ' ASSE-968 M6 IMPORT_GAMMA_LAUGE=JA um Lauge-Messstellen ohne Probe ID einzulesen
        If (gammaKopfdaten.Proben_ID > 0 And Not getKonfiguration("IMPORT_GAMMA_LAUGE") = "ONLY") Or (gammaKopfdaten.Messstelle_Lauge > "" And (getKonfiguration("IMPORT_GAMMA_LAUGE") = "JA" Or getKonfiguration("IMPORT_GAMMA_LAUGE") = "ONLY")) Then
            For i = LBound(gammaMessungen) To UBound(gammaMessungen)
                strInsertTo = "INSERT INTO " & strImportTable & " (" & _
                "Spektrum_f,Aktivitaet_Messdaten,Isotop,Einheit,Messunsicherheit,Messunsicherheit_Prozent,UVG,OVG,EKG, NWG, Zeilen_Nummer)"
                strInsertFrom = "VALUES('" & gammaKopfdaten.Spektrum & "', '" & gammaMessungen(i).BSchaetzer & "', '" & gammaMessungen(i).Isotop & "', '" & gammaKopfdaten.EinheitMesserg & "', '" & gammaMessungen(i).FehlerAbs & "', '" & gammaMessungen(i).FehlerProz & "', '" & gammaMessungen(i).UVG & "', '" & gammaMessungen(i).OVG & "', '" & gammaMessungen(i).EKG & "', '" & gammaMessungen(i).NWG & "', '" & gammaMessungen(i).ZeilenNr & "')"
                strInsert = IIf(strInsert <> "", strInsert & "|", "") & strInsertTo & " " & strInsertFrom
            Next i
            ' umstellen auf Header f�r den Import
'            strImportTable = strImportTable & "Header"
            strDatei = gammaKopfdaten.Spektrum 'Spektrum entspricht dem Dateinamen
            strSpektrum = gammaKopfdaten.Spektrum
            strInsertTo = "Spektrum, Proben_ID_f, Auswertung_erstellt_von, Auswertung_erstellt_am,Dokument,Dokument_Timestamp,MD5Hash, Import_Datei, Abweichung_Probe,EinheitMesserg,KommentarHeader"
            strInsertFrom = "'" & gammaKopfdaten.Spektrum & "', " & gammaKopfdaten.Proben_ID & ", '" & fctGetUser() & "', CDate('" & strNow & "'), '" & strFile & "', CDate('" & strFileDateTime & "'), '" & strMD5Hash & "', '" & strDatei & "', " & IIf(gammaKopfdaten.AbweichungProbe, -1, 0) & ", '" & gammaKopfdaten.EinheitMesserg & "'" & ", '" & gammaKopfdaten.Kommentar & "'"
            'Probenahmeunsicherheit erg�nzen
            On Error GoTo UnsicherheitError 'IsEmpty l�sst sich nicht abfragen wegen selbst definierter Typ
            For i = LBound(gammaKopfdaten.Unsicherheit) To UBound(gammaKopfdaten.Unsicherheit)
                strInsertTo = strInsertTo & ", UnsicherheitBezeichnung_" & (i + 1) & ", UnsicherheitWert_" & (i + 1)
                strInsertFrom = strInsertFrom & ", '" & gammaKopfdaten.Unsicherheit(i).Bezeichnung & "', '" & gammaKopfdaten.Unsicherheit(i).Wert & "'"
                If i = 3 Then ' maximal 4 Unsicherheiten (0 - 3)
                    Exit For
                End If
            Next i
UnsicherheitError:
            On Error GoTo InsertError
            strInsert = insertHeader(strImportTable & "Header", strGammaArray, ":", strInsertTo, strInsertFrom, ".\", " vom ") & IIf(strInsert <> "", "|" & strInsert, "")
        Else
            'M6 Import Dateien ohne Probe ID merken und zuk�nftig ignorieren
            dbs.Execute ("INSERT INTO tblM0MessdatenImportIgnorieren (Datei) VALUES ('" & gammaKopfdaten.Spektrum & "')")
        End If
    End If
    
    'Datens�tze l�schen, die nicht notwendig sind, SQL Anweisungen sind mit | getrennt
    If strDelete <> "" Then
        strSQL = Split(strDelete, "|")
        For i = LBound(strSQL) To UBound(strSQL)
            dbs.Execute (strSQL(i))
        Next i
    End If
    
    'Datens�tze in die Datenbank Tabelle hinzuf�gen, SQL Anweisungen sind mit | getrennt
    On Error GoTo InsertError
    If strInsert <> "" Then
        strSQL = Split(strInsert, "|")
        For i = LBound(strSQL) To UBound(strSQL)
            dbs.Execute (strSQL(i))
            insertData = strSpektrum
        Next i
    Else
        'ASSE-980 M2 Spektrum Dateien ohne Probe-ID nicht mehr pr�fen
        insertData = ""
        dbs.Execute ("INSERT INTO tblM0MessdatenImportIgnorieren (Datei) VALUES ('" & strSpektrum & "')")
    End If
    'dbs.Close
    Set dbs = Nothing
    Exit Function
GammaFormatError:
    reportError "Error", getModulname, strFile & " konnte nicht eingelesen werden." & vbCrLf & "Das Spektrum " & gammaKopfdaten.Spektrum & " wird nicht erneut eingelesen.", , strFile, False
    dbs.Execute ("INSERT INTO tblM0MessdatenImportIgnorieren (Datei) VALUES ('" & gammaKopfdaten.Spektrum & "')")
    'dbs.Close
    Set dbs = Nothing
    insertData = "ERROR"
    Exit Function
InsertError:
    reportError "Error", getModulname, strFile & " konnte nicht eingelesen werden." & vbCrLf & "Folgende Anweisung ist fehlgeschlagen:" & vbCrLf & strInsert, , strFile, False
    'dbs.Close
    Set dbs = Nothing
    insertData = "ERROR"
End Function

'**************************************************************************************
'Datei als Langer Text bzw. Memo speichern.
'strImportTable : Import Tabelle, f�r GAMMA wird Header erg�nzt
'strType : GAMMA oder TRITIUM
'strSpektrum : Spektrum zu dem gespeichert wird
'strSpektrumDatei : Datei als neu zusammen gesetzter String
'erstellt von: Kai A. Quante
'erstellt am: 14.11.2022
'ASSE-973 M6 Spektrum in Datenbank speichern
'ASSE-974 M2 Spektrum in Datenbank speichern
'**************************************************************************************
Private Sub addSpektrumDatei(strImportTable As String, strType As String, strSpektrum As String, strSpektrumDatei As String)
    Dim dbs As DAO.Database
    Dim rst As DAO.Recordset
    Set dbs = CurrentDb
    
    Set rst = dbs.OpenRecordset("select * from " & strImportTable & " where " & IIf(strType = "GAMMA", "Spektrum", "Import_Datei") & " = '" & strSpektrum & "'", dbOpenDynaset)
 
    rst.MoveFirst
    Do Until rst.EOF
        rst.Edit
        rst!Spektrum_Datei = strSpektrumDatei
        rst.Update
        rst.MoveNext
    Loop
    rst.Close
    Set rst = Nothing
    Set dbs = Nothing
    
End Sub

'**************************************************************************************
'Liest ID der Messdatentabelle aus DB
'return: Messdaten ID wenn gefunden, ansonsten 0
'erstellt von: Kai A. Quante
'erstellt am: 23.11.2022
'**************************************************************************************
Private Function getID(strImportTable As String, strType As String, strSpektrum As String) As Long
    If strType = "GAMMA" Then
        getID = DLookupNumberWrapper("MessdatenHeader_ID", strImportTable, " = '" & strSpektrum & "'", 0)
    ElseIf strType = "TRITIUM" Then
        getID = DLookupNumberWrapper("Messdaten_ID", strImportTable, "Import_Datei = '" & strSpektrum & "'", 0)
    Else
        getID = 0
    End If
End Function
        
'**************************************************************************************
'L�scht angegebene Spektrum und gibt die laufende Nummer zur�ck
'Default ist 0, falls das Spektrum nicht gefunden werden konnte
'erstellt von: Kai A. Quante
'erstellt am: 15.11.2022
'return: Integer
'ASSE-951 M6 Neueinlesen Spektrum ohne �nderungen
'ge�ndert 18.08.2023, KAQ: ASSE-1074 M5 Spektrum neu einlesen
'**************************************************************************************
Public Function deleteSpektrum(strType As String, strSpektrum As String, strImportTable As String) As Integer
    Dim strSQL As String
    deleteSpektrum = DLookupNumberWrapper("Lfd_Nr", strImportTable & IIf(strType = "GAMMA" Or strType = "LLM", "Header", ""), "Spektrum = '" & strSpektrum & "'", 0)
    If strType = "GAMMA" Or strType = "LLM" Then
        strSQL = "DELETE FROM " & strImportTable & " WHERE Spektrum_f = '" & strSpektrum & "'"
        CurrentDb.Execute strSQL, dbSeeChanges
        strSQL = "DELETE FROM " & strImportTable & "Header" & " WHERE Spektrum = '" & strSpektrum & "'"
        CurrentDb.Execute strSQL, dbSeeChanges
    Else
        strSQL = "DELETE FROM " & strImportTable & " WHERE Import_Datei = '" & strSpektrum & "'"
        CurrentDb.Execute strSQL, dbSeeChanges
    End If
End Function

'**************************************************************************************
'Setzt f�r das angegebene Spektrum eine feste laufende Nummer.
'Dies kann verwendet werden, wenn ein Spektrum neu mit gleicher lfd Nummer neu eingelesen werden soll (l�schen, neue einlesen, Nr setzen)
'erstellt von: Kai A. Quante
'erstellt am: 15.11.2022
'return: Nothing
'ASSE-951 M6 Neueinlesen Spektrum ohne �nderungen
'ge�ndert 18.08.2023, KAQ: ASSE-1074 M5 Spektrum neu einlesen
'**************************************************************************************
Public Sub setLfdNr(strType As String, strSpektrum As String, strImportTable As String, intLfdNr As Integer)
    Dim strSQL As String
    strSQL = "UPDATE " & strImportTable & IIf(strType = "GAMMA" Or strType = "LLM", "Header", "") & " SET Lfd_Nr = " & intLfdNr & " WHERE " & IIf(strType = "TRITIUM", "Import_Datei", "Spektrum") & " = '" & strSpektrum & "'"
    CurrentDb.Execute strSQL, dbSeeChanges
End Sub

'**************************************************************************************
'Felder aus Tritium Datei auslesen. Sie stehen in der 2. Zeile.
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2022
'return: String Array
'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'**************************************************************************************
Private Function getTritiumFields(strData() As String) As String()
    Dim strFields() As String
    Dim i As Integer
    If countArray(strData) > 1 Then
        strFields = Split(strData(LBound(strData) + 1), ";")
        For i = LBound(strFields) To UBound(strFields)
            strFields(i) = Trim(Replace(strFields(i), " ", ""))
        Next i
    End If
    getTritiumFields = strFields
End Function

'**************************************************************************************
'Daten aus Tritium Datei auslesen. Sie stehen ab der 3. Zeile.
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2022
'return: String Array
'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'**************************************************************************************
Private Function getTritiumValues(strData As String, intCount As Integer, Optional strSeparator As String = ";") As String()
    Dim strValues() As String
    Dim i As Integer
    strValues = Split(strData, strSeparator)
    If countArray(strValues) = intCount Then
        For i = LBound(strValues) To UBound(strValues)
            strValues(i) = Trim(strValues(i))
        Next i
        getTritiumValues = strValues
    End If
End Function

'**************************************************************************************
'Holt aus String Tabellenspalte f�r die �berschriften aus Importdatei.
'Default-Werte sind f�r TRITIUM.
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2022
'return: String
'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'**************************************************************************************
Private Function getTableColumn(strSpaltenname As String, Optional strTable As String = "tblM2MessdatenImportConfig", _
        Optional strTabellenspalte As String = "Tabellenspalte", Optional strSpalte As String = "Spaltenname") As String
    Dim strCol As String
    getTableColumn = ""
    'im Array zwischenspeichern
    strCol = getValueFromKey(strTableColumn, strSpaltenname)
    If strCol = "" Then
        strCol = DLookupStringWrapper(strTabellenspalte, strTable, strSpalte & " = '" & strSpaltenname & "'", "")
        ReDim Preserve strTableColumn(UBound(strTableColumn) + 1)
        strTableColumn(UBound(strTableColumn)).Key = strSpaltenname
        strTableColumn(UBound(strTableColumn)).Value = strCol
    End If
    If strCol = "" Then
        Debug.Print "Spalte " & strSpaltenname & " in " & strTable & " nicht gefunden."
    Else
        getTableColumn = strCol
    End If
End Function
'**************************************************************************************
'Holt aus einem Key/Value Array den Value f�r einen Schl�ssel.
'Wenn nicht gefunden, dann leerer String
'erstellt von: Kai A. Quante
'erstellt am: 29.09.2022
'return: String
'**************************************************************************************
Private Function getValueFromKey(strKeyValueArray() As TKeyValue, strKey As String)
    Dim i As Integer
    getValueFromKey = ""
    For i = LBound(strKeyValueArray) To UBound(strKeyValueArray)
        If strKeyValueArray(i).Key = strKey Then
            getValueFromKey = strKeyValueArray(i).Value
        End If
    Next i
End Function

'**************************************************************************************
'Holt aus String die Proben ID von beliebiger Stelle, RegEx Pattern "\d\d[.]\d\d\d[.]\d\d\d[^T]" .
'Es darf kein nachfolgendes "T" geben (kommt bei Gamma vor)
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2022
'return: Long
'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'**************************************************************************************
Public Function getProbenId(strText As String) As Long
    getProbenId = 0
    'ASSE-908 M6 Probe ID nicht am Anfang vom Kommentar
    Dim regex As New RegExp, Fundstellen As MatchCollection, Fund As Match, strProbenId As String
    regex.Pattern = "\d\d[.]\d\d\d[.]\d\d\d[^T]?"
    regex.MultiLine = False
    regex.Global = False
    Set Fundstellen = regex.Execute(strText)
    For Each Fund In Fundstellen
        strProbenId = "" & Fund
        getProbenId = Val(Replace(strProbenId, ".", ""))
        Exit For
    Next
End Function

'**************************************************************************************
'L�scht aus String die Proben ID von beliebiger Stelle, RegEx Pattern "\d\d[.]\d\d\d[.]\d\d\d[^T]" .
'Es darf kein nachfolgendes "T" geben (kommt bei Gamma vor)
'siehe auch getProbenId
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2022
'return: Long
'ASSE-910 M2 Umstellung Import von Tabellen-Import auf Text-Import entsprechend M6
'**************************************************************************************
Private Function removeProbenId(strText As String) As String
    'ASSE-908 M6 Probe ID nicht am Anfang vom Kommentar
    Dim regex As New RegExp, Fundstellen As MatchCollection, Fund As Match, strProbenId As String
    regex.Pattern = "\d\d[.]\d\d\d[.]\d\d\d[^T]?"
    regex.MultiLine = False
    regex.Global = False
    Set Fundstellen = regex.Execute(strText)
    For Each Fund In Fundstellen
        removeProbenId = Trim(Replace(strText, Fund, ""))
    Next
End Function

'**************************************************************************************
'Generiert einen Import String f�r eine Header Datei, z.B. f�r Gamma.
'Grundlage sind die Felder in der Header Tabelle.
'Falls diese in der Importdatei zu finden sind, werden sie als String importiert.
'strImportTable = Zieltabelle
'strData() = Zeilen der Import Datei
'Optionale Parameter:
'strSplit = Zeichenkette f�r die Trennung nach Key Wort und Inhalt
'strFields = Feldliste, die bereits besetzt bzw. nicht auszulesen ist
'strValues = Werte f�r die Felder aus strFields. Muss gesetzt werden, wenn strFields gesetzt ist.
'erstellt von: Kai A. Quante
'erstellt am: 18.07.2019
'return: INSERT Anweisung als String
'ASSE-635 M6 Kopfdaten speichern
'ge�ndert 29.07.2019, KAQ: ASSE-437 M6 Gamm-Strahler Lauge: Masse nur Wert ohne +-x% auswerten
'**************************************************************************************
Private Function insertHeader(strImportTable As String, strData() As String, Optional strSplit As String = ";", Optional strFields As String = "", Optional strValues As String = "", _
    Optional strBorderLeft As String = "", Optional strBorderRight As String = "", Optional boolKommaAlsDezimal As Boolean = False) As String
    Dim dbs As DAO.Database
    Dim td As DAO.TableDef
    Dim fld As DAO.Field
    Dim strValue As String
    insertHeader = ""
    
    Set dbs = CurrentDb
    
    'Felder der Zieltabelle in Importdatei finden
    Set td = dbs.TableDefs(strImportTable)
    For Each fld In td.Fields
        strValue = getKeyValueFromStringArray(strData, fld.Name, strSplit, , , True)
        If strBorderLeft & strBorderRight <> "" And InStr(strValue, strBorderLeft) > 0 And InStr(strValue, strBorderRight) > InStr(strValue, strBorderLeft) Then
            strValue = getKeyValueFromStringArray(strData, fld.Name, strSplit, strBorderLeft, strBorderRight, True)
        End If
        If strValue <> "" Then
            strFields = strFields & IIf(strFields <> "", ", ", "") & "[" & fld.Name & "]"
            ' Punkt durch Komma ersetzen in Ausdruck, wenn Zahlenfeld
            If (fld.Type = dbDouble Or fld.Type = dbSingle Or fld.Type = dbFloat Or fld.Type = dbNumeric Or fld.Type = dbDecimal Or fld.Type = dbBigInt Or fld.Type = dbInteger Or fld.Type = dbLong) Then
                If boolKommaAlsDezimal Then
                    strValue = Replace(strValue, ",", ".")
                End If
                strValue = "" & Val(strValue)
            End If
            strValues = strValues & IIf(strValues <> "", ", ", "") & "'" & strValue & "'"
        End If
    Next
    If strFields <> "" Then
        insertHeader = "INSERT INTO " & strImportTable & " (" & strFields & ") VALUES (" & strValues & ")"
    End If
    Set td = Nothing
    Set dbs = Nothing
End Function

'**********************************************************
'Setzt die Lfd Nr auf Max+1 der bisherigen Nummern der Probe.
'erstellt von: Kai A. Quante
'erstellt am: 14.05.2019
'ge�ndert: 30.07.2019, KAQ: ASSE-655 ASSE-437 M6 Felder zwischen Messdaten und Header eineindeutig
'ge�ndert: 11.08.2023, KAQ: ASSE-436 M5 _Low Level Messung
'**********************************************************
Private Sub updateLfdNr_Obsolete(strTable As String, strImportDatei As String, strNow As String)
    Dim rs As DAO.Recordset
    Dim dbs As DAO.Database
    Dim intLfdNr As Integer
    Set dbs = CurrentDb
    Dim strUpdate As String
    Dim strIdField As String
    'wenn Tabelle mit Header dann gibt es auch das Header ID Feld
    strIdField = IIf(Right(strTable, Len("Header")) = "Header", "MessdatenHeader_ID", "Messdaten_ID")
    
    Set rs = dbs.OpenRecordset("Select " & strIdField & ", Proben_ID_f FROM " & strTable & " WHERE Auswertung_erstellt_am = CDate('" & strNow & "') AND Import_Datei = '" & strImportDatei & "'", dbOpenDynaset, dbSeeChanges)
    rs.MoveFirst
    Do While Not rs.EOF
        intLfdNr = DLookupNumberWrapper("Max(Lfd_Nr)", strTable, strIdField & " <> " & rs.Fields(strIdField) & " AND Proben_ID_f = " & rs.Fields("Proben_ID_f"), 0) + 1
        strUpdate = "UPDATE " & strTable & " SET Lfd_Nr = " & intLfdNr & " WHERE " & strIdField & " = " & rs.Fields(strIdField)
        dbs.Execute strUpdate
        rs.MoveNext
    Loop
    'Aufr�umen
    rs.Close
              
    Set rs = Nothing
    Set dbs = Nothing
End Sub

'**********************************************************
'Lfd Nr sortiert die Messdaten abh�ngig von den Messdaten Dateien f�r eine Probe.
'Procedure ersetzt alte Precedure, ge�ndert in updateLfdNr_Obsolete
'erstellt von: Kai A. Quante
'erstellt am: 13.09.2023
'ASSE-1083 M2 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1084 M6 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1085 M5 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ge�ndert: 18.09.2023, KAQ: lngProbeId = 0 und strDatei > "" => Tritium und alle Proben aktualisieren von der Datei...
'**********************************************************
Public Sub updateLfdNr(strTable As String, lngProbeId As Long, Optional strDatei As String = "")
    Dim rs As DAO.Recordset
    Dim dbs As DAO.Database
    Dim intLfdNr As Integer
    Set dbs = CurrentDb
    Dim strUpdate As String
    Dim strIdField As String
    'wenn Tabelle mit Header dann gibt es auch das Header ID Feld
    strIdField = IIf(Right(strTable, Len("Header")) = "Header", "MessdatenHeader_ID", "Messdaten_ID")
    
    If lngProbeId > 0 Then
        Set rs = dbs.OpenRecordset("Select Import_Datei FROM " & strTable & " WHERE Proben_ID_f = " & lngProbeId & " ORDER BY Import_Datei ASC", dbOpenDynaset, dbSeeChanges)
        rs.MoveFirst
        intLfdNr = 1
        Do While Not rs.EOF
            strUpdate = "UPDATE " & strTable & " SET Lfd_Nr = " & intLfdNr & " WHERE Proben_ID_f = " & lngProbeId & " AND Import_Datei = '" & rs("Import_Datei") & "'"
            dbs.Execute strUpdate
            intLfdNr = intLfdNr + 1
            rs.MoveNext
        Loop
        rs.Close
    ElseIf strDatei > "" Then
        'alle Proben durchlaufen
        Set rs = dbs.OpenRecordset("Select Proben_ID_f FROM " & strTable & " WHERE Import_Datei = '" & strDatei & "' ORDER BY Import_Datei ASC", dbOpenDynaset, dbSeeChanges)
        rs.MoveFirst
        Do While Not rs.EOF
            lngProbeId = rs("Proben_ID_f")
            updateLfdNr strTable, lngProbeId
            rs.MoveNext
        Loop
        rs.Close
    End If
    'Aufr�umen
    Set rs = Nothing
    Set dbs = Nothing
End Sub

'**********************************************************
'Aktualisiert zu_verwenden bei allen mit dem �bergebenen Datum und bisher f�r die Probe kein Messwert, der verwendet wird.
'erstellt von: Kai A. Quante
'erstellt am: 14.05.2019
'ge�ndert: 30.07.2019, KAQ: ASSE-655 ASSE-437 M6 Felder zwischen Messdaten und Header eineindeutig
'ge�ndert: 05.08.2019, KAQ: ASSE-616 M2 Spektrum / Aktivit�t zur Probe �bernehmen
'ge�ndert: 29.09.2022, KAQ: ASSE-933 M6 Neueste Spektrum f�r Probe verwenden
'ge�ndert: 29.09.2022, KAQ: ASSE-934 M2 Neueste Spektrum verwenden
'ge�ndert: 11.08.2023, KAQ: ASSE-436 M5 _Low Level Messung
'**********************************************************
Private Sub updateZuVerwenden_Obsolete(strTable As String, strImportDatei As String, strNow As String, Optional strType As String = "", Optional strSpektrum As String = "")
    Dim rs As DAO.Recordset
    Dim dbs As DAO.Database
    Dim intAnzahl As Integer
    Dim boolProbeFreigegeben As Boolean
    Set dbs = CurrentDb
    Dim strUpdate As String
    Dim strIdField As String
    'wenn Tabelle mit Header dann gibt es auch das Header ID Feld
    strIdField = IIf(Right(strTable, Len("Header")) = "Header", "MessdatenHeader_ID", "Messdaten_ID")
    
    Set rs = dbs.OpenRecordset("Select " & strIdField & ", Proben_ID_f FROM " & strTable & " WHERE Auswertung_erstellt_am = CDate('" & strNow & "') AND Import_Datei = '" & strImportDatei & "'", dbOpenDynaset, dbSeeChanges)
    rs.MoveFirst
    Do While Not rs.EOF
        intAnzahl = DLookupNumberWrapper("Count(*)", strTable, strIdField & " <> " & rs.Fields(strIdField) & " AND Proben_ID_f = " & rs.Fields("Proben_ID_f") & " AND zu_verwenden = True", 0)
        boolProbeFreigegeben = DLookupNumberWrapper("freigegeben", "tblM0Probe", "Probe_ID = " & rs.Fields("Proben_ID_f"), 0)
        If intAnzahl = 0 Or boolProbeFreigegeben = False Then
            strUpdate = "UPDATE " & strTable & " SET zu_verwenden = False WHERE Proben_ID_f = " & rs.Fields("Proben_ID_f")
            dbs.Execute strUpdate
            strUpdate = "UPDATE " & strTable & " SET zu_verwenden = True WHERE " & strIdField & " = " & rs.Fields(strIdField)
            dbs.Execute strUpdate
            ' Spektrum setzen mit "Messung_<strType>_"
            'ge�ndert: 11.08.2023, KAQ: ASSE-436 M5 _Low Level Messung
            If strType <> "" And rs.Fields("Proben_ID_f") > 0 And (strType = "Gamma" Or strType = "Tritium" Or strType = "LLM") Then
                strUpdate = "UPDATE tblM0Probe SET Messung_" & strType & " = True, Messung_" & strType & "_Spektrum = '" & strSpektrum & "' WHERE Probe_ID = " & rs.Fields("Proben_ID_f")
                dbs.Execute strUpdate
            End If
        End If
        rs.MoveNext
    Loop
    'Aufr�umen
    rs.Close
              
    Set rs = Nothing
    Set dbs = Nothing
End Sub

'**********************************************************
'Aktualisiert zu_verwenden auf h�chste Lfd-Nr, wenn aktuelle "Zu verwenden" noch nicht gepr�ft wurde.
'Falls nicht �nderbar, wird dies rot markiert und eine Bemerkung gesetzt.
'erstellt von: Kai A. Quante
'erstellt am: 14.05.2019
'ge�ndert: 30.07.2019, KAQ: ASSE-655 ASSE-437 M6 Felder zwischen Messdaten und Header eineindeutig
'ge�ndert: 05.08.2019, KAQ: ASSE-616 M2 Spektrum / Aktivit�t zur Probe �bernehmen
'ge�ndert: 29.09.2022, KAQ: ASSE-933 M6 Neueste Spektrum f�r Probe verwenden
'ge�ndert: 29.09.2022, KAQ: ASSE-934 M2 Neueste Spektrum verwenden
'ge�ndert: 11.08.2023, KAQ: ASSE-436 M5 _Low Level Messung
'ge�ndert: 13.09.2023, Kai A. Quante:
'ASSE-1083 M2 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1084 M6 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ASSE-1085 M5 Laufende Nummern abh�ngig von Reihenfolge Spektrum-Nr
'ge�ndert: 18.09.2023, KAQ: lngProbeId = 0 und strDatei > "" => Tritium und alle Proben aktualisieren von der Datei...
'**********************************************************
Public Sub updateZuVerwenden(strTable As String, lngProbeId As Long, Optional strType As String = "", Optional strDatei As String = "")
    Dim dbs As DAO.Database
    Dim rs As DAO.Recordset
    Dim boolProbeFreigegeben, boolVerwendetGeprueft As Boolean
    Dim lngVerwendet, lngZuVerwenden As Long
    Dim intMaxLfdNr, intLfdNrVerwendet As Integer
    
    Set dbs = CurrentDb
    Dim strUpdate As String
    Dim strIdField As String
    Dim strSpektrum As String
    Dim strTemp As String
    
    If lngProbeId = 0 And strDatei > "" Then
        'alle Proben durchlaufen
        Set rs = dbs.OpenRecordset("Select Proben_ID_f FROM " & strTable & " WHERE Import_Datei = '" & strDatei & "' ORDER BY Import_Datei ASC", dbOpenDynaset, dbSeeChanges)
        rs.MoveFirst
        Do While Not rs.EOF
            lngProbeId = rs("Proben_ID_f")
            updateZuVerwenden strTable, lngProbeId, strType, strDatei
            rs.MoveNext
        Loop
        rs.Close
        Set rs = Nothing
    End If
    
    'wenn Tabelle mit Header dann gibt es auch das Header ID Feld
    strIdField = IIf(Right(strTable, Len("Header")) = "Header", "MessdatenHeader_ID", "Messdaten_ID")
    
    boolProbeFreigegeben = DLookupNumberWrapper("freigegeben", "tblM0Probe", "Probe_ID = " & lngProbeId, 0)
    intMaxLfdNr = DLookupNumberWrapper("max(Lfd_Nr)", strTable, "Proben_ID_f = " & lngProbeId, 0)
    lngVerwendet = DLookupNumberWrapper(strIdField, strTable, "Proben_ID_f = " & lngProbeId & " AND zu_verwenden = true", 0)
    intLfdNrVerwendet = DLookupNumberWrapper("Lfd_Nr", strTable, strIdField & " = " & lngVerwendet, 0)
    boolVerwendetGeprueft = DLookupNumberWrapper("Auswertung_geprueft", strTable, strIdField & " = " & lngVerwendet, vbFalse)
    lngZuVerwenden = DLookupNumberWrapper(strIdField, strTable, "Proben_ID_f = " & lngProbeId & " AND Lfd_Nr = " & intMaxLfdNr, 0)
'Stop
    ' nur Nr setzen, wenn neue zu verwenden, �berhaupt ein Datensatz und eine Lfd Nr
    If lngZuVerwenden > 0 And lngZuVerwenden <> lngVerwendet And intMaxLfdNr >= 1 Then
        'lfd nur setzen bzw. verschieben, wenn nicht gepr�ft
        If Not boolProbeFreigegeben And Not boolVerwendetGeprueft Then
            strUpdate = "UPDATE " & strTable & " SET zu_verwenden = False WHERE " & strIdField & " = " & lngVerwendet
            dbs.Execute strUpdate
            strUpdate = "UPDATE " & strTable & " SET zu_verwenden = true WHERE " & strIdField & " = " & lngZuVerwenden
            dbs.Execute strUpdate
            strSpektrum = DLookupStringWrapper(IIf(strType = "Tritium", "Import_Datei", "Spektrum"), strTable, strIdField & " = " & lngZuVerwenden, "")
            If strType = "Gamma" Or strType = "Tritium" Or strType = "LLM" And strSpektrum > "" Then
                strUpdate = "UPDATE tblM0Probe SET Messung_" & strType & " = True, Messung_" & strType & "_Spektrum = '" & strSpektrum & "' WHERE Probe_ID = " & lngProbeId
                dbs.Execute strUpdate
            End If
        Else
            'Fehler bei h�chster Lfd-Nr, Probe freigegeben odal Ber alte Messung schon gepr�ft
            strTemp = DLookupStringWrapper("Auswertung_Bemerkung", strTable, strIdField & " = " & lngZuVerwenden, "")
            strTemp = IIf(boolProbeFreigegeben, IIf(strTemp > "", strTemp & ", ", "") & "Probe bereits freigegeben", strTemp)
            strTemp = IIf(boolVerwendetGeprueft, IIf(strTemp > "", strTemp & ", ", "") & "Andere Lfd Nr. bereits gepr�ft und verwendet", strTemp)
            strUpdate = "UPDATE " & strTable & " SET Abweichung_Probe = True, Auswertung_Bemerkung = '" & strTemp & "' WHERE " & strIdField & " = " & lngZuVerwenden
            dbs.Execute strUpdate
        End If
    End If
End Sub

'**********************************************************
'Wechselt zu neuem verwendet.
'erstellt von: Kai A. Quante
'erstellt am: 29.09.2022
'ge�ndert: 29.09.2022, KAQ: ASSE-933 M6 Neueste Spektrum f�r Probe verwenden
'ge�ndert: 29.09.2022, KAQ: ASSE-934 M2 Neueste Spektrum verwenden
'**********************************************************
Public Sub changeZuVerwenden(strTable As String, lngHeader_ID As Long, lngProben_ID As Long, Optional strType As String = "", Optional strSpektrum As String = "")
    Dim strIdField As String
    Dim strUpdate As String
    'wenn Tabelle mit Header dann gibt es auch das Header ID Feld
    strIdField = IIf(Right(strTable, Len("Header")) = "Header", "MessdatenHeader_ID", "Messdaten_ID")
    strUpdate = "UPDATE " & strTable & " SET zu_verwenden = false WHERE Proben_ID_f = " & lngProben_ID & " AND " & strIdField & " <> " & lngHeader_ID
    CurrentDb.Execute strUpdate
    strUpdate = "UPDATE " & strTable & " SET zu_verwenden = true WHERE Proben_ID_f = " & lngProben_ID & " AND " & strIdField & " = " & lngHeader_ID
    CurrentDb.Execute strUpdate
    strUpdate = "UPDATE tblM0Probe SET Messung_" & strType & " = True, Messung_" & strType & "_Spektrum = '" & strSpektrum & "' WHERE Probe_ID = " & lngProben_ID
    CurrentDb.Execute strUpdate
End Sub

'**********************************************************
'Gamma Kopfdaten aus String Array lesen
'erstellt von: Kai A. Quante
'erstellt am: 10.07.2019
'ge�ndert 24.07.2019, KAQ: ASSE-631 M6 Felder aus Probe abgleichen mit Messung
'ge�ndert 24.07.2019, KAQ: ASSE-965 M6 Warnmeldungen
'**********************************************************
Public Function getGammaHeader(strArray() As String) As TGamma_Kopf
    Dim Kopfdaten As TGamma_Kopf
    Dim strData As String
    Dim strWarnmeldung As String
    Kopfdaten.Spektrum = getKeyValueFromStringArray(strArray, "Auswertung_Spektrum", " T")
    Kopfdaten.Spektrum = Left(Kopfdaten.Spektrum, InStr(Kopfdaten.Spektrum, ".") - 1)
'    Kopfdaten.Detektor = getKeyValueFromStringArray(strArray, "Detektor", ":")
'    Kopfdaten.Messzeit = getKeyValueFromStringArray(strArray, "Messzeit (real)", ":")
'    Kopfdaten.Probenahmeunsicherheit = getKeyValueFromStringArray(strArray, "Probenahmeunsicherheit", ":")
    strData = getKeyValueFromStringArray(strArray, "Beginn_Probennahme", ":")
    If strData <> "" Then
        Kopfdaten.BeginnPN = CDate(strData)
    End If
    strData = getKeyValueFromStringArray(strArray, "Ende_Probennahme", ":")
    If strData <> "" Then
        Kopfdaten.EndePN = CDate(strData)
    End If
    strData = getKeyValueFromStringArray(strArray, "Bezugsdatum", ":")
    If strData <> "" Then
        Kopfdaten.Bezugsdatum = CDate(strData)
    End If
'    Kopfdaten.Probenalter = getKeyValueFromStringArray(strArray, "Alter der Probe", ":")
'    strData = getKeyValueFromStringArray(strArray, "Messdatum", ":")
'    If strData <> "" Then
'        Kopfdaten.Messdatum = CDate(strData)
'    End If
    Kopfdaten.Masse_Volumen = getKeyValueFromStringArray(strArray, "Probenmasse_Volumen", ":")
'    Kopfdaten.Auswertedatum = getKeyValueFromStringArray(strArray, "Auswertedatum", ":")
    Kopfdaten.Kommentar = getKeyValueFromStringArray(strArray, "Kommentar", ":", , , True)
    'ASSE-965 M6 Warnmeldungen
    strWarnmeldung = Trim(getKeyValueFromStringArray(strArray, "Warnmeldungen", "", , , True, True))
    If strWarnmeldung > "" Then
        Kopfdaten.Kommentar = Left(Kopfdaten.Kommentar & " Warnmeldung: " & strWarnmeldung, 255)
    End If
    
    Kopfdaten.EinheitMesserg = getKeyValueFromStringArray(strArray, "|   |         |        (Bq", "/", , ")", , False)
    Kopfdaten.EinheitMesserg = "Bq/" & IIf(Kopfdaten.EinheitMesserg <> "", Kopfdaten.EinheitMesserg, getKeyValueFromStringArray(strArray, "|   |         |         (Bq", "/", , ")", , False))
    'Probe-ID nach Format 00.000.000
    Kopfdaten.Proben_ID = 0 ' wenn keine Probe ID einlesbar, dann 0

    'ASSE-908 M6 Probe ID nicht am Anfang vom Kommentar
    Dim regex As New RegExp, Fundstellen As MatchCollection, Fund As Match, strProbenId As String
    regex.Pattern = "\d\d[.]\d\d\d[.]\d\d\d[^T]?"
    regex.MultiLine = False
    regex.Global = False
    Set Fundstellen = regex.Execute(Kopfdaten.Kommentar)
    For Each Fund In Fundstellen
        strProbenId = "" & Fund
        Kopfdaten.Proben_ID = Val(Replace(strProbenId, ".", ""))
        Kopfdaten.Kommentar = Trim(Replace(Kopfdaten.Kommentar, Fund, ""))
    Next
' ALT: Einlesen Probe ID vom Anfang
'    If Len(Kopfdaten.Kommentar) >= 10 Then
'        If Mid(Kopfdaten.Kommentar, 3, 1) & Mid(Kopfdaten.Kommentar, 7, 1) = ".." Then
'            Kopfdaten.Proben_ID = Val(Replace(Left(Kopfdaten.Kommentar, 10), ".", ""))
'            Kopfdaten.Kommentar = Trim(Mid(Kopfdaten.Kommentar, 11))
'        End If
'    End If
    
    Kopfdaten.Unsicherheit = getGammaUnsicherheit(strArray)
    
    'ASSE-631 M6 Felder aus Probe abgleichen mit Messung
    Kopfdaten.AbweichungProbe = (getGammaProbeDifference(Kopfdaten) <> "")
    
    ' Lauge Messstelle
    '[PL]\d{6}
    If regExpSuche(Left(Kopfdaten.Kommentar, 7), "[PL]\d{6}") Then
        Kopfdaten.Messstelle_Lauge = Left(Kopfdaten.Kommentar, 7)
    Else
        Kopfdaten.Messstelle_Lauge = ""
    End If
    getGammaHeader = Kopfdaten
End Function

'**********************************************************
'Gamma Kopfdaten aus Datenbank lesen
'erstellt von: Kai A. Quante
'erstellt am: 10.07.2019
'ge�ndert 24.07.2019, KAQ: ASSE-631 M6 Felder aus Probe abgleichen mit Messung
'ge�ndert 29.08.2019, KAQ: ASSE-674 M6 Gamma Proben Differenz
'ge�ndert 04.03.2024, KAQ: ASSE-1133 M6 Performanceoptimierung Differenzberechnung (alle �ber ein Recordset holen)
'**********************************************************
Public Function getGammaHeaderFromDB(lngProbeId As Long, strSpektrum As String) As TGamma_Kopf
    Dim Kopfdaten As TGamma_Kopf
    Dim strData As String
'    Kopfdaten.Spektrum = DLookupStringWrapper("[Spektrum]", "tblM6MessdatenHeader", "[Spektrum] = '" & strSpektrum & "' AND [Proben_ID_f] = " & lngProbeId, "")
    
    Dim dbs As DAO.Database
    Dim rs As DAO.Recordset
    Set dbs = CurrentDb
    
    'alle Proben durchlaufen
    Set rs = dbs.OpenRecordset("Select * FROM tblM6MessdatenHeader where [Spektrum] = '" & strSpektrum & "' AND [Proben_ID_f] = " & lngProbeId, dbOpenSnapshot, dbReadOnly)
    rs.MoveFirst
    Do While Not rs.EOF
        Kopfdaten.Spektrum = strSpektrum
        strData = Nz(rs("[Beginn_Probennahme]"), "")
        If strData <> "" Then
            Kopfdaten.BeginnPN = CDate(strData)
        End If
        strData = Nz(rs("[Ende_Probennahme]"), "")
        If strData <> "" Then
            Kopfdaten.EndePN = CDate(strData)
        End If
        strData = Nz(rs("[Bezugsdatum]"), "")
        If strData <> "" Then
            Kopfdaten.Bezugsdatum = CDate(strData)
        End If
        Kopfdaten.Masse_Volumen = Nz(rs("[Probenmasse_Volumen]"), "")
    '    Kopfdaten.Auswertedatum = getKeyValueFromStringArray(strArray, "Auswertedatum", ":")
        Kopfdaten.Kommentar = Nz(rs("[KommentarHeader]"), "")
        Kopfdaten.EinheitMesserg = Nz(rs("[EinheitMesserg]"), "")
        'Probe-ID nach Format 00.000.000
        Kopfdaten.Proben_ID = lngProbeId
        'Felder aus Probe abgleichen mit Messung
        Kopfdaten.AbweichungProbe = (getGammaProbeDifference(Kopfdaten) <> "")
        
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    Set dbs = Nothing
    
    getGammaHeaderFromDB = Kopfdaten

End Function

'**********************************************************
'Differenz zweier Gamma Messungskopfdaten als Text f�r eine Messagebox
'Proben-ID, Spektrum, Bezugsdatum Einheit, Beginn, Ende und Masse/Volumen werden verglichen.
'Unsicherheiten werden ignoriert
'erstellt am: 15.11.2022
'return String
'ASSE-951 M6 Neueinlesen Spektrum ohne �nderungen
'**********************************************************
Public Function getGammaHeaderDifference(headerA As TGamma_Kopf, headerB As TGamma_Kopf) As String
    getGammaHeaderDifference = ""
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.Proben_ID <> headerB.Proben_ID, "Proben-ID: " & headerA.Proben_ID & " vs. " & headerB.Proben_ID & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.Spektrum <> headerB.Spektrum, "Spektrum: " & headerA.Spektrum & " vs. " & headerB.Spektrum & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.Bezugsdatum <> headerB.Bezugsdatum, "Bezugsdatum: " & headerA.Bezugsdatum & " vs. " & headerB.Bezugsdatum & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.EinheitMesserg <> headerB.EinheitMesserg, "Einheit: " & headerA.EinheitMesserg & " vs. " & headerB.EinheitMesserg & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.BeginnPN <> headerB.BeginnPN, "Beginn: " & headerA.BeginnPN & " vs. " & headerB.BeginnPN & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.EndePN <> headerB.EndePN, "Ende: " & headerA.EndePN & " vs. " & headerB.EndePN & vbCrLf, "")
    getGammaHeaderDifference = getGammaHeaderDifference & IIf(headerA.Masse_Volumen <> headerB.Masse_Volumen, "Masse/Volumen: " & headerA.Masse_Volumen & " vs. " & headerB.Masse_Volumen & vbCrLf, "")
    'Unsicherheiten werden ignoriert
End Function

'**********************************************************
'Differenz zweier Gamma Messungskopfdaten als Text f�r eine Messagebox
'Proben-ID, Spektrum, Bezugsdatum Einheit, Beginn, Ende und Masse/Volumen werden verglichen.
'Unsicherheiten werden ignoriert
'erstellt am: 15.11.2022
'return String
'ASSE-951 M6 Neueinlesen Spektrum ohne �nderungen
'ToDo
'**********************************************************
Public Function getGammaDataDifference(dataA As TGamma_Tabelle, dataB As TGamma_Tabelle) As String
    Dim i As Integer
    getGammaDataDifference = ""
'    getGammaDataDifference = getGammaDataDifference & IIf(headerA.Proben_ID <> headerB.Proben_ID, "Proben-ID: " & headerA.Proben_ID & " vs. " & headerB.Proben_ID & vbCrLf, "")
    'Unsicherheiten werden ignoriert
End Function

'**********************************************************
'Differenz der Gamma Messungskopfdaten zur Probe als Text f�r eine Messagebox
'ge�ndert 24.07.2019, KAQ: ASSE-631 M6 Felder aus Probe abgleichen mit Messung
'erstellt von: Kai A. Quante
'erstellt am: 24.07.2019
'return String
'26.08.2019, KAQ: ASSE-679 M6 Gamma-Messdaten k�nnen nicht eingelesen werden
'29.08.2019, KAQ: ASSE-680 M6 Probenahmeunsicherheiten importieren
'12.10.2022, KAQ: ASSE-946 M6 Wasserprobe: Probenmenge in kg/l vs. Bezugsmenge m� im Labor (Formel fix)
'19.12.2022, KAQ: ASSE-984 M6 AE Mengen Ganzzahlung in Messung entspricht Rundung aus Probe
'19.12.2022, KAQ: ASSE-985 M6 Differenzanzeige mit Bezugsmenge bei Wasserprobe
'**********************************************************
Public Function getGammaProbeDifference(Kopfdaten As TGamma_Kopf) As String
    getGammaProbeDifference = ""
    Dim strData As String
    Dim boolDiff As Boolean
    Dim boolWA As Boolean
    Dim boolWT As Boolean
    Dim boolAE As Boolean
    Dim strProbeart As String
    Dim datBeginn As Date
    Dim datEnde As Date
    Dim strBeginn As String
    Dim strEnde As String
    Dim dblMenge, dblMengeMessdaten As Double
    Dim dblBezugsmenge As Double
    Dim strBezugsmengeSQL, strProbenvolumenSQL As String
    boolDiff = False
    
    
    Dim dbs As DAO.Database
    Dim rs As DAO.Recordset
    Set dbs = CurrentDb
    'alle Proben durchlaufen
    Set rs = dbs.OpenRecordset("Select * FROM qryM0Proben where Probe_ID = " & Kopfdaten.Proben_ID, dbOpenSnapshot, dbReadOnly)
    
    If Kopfdaten.Proben_ID > 0 And Not rs.EOF Then
        rs.MoveFirst
        ' Probeart
        strProbeart = rs("Probeart")
        boolWA = (strProbeart = "WA")
        boolWT = (strProbeart = "WT")
        boolAE = (strProbeart = "AE")
        
        ' Zeitraum / Bezugsdatum
        strBeginn = Nz(rs("Probenentnahme_Start"), "")
        strEnde = Nz(rs("Probenentnahme_Ende"), "")
        datBeginn = CDate(IIf(strBeginn <> "", strBeginn, "00:00:00"))
        datEnde = CDate(IIf(strEnde <> "", strEnde, "00:00:00"))
        If Kopfdaten.Bezugsdatum <> 0 Then ' keine Bezugsdatum sondern Beginn/Ende
            If datEnde <> Kopfdaten.Bezugsdatum Then
                getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Bezugsdatum: '" & datEnde & "' vs. '" & Kopfdaten.Bezugsdatum & "'"
            End If
        Else
            If datBeginn <> Kopfdaten.BeginnPN Then
                getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Beginn: '" & datBeginn & "' vs. '" & Kopfdaten.BeginnPN & "'"
            End If
            If datEnde <> Kopfdaten.EndePN Then
                getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Ende: '" & datEnde & "' vs. '" & Kopfdaten.EndePN & "'"
            End If
        End If
        
        ' Probenmenge, Probenmasse / Volumen
        dblMenge = Nz(rs("Probenmenge"), 0)
        dblMengeMessdaten = Val(Replace(Kopfdaten.Masse_Volumen, ",", "."))
        ' ASSE-984 M6 AE Mengen Ganzzahlung in Messung entspricht Rundung aus Probe
        If boolAE Then
            dblMenge = Round(dblMenge, 0)
        End If
        'ToDo: ASSE-953 M6 Wasserprobe: Probenmenge in kg/l vs. Bezugsmenge m� im Labor (Formel flexibel)
        ' strBezugsmengeSQL = "IIf([Einheit]='kg' And [Probenmenge]<=2,0.5,IIf([Einheit]='mm',[Probenvolumen]/[Probenmenge],IIf([Einheit]='kg',[Probenvolumen]/[Probenmenge]/2,[Probenvolumen]/[Probenmenge])))"
        ' strProbenvolumenSQL = "1"
        'ASSE-946 M6 Wasserprobe: Probenmenge in kg/l vs. Bezugsmenge m� im Labor (Formel fix)
        'ASSE-1108 M6 Anzeige Bezugsmenge nur Wasserproben
        If dblMenge <> dblMengeMessdaten Then
            If boolWA Then
                dblBezugsmenge = Round(DLookupNumberWrapper("Bezugsmenge", "qryM6Bezugsmenge", "[Probe_ID] = " & Kopfdaten.Proben_ID, 0), Len(CStr(dblMengeMessdaten)) - InStrRev(CStr(dblMengeMessdaten), ","))
                If dblBezugsmenge <> dblMengeMessdaten Then
                    getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Probenmasse/Volumen: '" & dblMenge & "' vs. '" & Val(Replace(Kopfdaten.Masse_Volumen, ",", ".")) & "'"
                End If
                'ignore
            'ASSE-1110 M6 Kontrolle Bezugsgr��e bei Wischtests 10 %
            'ElseIf boolWT Then
                'ignore, wird prozesstechnisch gel�st
            Else
                getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Probenmasse/Volumen: '" & dblMenge & "' vs. '" & Val(Replace(Kopfdaten.Masse_Volumen, ",", ".")) & "'"
            End If
        End If
        
        ' Einheit
        strData = Nz(rs("Einheit"), "")
'        If "Bq/" & strData <> Kopfdaten.EinheitMesserg And (dblBezugsmenge <> dblMengeMessdaten Or dblBezugsmenge = 0) Then
        If "Bq/" & strData <> Kopfdaten.EinheitMesserg Then
            'ASSE-985 M6 Differenzanzeige mit Bezugsmenge bei Wasserprobe
            'ASSE-1108 M6 Anzeige Bezugsmenge nur Wasserproben
            If boolWA Then
                If dblBezugsmenge <> dblMengeMessdaten And dblBezugsmenge > 0 Then
                    getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Bezugsmenge: '" & dblBezugsmenge & "' vs. '" & Val(Replace(Kopfdaten.Masse_Volumen, ",", ".")) & "'"
                End If
                If dblBezugsmenge <> dblMengeMessdaten Or dblBezugsmenge = 0 Then
                    getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Einheit: '" & "Bq/" & strData & "' vs. '" & Kopfdaten.EinheitMesserg & "'"
                End If
            Else
                getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Einheit: '" & "Bq/" & strData & "' vs. '" & Kopfdaten.EinheitMesserg & "'"
            End If
        End If
        'Messunsicherheit ignorieren, da mehrere m�glich, ASSE-680
'        strData = DLookupStringWrapper("[Messunsicherheit_Abweichung_Standard]", "tblM0Probe", "[Probe_ID] = " & Kopfdaten.Proben_ID, "")
'        If strData <> "" And strData <> Kopfdaten.Probenahmeunsicherheit Then
'            getGammaProbeDifference = IIf(getGammaProbeDifference <> "", getGammaProbeDifference & vbCrLf, "") & "Probenahmeunsicherheit: '" & strData & "' vs. '" & Kopfdaten.Probenahmeunsicherheit & "'"
'        End If
        'Titel
        getGammaProbeDifference = IIf(getGammaProbeDifference <> "", "Feld: Probe vs. Messergebnisse" & vbCrLf & getGammaProbeDifference, "")
    Else
        getGammaProbeDifference = ""
    End If
    rs.Close
    Set rs = Nothing
    Set dbs = Nothing
    
End Function

'**********************************************************
'Alle Gamma Unsicherheiten auslesen
'erstellt von: Kai A. Quante
'erstellt am: 27.08.2019
'**********************************************************
Public Function getGammaUnsicherheit(strGammaArray() As String) As TGamma_Unsicherheit()
    Dim EineTabZeile() As String
    Dim strZeile As String
    Dim gammaUnsicherheit() As TGamma_Unsicherheit
    Dim i As Integer
    Dim intCount As Integer
    Dim intKommentarZeile As Integer
    intCount = 0
    ' Kommentarzeile finden
    For i = 0 To UBound(strGammaArray)
        If Left(strGammaArray(i), Len("Kommentar:")) = "Kommentar:" Then
            intKommentarZeile = i
            Exit For
        End If
    Next i
    ' Unsicherheiten definieren
    For i = intKommentarZeile - 1 To LBound(strGammaArray) Step -1
        strZeile = Trim(strGammaArray(i))
        If strZeile <> "" Then
            EineTabZeile() = Split(strZeile, ":")
            If InStr(Trim(EineTabZeile(0)), "unsich") > 0 Then
                ReDim Preserve gammaUnsicherheit(intCount)
                gammaUnsicherheit(intCount).Bezeichnung = Trim(EineTabZeile(0))
                gammaUnsicherheit(intCount).Wert = Trim(EineTabZeile(1))
                intCount = intCount + 1
            Else
                Exit For
            End If
        End If
    Next i
    getGammaUnsicherheit = gammaUnsicherheit
End Function


'**********************************************************
'Alle Gamma Messungen aus String Array lesen
'erstellt von: Kai A. Quante
'erstellt am: 10.07.2019
'**********************************************************
Public Function getGammaData(strGammaArray() As String) As TGamma_Tabelle()
    Dim EineTabZeile() As String
    Dim strZeile As String
    Dim Wert() As String
    Dim gammaMessung() As TGamma_Tabelle
    Dim i As Integer
    Dim intCount As Integer
    intCount = 0
    Dim j As Integer
    j = 0
    
    For i = 1 To UBound(strGammaArray) - 1
        strZeile = strGammaArray(i)
        If Left(strZeile, 1) = GAMMA_TRENN_SPALTE And InStr(strZeile, GAMMA_TRENN_SPALTE & "Nr." & GAMMA_TRENN_SPALTE) = 0 And InStr(strZeile, ".") > 0 Then
            strZeile = Replace(strZeile, ".", ",")          'engl Dezimalz. in deut. Dezimalz. wandeln
            strZeile = Mid(strZeile, 2, Len(strZeile) - 2)  'Trennzeichen links und rechts abschneiden
            EineTabZeile() = Split(strZeile, GAMMA_TRENN_SPALTE)
            If UBound(EineTabZeile()) > 0 Then
                ReDim Preserve gammaMessung(intCount)
                intCount = intCount + 1
                gammaMessung(j).ZeilenNr = CInt(EineTabZeile(0))
                gammaMessung(j).Isotop = Trim(EineTabZeile(1))
                If Trim(EineTabZeile(2)) = "" Then
                    gammaMessung(j).BSchaetzer = 0
                    gammaMessung(j).FehlerAbs = 0
                    gammaMessung(j).FehlerProz = 0
                    gammaMessung(j).UVG = 0
                    gammaMessung(j).OVG = 0
                Else
                    Wert() = Split(EineTabZeile(2), GAMMA_TRENN_PLUS_MINUS) ' Bester Sch�tzer und Fehler trennen
                    gammaMessung(j).BSchaetzer = CDbl(Wert(0))
                    gammaMessung(j).FehlerAbs = CDbl(Wert(1))
                    Wert() = Split(EineTabZeile(3), GAMMA_TRENN_MINUS) ' untere und obere Vertrauensgrenze trennen
                    gammaMessung(j).UVG = CDbl(Wert(0))
                    gammaMessung(j).OVG = CDbl(Wert(1))
                    gammaMessung(j).FehlerProz = gammaMessung(j).FehlerAbs / gammaMessung(j).BSchaetzer * 100
                End If
                gammaMessung(j).EKG = EineTabZeile(4)
                gammaMessung(j).NWG = EineTabZeile(5)
                j = intCount
            End If
        End If
    Next i
    getGammaData = gammaMessung
End Function

'**********************************************************
'Sonderzeichen aus Keys f�r Speichern und Vergleich DB entfernen bzw. umwandeln
' ( und ) entfernen
' Leerzeichen in Unterstrich
' - in Unterstrich
' / in Unterstrich
' . entfernen
'erstellt von: Kai A. Quante
'erstellt am: 10.07.2019
'**********************************************************
Public Function entferneSonderzeichen(strText As String) As String
    entferneSonderzeichen = strText
    entferneSonderzeichen = Replace(entferneSonderzeichen, "(", "")
    entferneSonderzeichen = Replace(entferneSonderzeichen, ")", "")
    entferneSonderzeichen = Replace(entferneSonderzeichen, " ", "_")
    entferneSonderzeichen = Replace(entferneSonderzeichen, "-", "_")
    entferneSonderzeichen = Replace(entferneSonderzeichen, "/", "_")
    entferneSonderzeichen = Replace(entferneSonderzeichen, ".", "")
End Function

'**********************************************************
'Feld Spektrum_Datei f�r Spektrum Datei hinzu f�gen.
'erstellt von: Kai A. Quante
'erstellt am: 14.11.2022
'ASSE-973 M6 Spektrum in Datenbank speichern
'**********************************************************
Private Sub addFieldSpektrumDatei(strType As String, strImportTable As String)
    Dim objAccess As Access.Application
    Dim dbs As DAO.Database, td As TableDef
    Dim strDB As String
    Dim strBemerkung As String
    
    Dim strTable As String
    If strType = "GAMMA" Then
        strTable = strImportTable & "Header"
        strBemerkung = "ASSE-973 M6 Spektrum in Datenbank speichern"
    ElseIf strType = "TRITIUM" Then
        strTable = strImportTable
        strBemerkung = "ASSE-972 M2 Spektrum in Datenbank speichern"
    Else
        Exit Sub
    End If
    
    'Gibts die Spalte, dass erledigt.
    If existsField(strTable, "Spektrum_Datei") Then
        Exit Sub
    End If
    
    'Spalte hinzu f�gen
    strDB = getKonfiguration("BACKEND_PFAD")
    Set objAccess = CreateObject("Access.Application")
    objAccess.OpenCurrentDatabase strDB, , getKonfigurationDB(getModulname)
    Set dbs = objAccess.CurrentDb
    Set td = dbs.TableDefs(strTable)
    With td
        .Fields.Append .CreateField("Spektrum_Datei", dbMemo)
        .Fields.Refresh
    End With
    Set td = Nothing
    dbs.Execute ("INSERT INTO tblVersionBackend (Version, Versionsdatum, Bemerkung) VALUES ('" & DLookupStringWrapper("Version", "qryVersionFrontend") & "', CDATE('" & Now() & "') , '" & strBemerkung & "')")

    Set dbs = Nothing
    objAccess.Application.Quit
    
    'Spektrum setzen bei allen, die noch keins haben
    Dim strFile() As String
    Dim rs As DAO.Recordset
    
    Set rs = CurrentDb.OpenRecordset("SELECT Spektrum, Dokument FROM " & strTable & " WHERE Spektrum_Datei Is Null OR Spektrum_Datei  = ''", dbOpenDynaset)
    rs.MoveFirst
    Do While Not rs.EOF
        Fortschritt 1, "Spektrum wird aus Datei in Datenbank gespeichert: " & rs!Spektrum
        strFile = getFileAsStringArray(rs!Dokument)
        addSpektrumDatei strTable, strType, rs!Spektrum, getStringFromStringArray(strFile)
        rs.MoveNext
    Loop
    Fortschritt 101, "Spektren eingelesen"
End Sub

'**********************************************************
'MD4Hash f�r XLS Dateien berechnen.
'Einlesen erfolgt als String.
'Berechnung �ber mdlHashMD5.calculateMD5
'erstellt von: Kai A. Quante
'erstellt am: 11.08.2023
'return String
'**********************************************************
Public Function calculateMD5ForBinaryFile(filePath As String) As String
    Dim fso As Object
    Dim fileStream As Object
    Dim stream As String
    Dim MD5Hash As String
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    ' Check if the file exists
    If Not fso.FileExists(filePath) Then
        calculateMD5ForBinaryFile = ""
        Exit Function
    End If
    
    ' Read the binary file
    Set fileStream = fso.OpenTextFile(filePath, 1, False)
    stream = fileStream.ReadAll
    fileStream.Close
    
    MD5Hash = calculateMD5(stream)
    
    calculateMD5ForBinaryFile = MD5Hash
End Function

'**********************************************************
'erstellt von: Kai A. Quante
'erstellt am: 13.02.2024
'return Long Anzahl der bearbeiteten/gepr�ften Abweichungen zur Probe
'ASSE-1108 M6 Anzeige Bezugsmenge nur Wasserproben
'ge�ndert 18.03.2024, KAQ: ASSE-1129 M6 Aktualisierung Differenz Probe vs. Spektrum per Button
'**********************************************************
Public Function recalcAbweichungGamma(Optional strFilter As String = "", Optional boolRecalc As Boolean = False, Optional boolExport As Boolean = False) As Long
    Dim dbs As DAO.Database
    Dim rs As DAO.Recordset
    Dim strExport As String
'    Dim boolProbeFreigegeben, boolVerwendetGeprueft As Boolean
'    Dim lngVerwendet, lngZuVerwenden As Long
'    Dim intMaxLfdNr, intLfdNrVerwendet As Integer
    Dim gammaHeader As TGamma_Kopf
    Dim strDifference As String
    Dim lngProbeId As Long
    Dim strProbeart As String
    Dim lngCount As Long
    
    Set dbs = CurrentDb
'    Dim strUpdate As String
'    Dim strIdField As String
    Dim strSpektrum As String
    Dim strTemp As String
    
    'alle Proben durchlaufen
    Set rs = dbs.OpenRecordset("Select Proben_ID_f, Spektrum, MessdatenHeader_id, lfd_nr, Abweichung_Probe FROM qryM6MessdatenErgebnisse " & _
        IIf(strFilter > "", "where " & strFilter, "") & " ORDER BY Proben_ID_f ASC, lfd_nr ASC", dbOpenSnapshot, dbReadOnly)
    If Not rs.EOF Then
        rs.MoveFirst
        lngCount = 0
        strExport = "Z�hler" & vbTab & "Probe-ID" & vbTab & "DB Abweichung" & vbTab & "Probeart" & vbTab & "Spektrum" & vbTab & "Abweichung" & vbTab & "Pr�fdatum" & vbCrLf
        Do While Not rs.EOF
            lngCount = lngCount + 1
            Fortschritt lngCount / rs.RecordCount * 100, "Neukalkulation Abweichung: " & lngCount & "/" & rs.RecordCount
            lngProbeId = rs("Proben_ID_f")
            strSpektrum = rs("Spektrum")
            gammaHeader = getGammaHeaderFromDB(lngProbeId, strSpektrum)
            strProbeart = DLookupStringWrapper("Probeart", "qryM0Proben", "[Probe_ID] = " & lngProbeId, "")
            
            strDifference = getGammaProbeDifference(gammaHeader)
            
            If (rs("Abweichung_Probe") Or strDifference > "") Then
                'DB aktualisieren
                dbs.Execute ("UPDATE tblM6MessdatenHeader SET Abweichung_Probe = " & IIf(gammaHeader.AbweichungProbe, "true", "false") & " WHERE Spektrum = '" & gammaHeader.Spektrum & "' AND Proben_ID_f = " & gammaHeader.Proben_ID)
                If strDifference = "" Then
    'Debug.Print lngCount & ": " & lngProbeId & "(" & rs("Abweichung_Probe") & ", " & strProbeart & "), " & strSpektrum & ", NULL"
                Else
    'Debug.Print vbCrLf & lngCount & ": " & lngProbeId & "(" & rs("Abweichung_Probe") & ", " & strProbeart & "), " & strSpektrum & ", " & Replace(IIf(strDifference <> "", vbCrLf & strDifference, "NULL"), vbCrLf & "Feld: Probe vs. Messergebnisse", "")
                End If
                'Export der Differenz nur, wenn gew�nscht
                If boolExport = True Then
                    strExport = strExport & lngCount & vbTab & lngProbeId & vbTab & rs("Abweichung_Probe") & vbTab & strProbeart & vbTab & strSpektrum & vbTab & Replace(Replace(IIf(strDifference <> "", strDifference, "NULL"), "Feld: Probe vs. Messergebnisse" & vbCrLf, ""), vbCrLf, "; ") & vbTab & DLookupStringWrapper("Auswertung_geprueft_am", "tblM6MessdatenHeader", "Spektrum = '" & strSpektrum & "'") & vbCrLf
                End If
            End If
            rs.MoveNext
        Loop
    End If
    rs.Close
    Set rs = Nothing
    Set dbs = Nothing
    
    recalcAbweichungGamma = lngCount
    
    'Export der Differenz nur, wenn gew�nscht
    If boolExport = True Then
        'in Datei schreiben
        Dim strDatei As String
        strDatei = getKonfiguration("AUSWERTUNG_PFAD") & "Differenz" & Format(DateTime.Now, "yyMMddhhmm") & ".xls"
        On Error Resume Next
        Open strDatei For Output As #1
        Print #1, strExport
        Close #1
        On Error GoTo 0
    End If
    
    Fortschritt 101, "Ende"
End Function

'**********************************************************
' Demo Funktion f�r den Import
'**********************************************************
Private Function StoreBLOB2007(strFileName As String, strTable As String, _
    strFieldAttach As String, Optional boolEdit As Boolean, _
    Optional strIdField As String, Optional varID As Variant, _
    Optional strAttachment As String) As Boolean

    Dim fld2 As DAO.Field2
    Dim rstDAO As DAO.Recordset2
    Dim rstACCDB As DAO.Recordset2

    On Error GoTo ErrHandler
    
    Set rstDAO = CurrentDb.OpenRecordset("SELECT * FROM [" & strTable _
        & "]", dbOpenDynaset)
    If boolEdit Then
        If IsNull(varID) Then Err.Raise vbObjectError + 1, , _
            "Keine Datensatz-ID angegeben!"
        rstDAO.FindFirst "CStr([" & strIdField & "])='" & CStr(varID) & "'"
        If rstDAO.NoMatch Then Err.Raise vbObjectError + 2, , _
            "Datensatz mit ID " & varID & " nicht gefunden!"
        rstDAO.Edit
    Else
        rstDAO.AddNew
    End If
    
    Set rstACCDB = rstDAO(strFieldAttach).Value
    If boolEdit Then
        If rstACCDB.EOF Then
            'Fall 1: Es gibt noch keine Anlagen; > neue Anlage
            rstACCDB.AddNew
        Else
            rstACCDB.FindFirst "[FileName]=' & strAttachment" & "'"
            'Fall2: Es gibt keine Anlage mit dem Namen in sAttachment: >
            'neue Anlage
            If rstACCDB.NoMatch Then
                rstACCDB.AddNew
            'Fall3: Anlage gefunden; dann editieren
            Else
                rstACCDB.Edit
            End If
        End If

    Else
        rstACCDB.AddNew
    End If
    
    Set fld2 = rstACCDB.Fields!FileData
    On Error Resume Next
    fld2.LoadFromFile (strFileName)
    If Err.Number = -2146697202 Then
        'Unerlaubte Dateiendung! Spezialbehandlung...
        On Error GoTo ErrHandler
        'Datei zuerst mit erlaubte Endung ".dat" anf�gen
        Name strFileName As strFileName & ".dat"
        'Datei laden
        fld2.LoadFromFile (strFileName & ".dat")
        'Umbenennung r�ckg�ngig machen
        Name strFileName & ".dat" As strFileName
        rstACCDB.Fields!FileName = Mid(strFileName, _
            InStrRev(strFileName, "\") + 1)    'Anlagename setzen
        rstACCDB.Update
    Else
        On Error GoTo ErrHandler
        rstACCDB.Update
    End If
    rstDAO.Update
    StoreBLOB2007 = True    'R�ckgabe True = Alles ok.

Finally:
    On Error Resume Next
    rstACCDB.Close
    rstDAO.Close
    Set rstACCDB = Nothing
    Set rstDAO = Nothing
    Set fld2 = Nothing
    Exit Function

ErrHandler:
    MsgBox Err.Description, vbCritical
    Resume Finally
End Function





