Attribute VB_Name = "mdlTools"
Option Compare Database
Option Explicit

Public Const Version As String = "1.0.0"

'**************************************************************************************
'Erstellt eine Infobox mit �bergebenen Text und Titel. Blendet sich nach X Sekunden wieder aus. http://access.mvps.org/access/forms/frm0046.htm
'erstellt am: 21.09.2016
'erstellt von: Tobias Langer
'R�ckgabewert: void
'**************************************************************************************
Sub mxbPopupMessage(ByVal message As String, _
                    Optional ByVal title As Variant, _
                    Optional ByVal duration As Single)
    Dim f As Form
    Dim lbl As Label
    Dim dblWidth As Double
    Dim myName As String
    Dim savedForm As Boolean
    
    ' used for error handling
    '
    savedForm = False
    
    ' turn off screen repainting so that we don't see the
    ' form being created
    '
    On Error GoTo ErrorHandler
    Application.Echo False
    
    ' make a simple blank form
    '
    Set f = CreateForm
    myName = f.Name
    f.RecordSelectors = False
    f.NavigationButtons = False
    f.DividingLines = False
    f.ScrollBars = 0  ' none
    f.PopUp = True
    f.BorderStyle = acDialog
    f.Modal = True
    f.ControlBox = False
    f.AutoResize = True
    f.AutoCenter = True
    
    ' set the title
    '
    If IsMissing(title) Then
        f.Caption = "Info"
    Else
        f.Caption = title
    End If
    
    ' add a label for the message
    '
    Set lbl = CreateControl(f.Name, acLabel)
    lbl.Caption = message
    lbl.BackColor = 0 ' transparent
    lbl.ForeColor = 0
    lbl.Left = 100
    lbl.Top = 100
    lbl.SizeToFit
    dblWidth = lbl.Width + 200
    f.Width = dblWidth - 200
    f.Section(acDetail).Height = lbl.Height + 200
    
    ' display the form (first close and save it so that when
    ' it is reopened it will auto-centre itself)
    '
    DoCmd.Close acForm, myName, acSaveYes
    savedForm = True
    DoCmd.OpenForm myName
    DoCmd.MoveSize , , dblWidth
    DoCmd.RepaintObject acForm, myName

    ' turn screen repainting back on again
    '
    Application.Echo True

    ' display form for specifed number of seconds
    '
    If duration <= 0 Then duration = 2
    Dim startTime As Single
    startTime = Timer
    While Timer < startTime + duration
    Wend
    
    ' close and delete the form
    '
    DoCmd.Close acForm, myName, acSaveNo
    DoCmd.DeleteObject acForm, myName
    Exit Sub
    
ErrorHandler:
    Application.Echo True
    Dim i As Integer
    For Each f In Forms
      If f.Name = myName Then
        DoCmd.Close acForm, myName, acSaveNo
        Exit For
      End If
    Next f
    If savedForm Then
      DoCmd.DeleteObject acForm, myName
    End If
               
End Sub

'**************************************************************************************
'Pr�ft, ob das bis Datum vor dem von Datum liegt und gibt ggf. eine Meldung aus
'TODO Zukunft
'erstellt am: 24.05.2017
'erstellt von: Tobias Langer
'R�ckgabewert: Boolean
'**************************************************************************************
Public Function hatGueltigenZeitraum(strDatumVonBezeichnung As String, strDatumVon As String, strDatumBisBezeichnung As String, strDatumBis As String) As Boolean
    hatGueltigenZeitraum = True
End Function

'***********************************************************************************
'Ermittle den RGB Wert aus einem String 000.000.000
'Default: RGB(255.255.255)
'erstellt am: 08.08.2017
'erstellt von: Kai A. Quante
'R�ckgabewert: Long
'***********************************************************************************
Public Function getRGB(strRGB As String) As Long
    getRGB = IIf(strRGB = "", RGB(255, 255, 255), RGB(Val(Left(strRGB, 3)), Val(Mid(strRGB, 5, 3)), Val(Right(strRGB, 3))))
End Function

'**************************************************************************************
'Pr�fen ob eine Abfrage existiert
'erstellt am: 25.08.2016
'erstellt von: Kai A. Quante
'**************************************************************************************
Public Function QueryExists(QueryName As String) As Boolean
    Dim db As DAO.Database
    Dim qdf As DAO.QueryDef

    QueryExists = False
    Set db = CurrentDb
    For Each qdf In db.QueryDefs
        If qdf.Name = QueryName Then
            QueryExists = True
            Exit Function
        End If
    Next qdf
    Set qdf = Nothing
    Set db = Nothing
End Function


'*******************************************************************************************
'Schliesst alle offenen Formulare
'erstellt am: 08.03.2018
'erstellt von: Tobias Langer
'ASSE-311 Mx: Benutzer wechseln / abmelden
'*******************************************************************************************
Public Function CloseAllForms(Optional BesidesThisOne As String)
    Dim i As Long, FrmName As String
    For i = Forms.Count - 1 To 0 Step -1
      FrmName = Forms(i).Name
      If FrmName <> BesidesThisOne Then
        DoCmd.Close acForm, FrmName, acSaveYes
      End If
    Next i
End Function

'**************************************************************************************
'Diese Funktion ermittelt, ob das Formular schon ge�ffnet ist
'erstellt am: 21.03.2018
'erstellt von: Kai A. Quante
'R�ckgabewert: Boolean
'**************************************************************************************
Public Function IstFormOffen(frmFormular As String) As Boolean
    IstFormOffen = (SysCmd(acSysCmdGetObjectState, acForm, frmFormular) <> 0)
End Function

'Datum ins VBA-Datumsformat (SQL-Format) umwandeln
'https://dbwiki.net/wiki/VBA_Tipp:_Datum_ins_VBA-Datumsformat_(SQL-Format)_umwandeln
Public Function CDateSQL(ByVal Datumswert As Variant) As String
 
   'Quelle: www.dbwiki.net oder www.dbwiki.de
 
   Const AccessDateFmt As String = "\#mm\/dd\/yyyy\#"
 
   If IsDate(Datumswert) Then
      CDateSQL = Format$(CDate(Datumswert), AccessDateFmt)
   Else
      Err.Raise 13, , "Ung�ltiger Datumswert: " & Nz(Datumswert, "Null")
   End If
End Function
