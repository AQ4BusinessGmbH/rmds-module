Attribute VB_Name = "mdlGlobal"
Option Explicit

'**************************************************************************************
'1.1.0 05.11.18 KAQ: ASSE-510 M2 Umstellen Messdaten auf Probe
'1.1.1 28.11.18 KAQ: ASSE-510 M2 Einbindung tblSohle statt tblM0Sohle
'1.1.2 03.12.18 KAQ: ASSE-515 M4 Messdatenpflege, Einbindung
'1.1.3 10.12.18 KAQ: ASSE-502 Mx Optimierung der Einbindung
'1.1.4 11.12.18 KAQ: Konstanten f�r Datei �ffnen
'1.1.5 11.12.18 KAQ: generiereProbennummer �bernommen aus M0
'1.2.0 28.01.19 KAQ: Import Modul (muss noch aus M2 verlagert werden, ToDo)
'1.2.1 31.01.19 KAQ: getMitarbeiterBereich, etc.
'1.2.2 01.02.19 KAQ: Tabellen einbinden aktualisieren
'2.0.0 07.02.19 KAQ: RMDS Module nur noch Vorlage, keine Einbindung !!!!!!!
'2.1.0 13.05.19 KAQ: Error Handling, ASSE-590, reportError(...), Fortschritt
'2.1.1 24.05.19 KAQ: Error Handling, Format f�r Datum/Zeit
'2.2.0 27.05.19 KAQ: ASSE-605 M0 Tabellen einbinden f�r remote Zugriff
'2.2.1 27.05.19 KAQ: ASSE-604 M0 Neue Passworte f�r M0 und M2 Backends
'2.2.2 19.06.19 TL:  ASSE-617 M0 Benutzerspezifische Formularansicht
'2.2.3 22.07.19 KAQ: ASSE-638 Mx Fortschritt max. 1 mal pro Sekunde
'2.2.4 03.09.19 TL:  ASSE-654 M7 Basisversion
'2.2.5 12.09.19 KAQ: ASSE-686 M0 Rechte
'2.2.6 13.09.19 KAQ: ASSE-698 M0 Benutzer ohne Rechte ber�cksichtigen
'2.2.7 26.06.20 KAQ: ASSE-739 Mx SSO Vorbereitung und Implementierung
'2.2.8 15.10.20 KAQ: ASSE-814 M6 Eigenst�ndiges Modul
'2.2.9 27.10.20 KAQ: ASSE-818 M0 Performanceoptimierung beim Start eines neuen Moduls
'2.3.0 13.09.22 KAQ: ASSE-920 M0 PWDs f�r weitere Module in mdlGlobal: M3, M5, M8, REP
'2.3.1 28.09.22 KAQ: ASSE-930 Mx Aktualisierung Fortschrittsanzeige, DoEvents
'2.3.1 28.09.22 KAQ: ASSE-938 M6 Performance Import
'2.3.1 28.09.22 KAQ: ASSE-937 M2 Performance Import
'2.3.2 28.09.22 KAQ: ASSE-941 Mx Parameter Array in ini-Datei
'2.3.3 02.11.22 KAQ: ASSE-966 Mx Error Meldung in DB ohne Message
'2.4.0 23.01.23 KAQ: existsField, addField, countArray u.a. von mdlImport (!!!!)
'2.4.1 17.05.24 KAQ: ASSE-1165 Mx reportError auch mit einfachen Anf�hrungszeichen in Description
'2.4.2 25.10.24 KAQ: ASSE-1217 M0 Einbindung M2, M5, M6
'**************************************************************************************
Private Const Version As String = "2.4.2"

' Globale Variablen definieren
Global GBL_strUsername As String
Global GBL_arrRechte() As String
Public hWnd As Long
Private timestampFortschritt, timestampDoEvens As Date

'**************************************************************************************
'32/64bit Einbindung
'erstellt am: 01.01.2018
'erstellt von: Kai A. Quante
'**************************************************************************************
#If VBA7 Then
'  Code is running in the new VBA7 editor
     #If Win64 Then
     '  Code is running in 64-bit version of Microsoft Office
    Public Declare PtrSafe Function ShellExecute Lib "shell32.dll" Alias _
        "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, _
                     ByVal lpFile As String, ByVal lpParameters As String, _
                     ByVal lpDirectory As String, ByVal nshowcmd As Long) _
                    As Long
     #Else
     '  Code is running in 32-bit version of Microsoft Office
    Public Declare Function ShellExecute Lib "shell32.dll" Alias _
        "ShellExecuteA" (ByVal hWnd As Long, ByVal lpOperation As String, _
                     ByVal lpFile As String, ByVal lpParameters As String, _
                     ByVal lpDirectory As String, ByVal nshowcmd As Long) _
                    As Long
     #End If
#Else
' Code is running in VBA version 6 or earlier
#End If

'Konstanten zum Datei �ffnen
Public Const SW_HIDE = 0          ' Versteckt �ffnen
Public Const SW_MAXIMIZE = 3      ' Maximiert �ffnen
Public Const SW_MINIMIZE = 6      ' Minimiert �ffnen
Public Const SW_NORMAL = 1
Public Const SW_RESTORE = 9
Public Const SW_SHOWMAXIMIZED = 3
Public Const SW_SHOWMINIMIZED = 2
Public Const SW_SHOWMINNOACTIVE = 7
Public Const SW_SHOWNOACTIVATE = 4
    
'**************************************************************************************
'Funktion zum Auslesen der Version.
'erstellt am: 02.11.2018
'erstellt von: Kai A. Quante
'R�ckgabewert: Version
'**************************************************************************************
Public Function getMxVersion() As String
  getMxVersion = Version
End Function

'**************************************************************************************
'Funktion zum Auslesen des Windows Users.
'erstellt am: 19.06.2020
'erstellt von: Kai A. Quante
'R�ckgabewert: Version
'**************************************************************************************
Public Function getWindowsUser() As String
  getWindowsUser = Environ("USERNAME")
End Function

'**************************************************************************************
'Funktion initialisiert globale Variable GBL_strUsername
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'ge�ndert: 23.04.18 Tobias Langer ASSE-362 RMDS M1: Gro�buchstaben in Benutzernamen
'**************************************************************************************
Public Function Init_Username(strUsername As String)
    GBL_strUsername = LCase(strUsername)
End Function
'**************************************************************************************
'Funktion initialisiert globale Variablen GBL_strUsername und GBL_arrRechte()
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert boolean: FALSE wenn Benutzer oder Berechtigungen nicht ermittelt werden konnten
'**************************************************************************************
Public Function Init_Globals() As Boolean
    Init_Globals = True
    
    ' Globale Variablen initialisieren: GBL_strUsername
    If Not AngemeldetenUserErmitteln Then
        MsgBox "Benutzer konnte nicht identifiziert werden." & vbNewLine & "Bitte kontaktieren Sie den Administrator.", vbCritical
        Init_Globals = False
    End If
    
    If Not RechteUserErmitteln Then
        MsgBox "Mitarbeiter ist keiner Rolle zugeordnet bzw. der Rolle sind keine Berechtigungen zugeordnet." & vbNewLine & "Bitte kontaktieren Sie den Administrator.", vbCritical
        Init_Globals = False
    End If
End Function
'**************************************************************************************
'Funktion ermittelt angemeldeten Benutzer und initialisiert globale Variable GBL_strUsername
'Gibt FALSE zur�ck, wenn Benutzer nicht ermittelt werden konnten
'erstellt am: 01.01.2016
'erstellt von: Kai A, Quante
'R�ckgabewert boolean: true|false
'**************************************************************************************
Private Function AngemeldetenUserErmitteln() As Boolean
    If Not IsNull(GBL_strUsername) And GBL_strUsername <> "" Then
        AngemeldetenUserErmitteln = True
    Else
        AngemeldetenUserErmitteln = False
    End If
End Function

'**************************************************************************************
'Funktion ermittelt angemeldeten Benutzer und initialisiert globale Variable GBL_strUsername
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert string: Benutzername|false (false, wenn Benutzer nicht ermittelt werden konnten)
'ge�ndert: 31.07.18 Tobias Langer ASSE-374 Mx Gro�buchstaben in Benutzernamen
'**************************************************************************************
Public Function fctGetUser() As String
    If IsNull(GBL_strUsername) Or GBL_strUsername = "" Then
        GBL_strUsername = LCase(Forms!frmStart!txtUsername.Value)
    End If
    fctGetUser = LCase(GBL_strUsername)
End Function

'***********************************************************************************
'Liefert den Mitarbeiter mit K�rzel des Bereichs abh�ngig von der Mitarbeiter ID.
'Vorhanden sein m�ssen tblMitarbeiter und tblM0Teilbereich.
'erstellt am: 31.01.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: String
'***********************************************************************************
Public Function getMitarbeiterBereich(strMitarbeiterKuerzel As Variant) As String
    Dim strMitarbeiter As String
    Dim strTeilbereich As String
    If IsNull(strMitarbeiterKuerzel) Then
        getMitarbeiterBereich = ""
    Else
        strMitarbeiter = getMitarbeiter(strMitarbeiterKuerzel)
        strTeilbereich = getTeilbereich(strMitarbeiterKuerzel)
        getMitarbeiterBereich = strMitarbeiter & IIf(strTeilbereich <> "", " (" & strTeilbereich & ")", "")
    End If
End Function

'***********************************************************************************
'Liefert den Mitarbeiter als Vorname+Leerzeichen+Nachname.
'Vorhanden sein muss tblMitarbeiter.
'erstellt am: 31.01.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: String
'***********************************************************************************
Public Function getMitarbeiter(strMitarbeiterKuerzel As Variant) As String
    If IsNull(strMitarbeiterKuerzel) Then
        getMitarbeiter = ""
    Else
        getMitarbeiter = DLookupStringWrapper("vorname & ' ' & nachname", "tblMitarbeiter", "mitarbeiter_id='" & strMitarbeiterKuerzel & "'")
    End If
End Function

'***********************************************************************************
'Liefert den Teilbereich als ID zu einem Mitarbeiter K�rzel.
'Vorhanden sein m�ssen tblMitarbeiter und tblM0Teilbereich.
'erstellt am: 31.01.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: Integer, 0 wenn nicht gefunden
'***********************************************************************************
Public Function getTeilbereichID(strMitarbeiterKuerzel As Variant) As Integer
    If IsNull(strMitarbeiterKuerzel) Then
        getTeilbereichID = 0
    Else
        getTeilbereichID = DLookupNumberWrapper("Teilbereich_ID_f", "tblMitarbeiter", "mitarbeiter_id='" & strMitarbeiterKuerzel & "'", 0)
    End If
End Function

'***********************************************************************************
'Liefert den Teilbereich als K�rzel zu einem Mitarbeiter K�rzel.
'Vorhanden sein m�ssen tblMitarbeiter und tblM0Teilbereich.
'erstellt am: 31.01.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: String
'***********************************************************************************
Public Function getTeilbereich(strMitarbeiterKuerzel As Variant) As String
    If IsNull(strMitarbeiterKuerzel) Then
        getTeilbereich = ""
    Else
        getTeilbereich = DLookupStringWrapper("Kuerzel", "tblM0Teilbereich", "Teilbereich_ID = " & getTeilbereichID(strMitarbeiterKuerzel), "")
    End If
End Function

'****************************************************************************************************************
'Funktion ermittelt Berechtigungen von angemeldeten Benutzer und initialisiert globale Variablen GBL_arrRechte()
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert boolean: true|false (false, wenn Berechtigungen nicht ermittelt werden konnten)
'ge�ndert: 11.09.2019, KAQ: ASSE-686 M0 Rechte
'*****************************************************************************************************************
Private Function RechteUserErmitteln(Optional strModul As String = "") As Boolean
    Dim strBerechtigung As String
    Dim rsRechte As DAO.Recordset
    Dim dbs As DAO.Database
    Set dbs = CurrentDb
    Dim intCount As Integer

    Set rsRechte = dbs.OpenRecordset("Select * FROM qryBenutzerBerechtigungen WHERE Modul = '" & IIf(strModul <> "", strModul, getModulname()) & "' AND Mitarbeiter_ID= '" & GBL_strUsername & "'", dbOpenDynaset, dbSeeChanges)
    If Not rsRechte.EOF And Not rsRechte.BOF Then
        ReDim GBL_arrRechte(rsRechte.RecordCount - 1)
        Do While Not rsRechte.EOF
            ' Berechtigungen f�r den Mitarbeiter im Array abspeichern
            GBL_arrRechte(UBound(GBL_arrRechte)) = rsRechte.Fields("Kuerzel").Value
            ReDim Preserve GBL_arrRechte(UBound(GBL_arrRechte) + 1)
'Debug.Print UBound(GBL_arrRechte)
        rsRechte.MoveNext
        Loop
        RechteUserErmitteln = True
    Else
        RechteUserErmitteln = False
    End If
    
    'Aufr�umen
    rsRechte.Close
    dbs.Close
    Set rsRechte = Nothing
    Set dbs = Nothing

End Function

'***********************************************************************************
'Feststellen, ob Benutzer ein Recht hat
'erstellt am: 25.08.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'Optionale Parameter:
'lngBereich : Fachbereich bzw. Gruppen ID , die identisch sein muss
'strModul   : Modul mit ber�cksichtigen
'2.2.5 12.09.19 KAQ: ASSE-686 M0 Rechte
'2.2.6 13.09.19 KAQ: ASSE-698 M0 Benutzer ohne Rechte ber�cksichtigen
'***********************************************************************************
Public Function HatBenutzerRecht(txtRecht As String, Optional lngBereich As Long = 0, Optional strModul As String = "") As Boolean
    Dim i As Integer
    HatBenutzerRecht = False
    
    ' Bereich / Gruppe
    If lngBereich <> 0 And getTeilbereichID(fctGetUser()) <> lngBereich Then
        HatBenutzerRecht = False
    Else
        ' anderes Modul
        If strModul <> "" Then
            RechteUserErmitteln (strModul)
        End If
        On Error Resume Next
        i = LBound(GBL_arrRechte)
        If Err.Number <> 0 Then
            HatBenutzerRecht = False
            Exit Function
        End If
        On Error GoTo 0
        For i = LBound(GBL_arrRechte) To UBound(GBL_arrRechte)
            If GBL_arrRechte(i) = txtRecht Then
                HatBenutzerRecht = True
            End If
        Next i
        If strModul <> "" Then
            RechteUserErmitteln
        End If
    End If
End Function

'***********************************************************************************
'Pr�fe Passwort auf korrekte Anforderungen
'erstellt am: 22.01.2018
'erstellt von: Tobias Langer
'ASSE-278 WKP Passwortrichtlinien
'R�ckgabewert: boolean
'***********************************************************************************
Public Function PasswortEntsprichtAnforderungen(txtPasswort As String, txtMitarbeiter As String, boolNachricht As Boolean) As Boolean
    PasswortEntsprichtAnforderungen = False
    
    Dim objRegExp As New RegExp
    Dim zaehleZeichnengruppen As Integer
    
    zaehleZeichnengruppen = 0
    
    'Min. 6 Zeichen
    If Len(txtPasswort) >= 6 Then
        'Gro�buchstaben, Kleinbuchstaben, Zahlen, Sonderzeichen
        objRegExp.IgnoreCase = False
        objRegExp.Global = True
        
        objRegExp.Pattern = "[A-Z]"
        If objRegExp.Test(txtPasswort) = True Then
            zaehleZeichnengruppen = zaehleZeichnengruppen + 1
        End If
        
        objRegExp.Pattern = "[a-z]"
        If objRegExp.Test(txtPasswort) = True Then
            zaehleZeichnengruppen = zaehleZeichnengruppen + 1
        End If
        
        objRegExp.Pattern = "[0-9]"
        If objRegExp.Test(txtPasswort) = True Then
            zaehleZeichnengruppen = zaehleZeichnengruppen + 1
        End If
        
        objRegExp.Pattern = "[^A-Za-z0-9]"
        If objRegExp.Test(txtPasswort) = True Then
            zaehleZeichnengruppen = zaehleZeichnengruppen + 1
        End If
        
        If zaehleZeichnengruppen >= 2 Then
            'Frueher bereits verwendet?
            If DLookupNumberWrapper("COUNT(Passwort)", "tblPasswort", "Passwort = '" & txtPasswort & "' And Mitarbeiter_ID = '" & txtMitarbeiter & "'", 0) = 0 Then
                'Aktuelles Passwort nicht j�nger als einen Tag
                If DLookupNumberWrapper("COUNT(Passwort)", "tblPasswort", "Passwort = '" & txtPasswort & "' And Mitarbeiter_ID = '" & txtMitarbeiter & "'", 0) = 0 Then
                    PasswortEntsprichtAnforderungen = True
                End If
            End If
        End If
    End If
    
    If PasswortEntsprichtAnforderungen = False And boolNachricht = True Then
        MsgBox "Das Passwort entspricht nicht den Passwort-Richtlinien:" & vbCrLf & "-Passwort wurde noch nie verwendet" & vbCrLf & "-Passwortl�nge mindestens 6 Zeichen" & vbCrLf & "-Passwort muss mindestens 2 der folgenden Gruppen beinhalten (Gro�buchstaben, Kleinbuchstaben, Sonderzeichen, Zahlen )", vbCritical
    End If
End Function

'**************************************************************************************
'OHNE FUNKTION !!!!
'Liest, wenn vorhanden Konf. Datei aus und Gibt Pfad des Backends zur�ck'Config mit Backend schreiben
'ToDo: Schreiben �ber Word als OLE geht nicht auf gemischten Systemen!
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'**************************************************************************************
Public Sub BackendPfadInDateiSpeichern(ByVal strPfadBE As String)
    Dim objWord As Object
'    Set objWord = CreateObject("Word.Application")
'    objWord.System.PrivateProfileString(FileName:=CurrentProject.Path & "\rmds_m1.ini", Section:="BACKEND", Key:="Pfad_BE") = strPfadBE
'    objWord.Quit
    Set objWord = Nothing
End Sub

'**************************************************************************************
'Backend reparieren und komprimieren
'erstellt am: 01.07.2016
'erstellt von: Kai A. Quante
'**************************************************************************************
Public Function ReparierenKomprimieren() As Boolean
    ' Backend reparieren und komprimieren
  On Error GoTo myError
  DoCmd.Hourglass True
  Dim strPath As String
  Dim strFile As String
  Dim varDummyPath As Variant
  Dim varDummyFile As Variant
  strPath = GetBackendPfad
  strFile = Dir(strPath)
  varDummyPath = Left$(strPath, Len(strPath) - Len(strFile))
  varDummyFile = varDummyPath & "Dummy.mdb"
  FileCopy strPath, strPath & "." & Format(Now(), "YYMMDDHHmmss")
  DBEngine.CompactDatabase strPath, varDummyFile, "; Pwd =" & GetBackendPwd, , "; Pwd =" & GetBackendPwd
  Kill strPath
  Name varDummyFile As strPath
  
  ' Frontend beim Schlie�en automatisch reparieren und komprimieren
  SetOption "Auto Compact", True
  
  ReparierenKomprimieren = True
myExit:
    DoCmd.Hourglass False
    Exit Function
    
myError:
    Select Case Err.Number
      Case 3005, 3024, 53, 3044, 76
        MsgBox "Die Datenbank konnte nicht gefunden werden.", vbOKOnly
      Case 3196
        MsgBox "Die Datenbank wird zur Zeit verwendet.", vbOKOnly
      Case Else
        MsgBox "Ausnahme Nr. " & Err.Number & ". Das hei�t: " & Err.Description
    End Select
    ReparierenKomprimieren = False
    Resume myExit

End Function

'**************************************************************************************
'Liest das Backend Passwort
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'ge�ndert am: 14.12.2016 (aus DB holen)
'R�ckgabewert: string: Passwort des Backends
'ge�ndert 27.05.2019, KAQ: aus Konfiguration holen
'ge�ndert 25.10.2024, KAQ: ASSE-1217 M0 Einbindung M2, M5, M6
'**************************************************************************************
Public Function GetBackendPwd(Optional strModul As String = "") As String
    GetBackendPwd = getKonfigurationDB(IIf(strModul > "", strModul, getModulname))
End Function

'**************************************************************************************
'Gibt das M0 Backend Passwort zurueck
'erstellt am: 11.09.2018
'erstellt von: Tobias Langer
'R�ckgabewert: string: Passwort des Backends
'ASSE-125 M0 Mitarbeiterverwaltung zentral
'ge�ndert 27.05.2019, KAQ: aus Konfiguration holen
'**************************************************************************************
Public Function GetM0BackendPwd() As String
    GetM0BackendPwd = getKonfigurationDB("M0")
End Function

'**************************************************************************************
'Liest die Konfigurationsdatei ein und registriert die Globalen Variablen oder
'erzeugt eine neue Datei
'erstellt am: 11.10.2016
'erstellt von: Tobias Langer
'R�ckgabewert: void
'**************************************************************************************
Public Function getKonfiguration(schluessel As String) As String
    Dim Wert As String
    Wert = ""
    Dim LineFromFile As String
    Dim LineItems() As String
    Dim konfiguration As String
    konfiguration = getKonfigurationsdatei
    If Dir(konfiguration) = "" Then
        MsgBox "Keine Konfigurationsdatei gefunden. Automatisch wurde eine erstellt."
    End If
    
    Open konfiguration For Input As #1
        Do Until EOF(1)
            Line Input #1, LineFromFile
            LineItems = Split(LineFromFile, "=")
            
            If LineItems(0) = schluessel Then
                Wert = LineItems(1)
            End If
        Loop
    Close #1
    getKonfiguration = Wert
End Function

'**************************************************************************************
'Liest ein Parameter Array aus der Konfigurationsdatei ein.
'Parameter haben eine direkt nachfolgende Ziffer oder keine, z.B. GammaPfad= oder GammaPfad2=
'Ziffern von 0 bis 9
'erstellt am: 28.09.2022
'erstellt von: Kai A. Quante
'R�ckgabewert: String()
'ASSE-941 Mx Parameter Array in ini-Datei
'**************************************************************************************
Public Function getKonfigurationArray(schluessel As String) As String()
    Dim i, intCount As Integer
    intCount = 0
    Dim strValue As String
    Dim strValues() As String
    For i = -1 To 9
        strValue = getKonfiguration(schluessel & IIf(i = -1, "", i))
        If strValue > "" Then
            intCount = intCount + 1
            ReDim Preserve strValues(1 To intCount)
            strValues(intCount) = strValue
        End If
    Next i
    getKonfigurationArray = strValues
End Function

'**************************************************************************************
'Liest Wert aus der Konfigurationstabelle ein
'erstellt am: 14.12.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'ge�ndert 27.05.2019, KAQ: ASSE-604 M0 Neue Passworte f�r M0 und M2 Backends
'ge�ndert 02.09.2019, TL: ASSE-654 M7 Basisversion
'ge�ndert 15.10.2020, KAQ: ASSE-814 M6 Eigenst�ndiges Modul
'ge�ndert 13.09.2022, KAQ: ASSE-920 M0 PWDs f�r weitere Module in mdlGlobal: M3, M5, M8, REP
'**************************************************************************************
Public Function getKonfigurationDB(schluessel As String) As String
    Dim strModul As String
    strModul = schluessel
    If strModul = "M0" Then
        getKonfigurationDB = "117-003BE176515"
    ElseIf strModul = "M1" Then
        getKonfigurationDB = "116-018BE162682"
    ElseIf strModul = "M2" Then
        getKonfigurationDB = "117-003BE176515"
    ElseIf strModul = "M3" Then
        getKonfigurationDB = "SFBNHAEPh6RJCpwTxMA"
    ElseIf strModul = "M4" Then
        getKonfigurationDB = "SFBNHAEPh6RJCpwTxMA"
    ElseIf strModul = "M5" Then
        getKonfigurationDB = "sDkNhiNQExrNHAR8i"
    ElseIf strModul = "M6" Then
        getKonfigurationDB = "sDkNhiNQExrNHAR8i"
    ElseIf strModul = "M7" Then
        getKonfigurationDB = "XzULJn4BKyoSYZMUy"
    ElseIf strModul = "M8" Then
        getKonfigurationDB = "XzULJn4BKyoSYZMUy"
    ElseIf strModul = "REP" Then
        getKonfigurationDB = "122-002_45214028"
    ElseIf strModul = "WKP" Then
        getKonfigurationDB = "117-BE172289"
    End If
End Function

'**************************************************************************************
'Hole den Namen der Konfigurationsdatei: identisch zu akt. DB plus ini inkl. Pfad.
'erstellt am: 12.10.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: Pfad der Konfigurationsdatei
'**************************************************************************************
Public Function getKonfigurationsdatei() As String
    getKonfigurationsdatei = Left(CurrentProject.FullName, Len(CurrentProject.FullName) - 5) & "ini"
End Function

'**************************************************************************************
'Existiert eine Konfigurationsdatei
'erstellt am: 12.10.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: true, wenn Datei existiert. Wenn Pfad leer oder nicht existent, dann false.
'**************************************************************************************
Public Function existiertKonfigurationsdatei(Pfad As String) As Boolean
    existiertKonfigurationsdatei = Not (Len(Pfad) = 0 Or Dir(Pfad) = "")
End Function

'**************************************************************************************
'Erstellt die Konfigurationsdatei sofern noch nicht vorhanden
'erstellt am: 11.10.2016
'erstellt von: Tobias Langer
'R�ckgabewert: void
'**************************************************************************************
Public Function erstelleKonfigurationsdatei(Pfad As String, Inhalt As String) As Boolean
On Error GoTo Fehler
    'Pr�fen ob vorhanden
    If existiertKonfigurationsdatei(Pfad) Then
        ' MsgBox "Konfigurationsdatei existiert bereits."
    Else
        Open Pfad For Output As #1
        Print #1, Inhalt
        Close #1
        MsgBox "Konfigurationsdatei '" & Pfad & "' wurde erstellt.", vbInformation
    End If
    erstelleKonfigurationsdatei = True
    Exit Function
Fehler:
    MsgBox "Konfigurationsdatei '" & Pfad & "' konnte nicht erstellt werden.", vbCritical
    erstelleKonfigurationsdatei = False
End Function

'*******************************************************************************************
'Pr�ft auf korrekte Logindaten und setzt die Globale Variable Init_Username
'erstellt am: 23.01.2018
'erstellt von: Tobias Langer
'ASSE-278 WKP Passwortrichtlinien
'R�ckgabewert: integer | 0 = Passwort �ndern; 1 = Anmeldung erfolgreich; -1 = Login nicht erlaubt
'ge�ndert: 05.09.2018 Tobias Langer ASSE-126 M0 Login �ber zentrale Oberfl�che
'ge�ndert: 25.06.2020, KAQ ASSE-742 M0 Implementierung Single Sign On
'*******************************************************************************************
Public Function BenutzerEinloggen(txtBenutzername As Variant, txtPasswort As Variant, Optional boolRemote As Boolean = False) As Integer
    BenutzerEinloggen = -1
    Dim db As DAO.Database
    Dim rstMitarbeiter As DAO.Recordset
    Dim updateAnweisung As String
    Dim sqlAnweisung As String
    
    'Benutzername eingegeben
    If IsNull(txtBenutzername) Or txtBenutzername = "" Then
        MsgBox "Sie haben keinen Benutzernamen eingegeben." & vbCrLf & "Bitte wiederholen Sie Ihre Eingabe.", vbCritical
    Else
        'Benutzerdaten holen
        Set db = CurrentDb
        sqlAnweisung = "SELECT TOP 1 tblMitarbeiter.*, tblPasswort.* FROM tblMitarbeiter INNER JOIN tblPasswort ON tblMitarbeiter.Mitarbeiter_ID = tblPasswort.Mitarbeiter_ID WHERE (((tblMitarbeiter.Mitarbeiter_ID) = '" & txtBenutzername & "')) ORDER BY tblPasswort.geaendert_am DESC"
        
        Set rstMitarbeiter = db.OpenRecordset(sqlAnweisung, dbOpenDynaset, dbSeeChanges)
        If DCountWrapper("*", "tblMitarbeiter", "Mitarbeiter_ID='" & txtBenutzername & "'") = 0 Then
            MsgBox "Der Benutzername ist nicht bekannt." & vbCrLf & "Bitte korrigieren Sie Ihre Eingabe oder kontaktieren Sie Ihren Benutzeradministrator.", vbCritical
        ElseIf DCountWrapper("*", "tblPasswort", "Mitarbeiter_ID='" & txtBenutzername & "'") = 0 And boolRemote <> True Then
            MsgBox "F�r den Benutzer wurde noch kein initiales Passwort entsprechend der Benutzerrichtlinien vergeben." & vbCrLf & "Bitte kontaktieren Sie Ihren Benutzeradministrator.", vbCritical
        Else
            rstMitarbeiter.MoveFirst
            'Mitarbeiter aktiv?
            If rstMitarbeiter!aktiv = 0 Then
                MsgBox "Das Benutzerkonto ist deaktiviert." & vbCrLf & "Bitte wenden Sie sich an Ihren Benutzeradministrator.", vbCritical
            'max. Anmeldeversuche erreicht - Konto f�r 30 Minuten gesperrt
            ElseIf DateDiff("n", rstMitarbeiter!Letzte_Anmeldung, Now()) < 30 And rstMitarbeiter.Fields("Fehlgeschlagende_Anmeldungen") >= 5 Then
                MsgBox "Ihr Konto wurde aufgrund von zu h�ufigen fehlerhaften Anmeldeversuchen f�r 30 Minuten gesperrt." & vbCrLf & "Bitte versuchen Sie es sp�ter erneut oder kontaktieren Sie bitte Ihren Benutzeradministrator, wenn Sie Ihren Benutzernamen oder Passwort vergessen haben.", vbCritical
            ' ASSE-742 M0 Implementierung Single Sign On
            ElseIf LCase(rstMitarbeiter!WindowsUser) = LCase(getWindowsUser) Then
                BenutzerEinloggen = 1
            'Passwort korrekt?
            ElseIf rstMitarbeiter!Passwort <> Nz(txtPasswort, "") And boolRemote <> True Then
                MsgBox "Das eingegebene Passwort ist falsch." & vbCrLf & "Bitte wiederholen Sie Ihre Eingabe.", vbCritical
            Else
                'Initiales Passwort?
                If rstMitarbeiter!Initial = True And boolRemote <> True Then
                    MsgBox "Ihr Passwort muss ge�ndert werden, da Sie noch kein eigenes Passwort vergeben haben bzw. es vom Benutzeradministrator neu vergeben wurde.", vbInformation
                    BenutzerEinloggen = 0
                    'Passwort abgelaufen (60 Tage)
                ElseIf DateDiff("d", rstMitarbeiter.Fields("tblPasswort.geaendert_am"), Now()) >= 60 And boolRemote <> True Then
                    MsgBox "Ihr Passwort muss ge�ndert werden, da es abgelaufen ist.", vbInformation
                    BenutzerEinloggen = 0
                Else
                    BenutzerEinloggen = 1
                End If
            End If
        End If
        rstMitarbeiter.Close
        db.Close
    End If
    
    'Anmeldeinformationen (Versuche, Zeitpunkt) speichern bzw. zur�cksetzen
    If BenutzerEinloggen = -1 Then
        updateAnweisung = "UPDATE tblMitarbeiter SET Fehlgeschlagende_Anmeldungen = IIf(DateDiff('n', Letzte_Anmeldung, Now()) >= 30, 1, Nz(Fehlgeschlagende_Anmeldungen,0)+1), Letzte_Anmeldung = NOW() WHERE Mitarbeiter_ID = '" & txtBenutzername & "' "
        CurrentDb.Execute updateAnweisung, dbSeeChanges
    Else
        updateAnweisung = "UPDATE tblMitarbeiter SET Fehlgeschlagende_Anmeldungen = 0, Letzte_Anmeldung = NOW() WHERE Mitarbeiter_ID = '" & txtBenutzername & "' "
        CurrentDb.Execute updateAnweisung, dbSeeChanges
        mdlGlobal.Init_Username (txtBenutzername)
    End If
End Function

'*******************************************************************************************
'Pr�ft ob SSO m�glich, ASSE-742 M0 Implementierung Single Sign On
'erstellt am: 26.06.2020
'erstellt von: Kai A. Quante
'R�ckgabewert: boolean , ob SSO f�r Windows User m�glich
'ge�ndert: 25.06.2020, KAQ
'*******************************************************************************************
Public Function checkSSO(txtWindowsUser As String) As Boolean
    checkSSO = False
    Dim strMitarbeiter_ID As String
    
    'Benutzername eingegeben
    If IsNull(txtWindowsUser) Or txtWindowsUser = "" Then
        Exit Function
    End If
        
    'Benutzerdaten holen
    strMitarbeiter_ID = DLookupStringWrapper("Mitarbeiter_ID", "tblMitarbeiter", "WindowsUser='" & txtWindowsUser & "'", "")
    
    If strMitarbeiter_ID > "" Then
        If DCountWrapper("*", "tblPasswort", "Mitarbeiter_ID = '" & strMitarbeiter_ID & "'") > 0 _
        And DLookupNumberWrapper("aktiv", "tblMitarbeiter", "Mitarbeiter_ID = '" & strMitarbeiter_ID & "'", 0) <> 0 Then
            ' Fehlgeschlagene Anmeldungen
            If DateDiff("n", DLookupNumberWrapper("Letzte_Anmeldung", "tblMitarbeiter", "Mitarbeiter_ID = '" & strMitarbeiter_ID & "'", 0), Now()) < 30 _
            And DLookupNumberWrapper("Fehlgeschlagende_Anmeldungen", "tblMitarbeiter", "Mitarbeiter_ID = '" & strMitarbeiter_ID & "'", 0) >= 5 Then
                checkSSO = False
            Else
                checkSSO = True
            End If
        End If
    End If
End Function

'*******************************************************************************************
'Pr�ft auf korrekte Daten, Passwortanforderungen und setzt die Globale Variable Init_Username
'erstellt am: 23.01.2018
'erstellt von: Tobias Langer
'ASSE-278 WKP Passwortrichtlinien
'R�ckgabewert: boolean
'*******************************************************************************************
Public Function PasswortAendern(txtPasswortAlt As Variant, txtPasswortNeu As Variant, txtPasswortNeuWiederholung As Variant, txtBenutzername As Variant) As Boolean
    PasswortAendern = False
    Dim strSQL As String
    Dim strBenutzer As String
    Dim strPasswort As String
    Dim db As DAO.Database
    Dim rstPasswort As DAO.Recordset
    
    'Angaben gef�llt?
    If IsNull(txtPasswortAlt) Or txtPasswortAlt = "" Or IsNull(txtPasswortNeu) Or txtPasswortNeu = "" Or IsNull(txtPasswortNeuWiederholung) Or txtPasswortNeuWiederholung = "" Then
        MsgBox "Bitte das alte, das neue und das wiederholte Passwort eingeben.", vbCritical
    Else
        'Aktuelles Passwort korrekt?
        strSQL = "SELECT TOP 1 Passwort FROM tblPasswort WHERE (((Mitarbeiter_ID) = '" & txtBenutzername & "') AND (Passwort = '" & txtPasswortAlt & "')) ORDER BY geaendert_am DESC"
        Set db = CurrentDb
        Set rstPasswort = db.OpenRecordset(strSQL, dbOpenDynaset, dbSeeChanges)
        
        If Not rstPasswort.RecordCount > 0 Then
            MsgBox "Das alte Passwort ist falsch. Bitte wiederholen Sie Ihre Eingabe.", vbCritical
        Else
            'Neues Passwort identisch?
            If txtPasswortNeu <> txtPasswortNeuWiederholung Then
                MsgBox "Das neue Passwort ist mit wiederholtem Passwort nicht identisch. Bitte wiederholen Sie Ihre Eingabe.", vbCritical
            Else
                strBenutzer = txtBenutzername
                strPasswort = txtPasswortNeu
                If PasswortEntsprichtAnforderungen(strPasswort, strBenutzer, True) = True Then
                    strSQL = "INSERT INTO tblPasswort (Mitarbeiter_ID, Passwort,geaendert_von, geaendert_am) VALUES ('" & txtBenutzername & "','" & txtPasswortNeu & "','" & txtBenutzername & "',NOW()) "
                    CurrentDb.Execute strSQL, dbSeeChanges
                    MsgBox "Ihr Passwort wurde ge�ndert.", vbInformation
                    PasswortAendern = True
                End If
            End If
        End If
        rstPasswort.Close
        db.Close
    End If
End Function

'*******************************************************************************************
'Pr�ft auf korrekte Daten, Passwortanforderungen und setzt die Globale Variable Init_Username
'erstellt am: 23.01.2018
'erstellt von: Tobias Langer
'ASSE-278 WKP Passwortrichtlinien
'R�ckgabewert: boolean
'*******************************************************************************************
Public Function PasswortAendernAdmin(txtPasswortNeu As Variant, txtPasswortNeuWiederholung As Variant, txtBenutzername As Variant) As Boolean
    PasswortAendernAdmin = False
    Dim strSQL As String
    Dim strBenutzer As String
    Dim strPasswort As String
    Dim db As DAO.Database
    Dim rstPasswort As DAO.Recordset
    
    strBenutzer = txtBenutzername
    strPasswort = txtPasswortNeu
    
    'Angaben gef�llt?
    If IsNull(txtPasswortNeu) Or txtPasswortNeu = "" Or IsNull(txtPasswortNeuWiederholung) Or txtPasswortNeuWiederholung = "" Then
        MsgBox "Bitte neue und das wiederholte Passwort eingeben.", vbCritical
    Else
        'Neues Passwort identisch?
        If txtPasswortNeu <> txtPasswortNeuWiederholung Then
            MsgBox "Das neue Passwort ist mit wiederholtem Passwort nicht identisch. Bitte wiederholen Sie Ihre Eingabe.", vbCritical
        Else
            'Initiales Passwort bereits verwendet?
            strSQL = "SELECT TOP 1 Passwort FROM tblPasswort WHERE (((Mitarbeiter_ID) = '" & strBenutzer & "') AND (Passwort = '" & strPasswort & "'))"
            Set db = CurrentDb
            Set rstPasswort = db.OpenRecordset(strSQL, dbOpenDynaset, dbSeeChanges)
            If Not rstPasswort.RecordCount = 0 Then
                MsgBox "Das Passwort wurde bereits verwendet. Bitte geben Sie ein neues ein.", vbCritical
            Else
                If PasswortEntsprichtAnforderungen(strPasswort, strBenutzer, True) = True Then
                    strSQL = "INSERT INTO tblPasswort (Mitarbeiter_ID, Passwort,Initial,geaendert_von, geaendert_am) VALUES ('" & txtBenutzername & "','" & txtPasswortNeu & "',1,'" & fctGetUser() & "',NOW()) "
                    CurrentDb.Execute strSQL, dbSeeChanges
                    strSQL = "UPDATE tblMitarbeiter SET Fehlgeschlagende_Anmeldungen = 0 WHERE Mitarbeiter_ID = '" & txtBenutzername & "' "
                    CurrentDb.Execute strSQL, dbSeeChanges
                    MsgBox "Das Passwort wurde ge�ndert. Der Mitarbeiter kann sich nun damit anmelden.", vbInformation
                    PasswortAendernAdmin = True
                End If
            End If
            rstPasswort.Close
            db.Close
        End If
    End If
End Function

'*******************************************************************************************
'Setzt das Anwendungsicon
'erstellt am: 31.07.2018
'erstellt von: Tobias Langer
'ASSE-206 Mx Anwendungsicon
'*******************************************************************************************
Public Sub SetAppIcon()
    Dim db As DAO.Database, prp As DAO.Property, IconPath As String
    IconPath = Application.CurrentProject.path & "\icon.ico"
    
On Error GoTo GotErr
   
   Set db = CurrentDb
   
   db.Properties("AppIcon") = IconPath 'Attempt to assign
   Application.RefreshTitleBar 'Update On Screen!

Exit Sub

GotErr:
   If Err.Number = 3270 Then 'property doesn't exist!
      Set prp = db.CreateProperty("AppIcon", dbText, IconPath)
      db.Properties.Append prp 'New property set!
   Else
      MsgBox "Error: " & Err.Number & vbCrLf & Err.Description
   End If
   
   Resume Next

End Sub


'**********************************************************
'Aktualisiert die Verkn�pfung von SQL Server Tabellen
'erstellt am: 21.08.2018
'erstellt von: Tobias Langer
'**********************************************************
Public Sub RefreshODBCLinks(newConnectionString As String)
    Dim db As DAO.Database
    Dim tb As DAO.TableDef
    Dim originalname As String
    Dim tempname As String
    Dim sourcename As String
    Dim i As Integer

    Set db = CurrentDb
    ' Get a list of all ODBC tables '
    Dim tables As New Collection
    For Each tb In db.TableDefs
        If (Left(tb.Connect, 4) = "ODBC") Then
            tables.Add Item:=tb.Name, Key:=tb.Name
        End If
    Next tb

    ' Create new tables using the given DSN after moving the old ones '
    For i = tables.count To 1 Step -1
            originalname = tables(i)
            tempname = "~" & originalname & "~"
            sourcename = db.TableDefs(originalname).SourceTableName
            ' Create the replacement table '
            db.TableDefs(originalname).Name = tempname
            Set tb = db.CreateTableDef(originalname, dbAttachSavePWD, _
                                        sourcename, newConnectionString)
            db.TableDefs.Append tb
            db.TableDefs.Refresh
            ' delete the old table '
            DoCmd.DeleteObject acTable, tempname
            db.TableDefs.Refresh
            tables.Remove originalname
    Next i
    Set db = Nothing
    
    removeDbPraefix "dbo_"
End Sub

'**********************************************************
'Entfernt das Datenbankschema in den verkn�pften Tabellen
'erstellt am: 21.08.2018
'erstellt von: Tobias Langer
'**********************************************************
Public Sub removeDbPraefix(strPraefix As String)
    Dim obj As AccessObject
    Dim dbs As Object
 
    Set dbs = Application.CurrentData
 
    'Search for open AccessObject objects in AllTables collection.
    For Each obj In dbs.AllTables
        'If found, remove prefix
       If Left(obj.Name, Len(strPraefix)) = strPraefix Then
            DoCmd.Rename Mid(obj.Name, Len(strPraefix) + 1), acTable, obj.Name
        End If
    Next obj
End Sub

'**************************************************************************************
'Datei �ffnen entnommen aus http://www.office-loesung.de/ftopic527748_0_0_asc.php
'ToDo: aus Tabelle?
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: string: Passwort des Backends
'**************************************************************************************
Public Function DateiOeffnen(Aktion As String, Pfad As String, Ansicht As Long) As String
    Const SE_ERR_FNF = 2&
    Const SE_ERR_PNF = 3&
    Const SE_ERR_ACCESSDENIED = 5&
    Const SE_ERR_OOM = 8&
    Const SE_ERR_DLLNOTFOUND = 32&
    Const SE_ERR_SHARE = 26&
    Const SE_ERR_ASSOCINCOMPLETE = 27&
    Const SE_ERR_DDETIMEOUT = 28&
    Const SE_ERR_DDEFAIL = 29&
    Const SE_ERR_DDEBUSY = 30&
    Const SE_ERR_NOASSOC = 31&
    Const ERROR_BAD_FORMAT = 11&
    Dim msg As String
'Fehler R�ckgabe der Shellfunktion auswerten:
    Select Case ShellExecute(hWnd, Aktion, Pfad, "", "", Ansicht)
      Case SE_ERR_FNF
        msg = "Datei nicht gefunden."
      Case SE_ERR_PNF
        msg = "Pfad nicht gefunden."
      Case SE_ERR_ACCESSDENIED
        msg = "Dateizugriff verweigert."
      Case SE_ERR_OOM
        msg = "Out of memory"
      Case SE_ERR_DLLNOTFOUND
        msg = "DLL f�r den Aufruf nicht gefunden."
      Case SE_ERR_SHARE
        msg = "Datei ist von anderem Anwender gesperrt."
      Case SE_ERR_ASSOCINCOMPLETE
        msg = "Unvollst�ndige und falscher Dateibezug."
      Case SE_ERR_DDETIMEOUT
        msg = "DDE Time out"
      Case SE_ERR_DDEFAIL
        msg = "DDE transaction failed"
      Case SE_ERR_DDEBUSY
        msg = "DDE busy"
      Case SE_ERR_NOASSOC
        msg = "Die Datei ist keiner Anwendung zugewiesen."
      Case ERROR_BAD_FORMAT
        msg = "Die Datei hat kein g�ltiges Format."
    End Select
    DateiOeffnen = msg
End Function

'**************************************************************************************
'Backend Tabellen werden neu angebunden
' TODO neues Passwort
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: boolean: true|false
'geaendert: 10.09.2018 Tobias Langer ASSE-125 M0 Mitarbeiterverwaltung zentral
'geaendert: 23.10.2018 Tobias Langer ASSE-502 Mx Modulschnittstellen
'geaendert: 10.12.2018 Kai A. Quante ASSE-502 Mx Modulschnittstellen, dynamische Einbindung
'geaendert: 27.05.2019 Kai A. Quante ASSE-605 M0 Tabellen einbinden f�r remote Zugriff
'geaendert: 20.10.2020 Kai A. Quante ASSE-818 M0 Performanceoptimierung beim Start eines neuen Moduls
'geaendert: 25.10.2024 Kai A. Quante ASSE-1217 M0 Einbindung M2, M5, M6
'**************************************************************************************
Public Function TabellenEinbinden(strPfad As String) As Boolean

    TabellenEinbinden = False

On Error GoTo Fehler

    Dim db As DAO.Database
    Dim tdf As DAO.TableDef
    Dim strPassword As String
    Dim strPasswordM0 As String
    Dim strPfadM0 As String
    Dim strModul As String
    Dim strTempModul As String
    Dim strTabelle As String
    Dim strTempPfad As String
    Dim strTdfPfad As String
    Dim intCount As Integer
    intCount = 0
    Set db = CurrentDb()
    strModul = getModulname
    strPassword = GetBackendPwd(getModulname)
    strPasswordM0 = GetM0BackendPwd
    strPfadM0 = IIf(strModul = "M0", getKonfiguration("BACKEND_PFAD"), getKonfiguration("BACKEND_M0_PFAD"))
    
    For Each tdf In db.TableDefs
        'Fortschritt
        intCount = intCount + 1
        Fortschritt intCount / db.TableDefs.count * 100, "Aktualisieren: " & tdf.Name
        
        'alle Tabellen ausser System/ODBC/lokale Tabellen neu verbinden
        strTabelle = tdf.Name
        If Left(tdf.Name, 4) <> "MSys" And Left(tdf.Name, 4) <> "~TMP" And Left(tdf.Connect, 4) <> "ODBC" And tdf.Connect <> "" Then
            'M0 falls bereits eingebunden, ansonsten entsprechendes modul
            strTdfPfad = Mid(tdf.Connect, InStr(tdf.Connect, "DATABASE=") + 9, InStr(tdf.Connect, ".accdb"))
            If InStr(tdf.Connect, "_M0_") > 0 Then
                strTempPfad = strPfadM0
                tdf.Connect = ";PWD=" & strPasswordM0 & ";DATABASE=" & strPfadM0
            'anderes Backendmodul angebunden als im Pfad �bergeben und nicht M0
            ElseIf strModul <> extrahiereModul(tdf.Connect) Then
                strTempModul = extrahiereModul(tdf.Connect)
                strTempPfad = getKonfiguration("BACKEND_" & strTempModul & "_PFAD")
                tdf.Connect = ";PWD=" & GetBackendPwd(strTempModul) & ";DATABASE=" & strTempPfad
            Else
                strTempPfad = strPfad
                tdf.Connect = ";PWD=" & strPassword & ";DATABASE=" & strPfad
            End If
            If strTdfPfad <> strTempPfad Then
                tdf.RefreshLink
            End If
        End If
    Next tdf
    db.Close
    Fortschritt 101, "Ende"
    Set db = Nothing
    TabellenEinbinden = True
  Exit Function
Fehler:
    Fortschritt 101, "Ende"
    MsgBox "Die Tabelle " & strTabelle & " aus der Datenbank " & strTempPfad & " konnte nicht neu eingebunden werden." & vbCrLf & vbCrLf & "Error-Number: " & Err.Number & vbCrLf & Err.Description, vbCritical, "RMDS"
'    If Err.Number = 3005 Or Err.Number = 3024 Or Err.Number = 53 Or Err.Number = 3044 Or Err.Number = 76 Then
        ' Fehler 3024: "Datei "|" nicht gefunden".
        ' Fehler 3005: "|" ist kein zul�ssiger Datenbankname
        ' Fehler 3044: "|" ist kein zul�ssiger Pfad
'        MsgBox "Der angegebene Pfad ist nicht vorhanden." & vbCrLf & "Die Datenbank " & strPfad & " konnte nicht gefunden werden.", vbCritical, "RMDS"
'    Else
'      MsgBox "Tabellen konnten nicht eingebunden werden." & vbNewLine & vbNewLine & _
'          Err.Number & vbNewLine & _
'          Err.Description, vbCritical, "RMDS"
'    End If
    TabellenEinbinden = False
End Function

'**************************************************************************************
'Extrahiere Modul aus einem Connect String zwischen "RMDS_" und "_BE"
'erstellt am: 25.10.2024
'erstellt von: Kai A. Quante
'R�ckgabewert: Modul wie M0, M2 oder REP
'**************************************************************************************
Public Function extrahiereModul(strConnect As String) As String
    Dim startPos As Integer
    Dim endPos As Integer
    
    ' Startposition von "RMDS_" finden
    startPos = InStr(strConnect, "RMDS_") + Len("RMDS_")
    
    ' Endposition von "_BE" finden
    endPos = InStr(startPos, strConnect, "_BE")
    
    ' Text zwischen "RMDS_" und "_BE" extrahieren
    extrahiereModul = Mid(strConnect, startPos, endPos - startPos)
End Function

'**************************************************************************************
'Backend Tabellen werden neu angebunden
' TODO neues Passwort
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: boolean: true|false
'geaendert: 10.09.2018 Tobias Langer ASSE-125 M0 Mitarbeiterverwaltung zentral
'geaendert: 23.10.2018 Tobias Langer ASSE-502 Mx Modulschnittstellen
'**************************************************************************************
Public Function TabellenEinbinden_OLD(strPfad As String) As Boolean
On Error GoTo Fehler

    Dim db As DAO.Database
    Dim tdf As TableDef
    Dim strPassword As String
    Dim strPasswordM0 As String
    Dim strPfadM0 As String
    Dim strModul As String
    
    Set db = CurrentDb
    strPassword = GetBackendPwd
    strModul = getModulname
    strPasswordM0 = GetM0BackendPwd
    
    strPfadM0 = getKonfiguration("BACKEND_M0_PFAD")
    If strPfadM0 = "" Then
        strPfadM0 = Application.CurrentProject.path & "\..\rmds-m0\RMDS_M0_BE.accdb"
    End If
    
    For Each tdf In db.TableDefs
        'alle Tabellen ausser System/ODBC/lokale Tabellen neu verbinden
        If Left(tdf.Name, 4) <> "MSys" And Left(tdf.Name, 4) <> "~TMP" And Left(tdf.Connect, 4) <> "ODBC" And tdf.Connect <> "" Then

MsgBox tdf.Name & ": " & tdf.Connect & " " & InStr(tdf.Connect, "_M0_")
            
            If strModul = "M4" Then
                If tdf.Name = "tblMessgeraete" Or tdf.Name = "tblSohle" Or tdf.Name = "tblM0Teilbereich" Or tdf.Name = "tblM0ProbeZusatzangaben" Or tdf.Name = "tblMessstelle" Or tdf.Name = "tblM0Probe" Or tdf.Name = "tblBerechtigung" Or tdf.Name = "tblMitarbeiter" Or tdf.Name = "tblMitarbeiterRollen" Or tdf.Name = "tblPasswort" Or tdf.Name = "tblRolle" Or tdf.Name = "tblRolleBerechtigungen" Then
                    tdf.Connect = ";PWD=" & strPasswordM0 & _
                    ";DATABASE=" & strPfadM0
                    tdf.RefreshLink
                Else
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                End If
            ElseIf strModul = "M2" Then
                If tdf.Name = "tblM0Probe" Or tdf.Name = "tblMessstelle" Or tdf.Name = "tblSohle" Or tdf.Name = "tblM0MessstelleTyp" Or tdf.Name = "tblBerechtigung" Or tdf.Name = "tblMitarbeiter" Or tdf.Name = "tblMitarbeiterRollen" Or tdf.Name = "tblPasswort" Or tdf.Name = "tblRolle" Or tdf.Name = "tblRolleBerechtigungen" Then
                    tdf.Connect = ";PWD=" & strPasswordM0 & _
                    ";DATABASE=" & strPfadM0
                    tdf.RefreshLink
                Else
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                End If
            ElseIf strModul = "M0" Then
                If tdf.Name = "tblBerechtigung" Or tdf.Name = "tblMitarbeiter" Or tdf.Name = "tblMitarbeiterRollen" Or tdf.Name = "tblPasswort" Or tdf.Name = "tblRolle" Or tdf.Name = "tblRolleBerechtigungen" Then
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                Else
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                End If
            ElseIf strModul = "WKP" Then
                If tdf.Name = "tblBerechtigung" Or tdf.Name = "tblMitarbeiter" Or tdf.Name = "tblMitarbeiterRollen" Or tdf.Name = "tblPasswort" Or tdf.Name = "tblRolle" Or tdf.Name = "tblRolleBerechtigungen" Then
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                Else
                    tdf.Connect = ";PWD=" & strPassword & _
                    ";DATABASE=" & strPfad
                    tdf.RefreshLink
                End If
            End If
        End If
    Next tdf
    db.Close
    Set db = Nothing
    TabellenEinbinden_OLD = True
    
  Exit Function
Fehler:
    
    If Err.Number = 3005 Or Err.Number = 3024 _
        Or Err.Number = 53 Or Err.Number = 3044 _
        Or Err.Number = 76 Then
        ' Fehler 3024: "Datei "|" nicht gefunden".
        ' Fehler 3005: "|" ist kein zul�ssiger Datenbankname
        ' Fehler 3044: "|" ist kein zul�ssiger Pfad
        MsgBox "Der angegebene Pfad ist nicht vorhanden." & _
            Chr$(10) & Chr$(13) & "Die Datenbank " & strPfad & _
            "konnte nicht gefunden werden.", vbCritical, "RMDS"
    Else
      MsgBox "Tabellen konnten nicht eingebunden werden." & vbNewLine & vbNewLine & _
          Err.Number & vbNewLine & _
          Err.Description, vbCritical, "RMDS"
    End If
    TabellenEinbinden_OLD = False
End Function


'**************************************************************************************
'Liest, wenn vorhanden Konf. Datei aus und Gibt Pfad des Backends zur�ck
'ToDo: Zugriff auf Backend �ber Pfad aus Konfig-Datei
'      Lesen �ber Word als OLE geht nicht auf gemischten Systemen!
'erstellt am: 01.01.2016
'erstellt von: Kai A. Quante
'R�ckgabewert: string: Pfad zur Konfig-Datei
'**************************************************************************************
Public Function GetBackendPfad() As String
    GetBackendPfad = getKonfiguration("BACKEND_PFAD")
End Function

'**************************************************************************************
'Liefert den Modulnamen zur�ck, d.h. ohne vorangestelltes "RMDS_"
'erstellt am: 11.09.2018
'erstellt von: Tobias Langer
'R�ckgabewert: void
'ge�ndert 02.09.2019, TL: ASSE-654 M7 Basisversion
'ge�ndert 15.10.2019, KAQ: ASSE-814 M6 Eigenst�ndiges Modul
'ge�ndert 13.09.2022, KAQ: ASSE-920 M0 PWDs f�r weitere Module in mdlGlobal: M3, M5, M8, REP
'**************************************************************************************
Public Function getModulname() As String
    getModulname = ""
    Dim strModul As String
    strModul = CurrentProject.FullName
    If InStr(strModul, "WKP") <> 0 Then
        getModulname = "WKP"
    ElseIf InStr(strModul, "RMDS_REP") <> 0 Then
        getModulname = "REP"
    ElseIf InStr(strModul, "RMDS_M0") <> 0 Then
        getModulname = "M0"
    ElseIf InStr(strModul, "RMDS_M1") <> 0 Then
        getModulname = "M1"
    ElseIf InStr(strModul, "RMDS_M2") <> 0 Then
        getModulname = "M2"
    ElseIf InStr(strModul, "RMDS_M3") <> 0 Then
        getModulname = "M3"
    ElseIf InStr(strModul, "RMDS_M4") <> 0 Then
        getModulname = "M4"
    ElseIf InStr(strModul, "RMDS_M5") <> 0 Then
        getModulname = "M5"
    ElseIf InStr(strModul, "RMDS_M6") <> 0 Then
        getModulname = "M6"
    ElseIf InStr(strModul, "RMDS_M7") <> 0 Then
        getModulname = "M7"
    ElseIf InStr(strModul, "RMDS_M8") <> 0 Then
        getModulname = "M8"
    End If
End Function

'**************************************************************************************
'Schreibt einen Fehler in die tblErrors
'erstellt am: 13.05.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'ge�ndert 02.11.2022, KAQ: ASSE-966 Mx Error Meldung in DB ohne Message
'ge�ndert 17.05.24 KAQ: ASSE-1165 Mx reportError auch mit einfachen Anf�hrungszeichen in Description
'**************************************************************************************
Public Sub reportError(strType As String, strModul As String, strDescription As String, Optional lngProbeId As Long = 0, Optional strReferenz As String = "", Optional boolMsgBox As Boolean = True)
    Dim strSQL As String
    If boolMsgBox Then
        MsgBox strDescription & IIf(lngProbeId > 0, vbCrLf & vbCrLf & "Probe ID: " & Format(lngProbeId, "00,000,000"), "") & IIf(strReferenz > "", vbCrLf & "Referenz: " & strReferenz, ""), IIf(strType = "Error", vbCritical, IIf(strType = "Warning", vbExclamation, vbInformation)), "Modul " & strModul
    End If
    strSQL = "INSERT INTO tblError (Type, Modul, Beschreibung, erstellt_von, erstellt_am, Probe_ID_f, Referenz) " & _
            "VALUES ('" & strType & "','" & strModul & "','" & "[" & fctGetUser & ", " & Format(Now(), "DD.MM.YYYY HH:mm") & "]" & vbCrLf & Replace(strDescription, "'", "�") & "','" & fctGetUser & "', NOW(), " & lngProbeId & ",'" & strReferenz & "')"
    CurrentDb.Execute strSQL
End Sub

'**************************************************************************************
'F�gt einen Kommentar zu einem Fehler hinzu.
'Aktueller User und Timestamp werden vor die Beschreibung gesetzt.
'erstellt am: 13.05.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: void
'**************************************************************************************
Public Sub addErrorDescription(lngErrorId As Long, strDescription As String)
    Dim strSQL As String
    strSQL = "UPDATE tblError SET [Beschreibung] = " & _
            "'[" & fctGetUser & ", " & Format(Now(), "DD.MM.YYYY HH:mm") & "]" & vbCrLf & strDescription & vbCrLf & "' & [Beschreibung] WHERE [Error_ID] = " & lngErrorId
    CurrentDb.Execute strSQL
End Sub

'**************************************************************************************
'Liefert True, falls die Datei nicht ge�ffnet werden k�nnte
'erstellt am: 09.05.2019
'erstellt von: Kai A. Quante
'R�ckgabewert: boolean
'**************************************************************************************
Public Function isFileOpen(strFile As String) As Boolean
    On Error Resume Next
    Open strFile For Binary Access Read Lock Read As #1
    Close #1
    If Err.Number <> 0 Then
        isFileOpen = True
        Err.Clear
    End If
End Function

'**************************************************************************************
'Diese Methode zeigt das Formular frmFortschritt an und setzt den Fortswchrittsgrad.
'strMeldung : setzt die auszugebende Meldung
'intAnteil gibt den prozentualen Fertigstellungsgrad an, Werte �ber 100 schlie�en das Formular!
'erstellt am: 21.03.2018
'erstellt von: Kai A. Quante
'ge�ndert 22.07.2019, KAQ: ASSE-638 Mx Fortschritt max. alle 1 Sekunden
'ge�ndert 28.09.2022, KAQ: ASSE-930 Mx Aktualisierung Fortschrittsanzeige
'ge�ndert 28.09.2022, KAQ: ASSE-938 M6 Performance Import
'ge�ndert 28.09.2022, KAQ: ASSE-937 M2 Performance Import
'**************************************************************************************
Public Sub Fortschritt(intAnteil As Integer, strMeldung As String)
    Dim intRot As Integer
    Dim intGruen As Integer
    Dim boolRepaint As Boolean
    If DateDiff("s", timestampFortschritt, Now()) < 1 And intAnteil <= 100 Then
        Exit Sub
    Else
        timestampFortschritt = Now()
        ' Events zulassen max alle 10 Sek.
        If DateDiff("s", timestampDoEvens, Now()) >= 5 And intAnteil > 10 Then
            timestampDoEvens = Now()
            DoEvents
        End If
    End If
    
    'Meldung ist der angezeigte Text
    'Anteil ist eine Integerzahl - es gibt 3 M�glichkeiten:
    ' 1. Anteil= 0    (nur der rote Balken ist sichtbar)
    ' 2. Anteil ist zwischen 1 und 100 (der gr�ne Balken �berdeckt den roten um
    '                                   diesen Anteil)
    ' 3. Anteil > 100  (schaltet das Formular aus)
    'FortschrittBalken vorbereiten
    ' falls das Formular zum ersten mal aufgerufen wird, wird es ge�ffnet
    If IstFormOffen("frmFortschritt") = False Then
        DoCmd.OpenForm "frmFortschritt", acNormal
        With Forms("frmFortschritt")
            ' Titel des Formulars
            .Caption = "Fortschrittsanzeige"
            ' Sicherstellen, dass die beiden Balken genau �bereinander liegen
            .Controls("GruenBalken").Left = .Controls("Rotbalken").Left
            .Controls("GruenBalken").Top = .Controls("Rotbalken").Top
            .Controls("GruenBalken").Height = .Controls("Rotbalken").Height
            'Auszugebender Text ist erstmal leer
            .Controls("Anteil").Caption = ""
            .Controls("Meldung").Caption = ""
        End With
    End If
    Select Case intAnteil
      Case Is > 100:  ' Formular ausschalten und schliessen
        Forms("frmFortschritt").Controls("GruenBalken").Visible = False
        Forms("frmFortschritt").Controls("RotBalken").Visible = False
        DoCmd.Close acForm, "frmFortschritt"
        Exit Sub
      Case 0: ' Initial .... nur der rote Balken ist sichtbar
        Forms("frmFortschritt").Controls("RotBalken").Visible = True
        Forms("frmFortschritt").Controls("GruenBalken").Visible = False
      Case Else ' Zahl zwischen 1 und 100
        If Not Forms("frmFortschritt").Controls("RotBalken").Visible Then
            'roter Balken sichtbar
            Forms("frmFortschritt").Controls("RotBalken").Visible = True
        End If
        'Gesamtl�nge von rot
        intRot = Forms("frmFortschritt").Controls("RotBalken").Width
        'Gesamtl�nge von gr�n ist Anzahl-Prozent von rot
        intGruen = Int(intRot / 100 * intAnteil)
        'Balkenl�nge setzen
        Forms("frmFortschritt").Controls("GruenBalken").Width = intGruen
        'und  anzeigen
        If Not Forms("frmFortschritt").Controls("GruenBalken").Visible = True Then
            Forms("frmFortschritt").Controls("GruenBalken").Visible = True
        End If
    End Select
    'Falls ein neuer Text ausgegeben werden soll, wird die Caption ge�ndert
    'Wenn die Caption bei jedem Aufruf ge�ndert wird,
    'flimmert der Text je nach Rechnerleistung
    boolRepaint = False
    If Forms("frmFortschritt").Controls("Meldung").Caption <> strMeldung Then
        Forms("frmFortschritt").Controls("Meldung").Caption = strMeldung
        boolRepaint = True
    End If
    If Forms("frmFortschritt").Controls("Anteil").Caption <> intAnteil & " %" Then
        Forms("frmFortschritt").Controls("Anteil").Caption = intAnteil & " %"
        boolRepaint = True
    End If
    If boolRepaint Then
'        Forms("frmFortschritt").Requery
        Forms("frmFortschritt").Repaint
    End If
End Sub


'**************************************************************************************
'Speichert die anzuzeigenden Felder zu einem Formular und zu einem Mitarbeiter
'erstellt am: 17.05.2019
'erstellt von: Tobias Langer
'R�ckgabewert: Void
'ToDo: funktioniert nicht !!!
'**************************************************************************************
Public Function SpeicherFormularLayout(frmForm As Form, strMitarbeiter_ID_f As String, strModul As String)
    Dim ctl As Control
    Dim strInsert
    Dim strDelete
    Dim prp As Property
    
    'Loesche vorhandene Konfiguration
    strDelete = "DELETE FROM tblFormLayout WHERE Formular = '" + frmForm.Name + "' AND Mitarbeiter_ID_f = '" + strMitarbeiter_ID_f + "' AND Modul = '" + strModul + "'"
    CurrentDb.Execute strDelete
    
    'Speichere neue Konfiguration
    For Each ctl In frmForm.Controls
        'If (ctl.ColumnHidden = True) Then 'Funktioniert nicht
            strInsert = "INSERT INTO tblFormLayout(Formular, Mitarbeiter_ID_f, Modul, Feld, Breite) VALUES('" + frmForm.Name + "','" + strMitarbeiter_ID_f + "','" + strModul + "','" + ctl.Name + "','" + CStr(ctl.Width) + "')"
            CurrentDb.Execute strInsert
        'End If
    Next ctl
    
    'frmForm.Requery
    
    MsgBox "Formularfelder wurden gespeichert.", vbOKOnly
End Function

'**************************************************************************************
'Laed die anzuzeigenden Felder zu einem Formular und zu einem Mitarbeiter aus der Konfiguration
'erstellt am: 18.05.2019
'erstellt von: Tobias Langer
'R�ckgabewert: Void
'ToDo: funktioniert nicht !!!
'**************************************************************************************
Public Function LadeFormularLayout(frmForm As Form, strMitarbeiter_ID_f As String, strModul As String)
    Dim db As DAO.Database
    Dim rst As DAO.Recordset
    Dim ctl As Control
    Dim strQuery

    Set db = CurrentDb

    'Lade neue Konfiguration
    strQuery = "SELECT * FROM tblFormLayout WHERE Formular = '" + frmForm.Name + "' AND Mitarbeiter_ID_f = '" + strMitarbeiter_ID_f + "' AND Modul = '" + strModul + "'"
    Set rst = db.OpenRecordset(strQuery, dbOpenDynaset)

    ' If the recordset is empty, exit.
    If Not rst.EOF And Not rst.BOF Then
        'Alle ausblenden
        For Each ctl In frmForm.Controls
            'Funktioniert nicht
            'ctl.SetFocus = False
            'ctl.Visible = False
        Next ctl
        
        'Eigene Felder einblenden
        Do Until rst.EOF
            frmForm.Form.Controls(rst!Feld).Visible = True
            frmForm.Form.Controls(rst!Feld).Width = 10
           rst.MoveNext
        Loop
    End If

    rst.Close
    db.Close
    
    frmForm.Requery
   
    
End Function

'**************************************************************************************
'Loescht die Konfiguration von Feldern zu einem Formular und zu einem Mitarbeiter
'erstellt am: 18.05.2019
'erstellt von: Tobias Langer
'R�ckgabewert: Void
'**************************************************************************************
Public Function ResetFormularLayout(frmForm As Form, strMitarbeiter_ID_f As String, strModul As String)
    Dim ctl As Control
    Dim strDelete
    'Loesche vorhandene Konfiguration
    strDelete = "DELETE FROM tblFormLayout WHERE Formular = '" + frmForm.Name + "' AND Mitarbeiter_ID_f = '" + strMitarbeiter_ID_f + "' AND Modul = '" + strModul + "'"
    CurrentDb.Execute strDelete
    For Each ctl In frmForm.Controls
        ctl.Visible = True
    Next ctl
    MsgBox "Formularfelder wurden zur�ckgesetzt.", vbOKOnly
End Function


'**********************************************************
'Pr�fung ob Feld vorhanden
'erstellt von: Kai A. Quante
'erstellt am: 07.05.2019
'**********************************************************
Public Function existsField(strTabName As String, strFldName As String) As Boolean
    Dim f As DAO.Field
    On Error Resume Next
      Set f = CurrentDb.TableDefs(strTabName).Fields(strFldName)
      existsField = Not (Err.Number = 3265)
End Function

'**********************************************************
'Feld einer Tabelle hinzu f�gen, wird am Ende angeh�ngt
'strModul: Modul f�r das Backend z.B. M0
'strTable: Tabelle
'strField: Feld
'intType: Typ z.B. dbBoolean
'varDefault: Default-Wert ungleich Null
'strReleaseNote: Aktuelle Version des Frontend wird als Version mit aktuellem Datum und Text als Releasetext genommen. Bei leerem Text wird keine Verison geschrieben.
'strVersion: optional falls eine bestimmte Version gesetzt werden soll.
'erstellt von: Kai A. Quante
'erstellt am: 06.12.2022
'**********************************************************
Public Sub addField(strModule As String, strTable As String, strField As String, intType As Integer, Optional varDefault As Variant = Null, Optional strReleaseNote As String = "", Optional strVersion As String = "")
    Dim objAccess As Access.Application
    Dim dbs As DAO.Database, td As TableDef
    Dim strDB As String
    
    'Gibts die Spalte, dann erledigt.
    If existsField(strTable, strField) Then
        Exit Sub
    End If
    
    'Spalte hinzu f�gen
    strDB = getKonfiguration("BACKEND_PFAD")
    Set objAccess = CreateObject("Access.Application")
    objAccess.OpenCurrentDatabase strDB, , strModule
    Set dbs = objAccess.CurrentDb
    Set td = dbs.TableDefs(strTable)
    With td
        .Fields.Append .CreateField(strField, intType)
        .Fields.Refresh
        If Not IsNull(varDefault) Then
            .Fields(strField).DefaultValue = varDefault
        End If
    End With
    Set td = Nothing
    'Release Note erg�nzen
    If strReleaseNote > "" Then
        dbs.Execute ("INSERT INTO tblVersionBackend (Version, Versionsdatum, Bemerkung) VALUES ('" & IIf(strVersion > "", strVersion, DLookupStringWrapper("Version", "qryVersionFrontend")) & "', CDATE('" & Now() & "') , '" & strReleaseNote & "')")
    End If
    
    Set dbs = Nothing
    objAccess.Application.Quit
End Sub

'**************************************************************************************
'Z�hlt die Elemente eines Arrays
'erstellt von: Kai A. Quante
'erstellt am: 12.08.2022
'return: long
'**************************************************************************************
Public Function countArray(varArray() As String) As Long
    countArray = IIf(IsEmpty(varArray), 0, (UBound(varArray) - LBound(varArray) + 1))
End Function

'**************************************************************************************
'Liefert eine Datei als String Array
'erstellt von: Kai A. Quante
'erstellt am: 08.07.2019
'return: String()
'**************************************************************************************
Public Function getFileAsStringArray(strFile As String) As String()
    Dim LineFromFile As String
    Dim LineItems() As String
    Dim i As Integer
    i = 0
    Open strFile For Input As #1
    While Not EOF(1)
        Line Input #1, LineFromFile
        ReDim Preserve LineItems(i)
        LineItems(i) = LineFromFile
        i = i + 1
    Wend
    Close #1
    getFileAsStringArray = LineItems
End Function

'**************************************************************************************
'Liefert String vom String Array
'erstellt von: Kai A. Quante
'erstellt am: 14.11.2022
'return: String
'**************************************************************************************
Public Function getStringFromStringArray(strArray() As String) As String
    Dim i As Integer
    Dim strResult As String
    strResult = ""
    For i = LBound(strArray) To UBound(strArray)
        strResult = strResult & strArray(i) & vbCrLf
    Next i
    getStringFromStringArray = strResult
End Function

'**************************************************************************************
'Liefert einen Teilstring zur�ck zwischen Start und Ende
'erstellt von: Kai A. Quante
'erstellt am: 19.01.2019
'return: String
'**************************************************************************************
Public Function getStringData(strInput As String, strStart As String, Optional strEnde As String = "") As String
    Dim intStart As Integer
    Dim intLaenge As Integer
    intStart = InStr(1, strInput, strStart) + Len(strStart)
    intLaenge = IIf(strEnde = "", Len(strInput) + 1, InStr(1, strInput, strEnde)) - intStart
    getStringData = Mid(strInput, intStart, intLaenge)
End Function

'**********************************************************
'Abgleich, ob Pattern zum Text passt
'erstellt von: Kai A. Quante
'erstellt am: 04.11.2022
'**********************************************************
Public Function regExpSuche(strText As String, strPattern As String) As Boolean
    Dim objRegExp As RegExp
    Set objRegExp = New RegExp
    With objRegExp
        .Pattern = strPattern
        regExpSuche = .Test(strText)
    End With
    Set objRegExp = Nothing
End Function






